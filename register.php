<?php
	require_once('_inc.php');
	require_once('functions.php');
	$head_title = array();
	$head_title[] = 'Register';

	if($user->isAuthorized()){
		header('Location: '.MK_Utility::serverUrl('/'), true, 302);
		exit;
	}

	$user_module = MK_RecordModuleManager::getFromType('user');
	$user_meta_module = MK_RecordModuleManager::getFromType('user_meta');
	
	$register_classes = ($config->extensions->core->email_verification)? "email_var_div rounded-corners" : "rounded-corners";	
	$output = '<div id="register" class="' .$register_classes. '">';

	if( $verification_code = MK_Request::getQuery('verification_code') )
	{
		$get_user_from_code = $user_meta_module->searchRecords(array(
			array('field' => 'key', 'value' => 'verification_code'),
			array('field' => 'value', 'value' => $verification_code),
		));

		if( $user_from_code = array_pop($get_user_from_code) )
		{
			$user = $user_from_code->objectUser();
			$user
				->setVerificationCode('')
				->isEmailVerified(true)
				->save();
			$output .= '<h2>Email Verified</h2>';
			$output .= '<p class="alert success">Your email address has been verified, your account has been activated and you have been logged in.</p>';
			$session->login = $user->getId();
			
			$cookie->set('login', $user->getId(), $config->site->user_timeout);
		}
		else
		{
			$output .= '<h2>Invalid Verification Code</h2>';
			$output .= '<p>This verification code is invalid.</p>';
		}

	}
	else
	{
		$field_module = MK_RecordModuleManager::getFromType('module_field');
		$criteria = array(
			array('field' => 'module_id', 'value' => $user_module->getId()),
			array('field' => 'name', 'value' => 'email')
		);
		
		$user_email_field = $field_module->searchRecords($criteria);
		$user_email_field = array_pop( $user_email_field );
	
		$settings = array(
			'attributes' => array(
				'class' => 'narrow clear-fix standard'
			)
		);
	
		$structure = array(
			'display_name' => array(
				'label' => 'Display name',
				'validation' => array(
					'instance' => array()
				)
			),
			'email' => array(
				'label' => 'Email',
				'validation' => array(
					'email' => array(),
					'instance' => array(),
					'unique' => array(null, $user_email_field, $user_module)
				)
			),
			'password' => array(
				'label' => 'Password',
				'type' => 'password',
				'validation' => array(
					'instance' => array(),
				),
			),
			'Postcode' => array(
				'label' => 'Postcode',
				'validation' => array(
					'instance' => array()
				)
			),
			'submit' => array(
				'type' => 'submit',
				'attributes' => array(
					'value' => 'Register'
				)
			)
		);
	
		
		$settings_login = array(
			'attributes' => array(
				'class' => 'narrow clear-fix standard social'
			)
		);
	
		$structure_login = array();
	
		if($config->site->facebook->login)
		{
			$structure_login['facebook'] = array(
				'type' => 'link',
				'text' => 'Facebook',
				'attributes' => array(
					'href' => 'login.php?platform=facebook'
				)
			);	
		}
	
		if($config->site->twitter->login)
		{
			$structure_login['twitter'] = array(
				'type' => 'link',
				'text' => 'Twitter',
				'attributes' => array(
					'href' => 'login.php?platform=twitter'
				)
			);	
		}
	
		if($config->site->windowslive->login)
		{
			$structure_login['windowslive'] = array(
				'type' => 'link',
				'text' => 'Windows Live',
				'attributes' => array(
					'href' => 'login.php?platform=windowslive'
				)
			);	
		}
	
		if($config->site->yahoo->login)
		{
			$structure_login['yahoo'] = array(
				'type' => 'link',
				'text' => 'Yahoo',
				'attributes' => array(
					'href' => 'login.php?platform=yahoo'
				)
			);
		}
	
		if($config->site->google->login)
		{
			$structure_login['google'] = array(
				'type' => 'link',
				'text' => 'Google',
				'attributes' => array(
					'href' => 'login.php?platform=google'
				)
			);	
		}
	
		$form = new MK_Form($structure, $settings);
	
		
		if($form->isSuccessful()){
			
			$new_user = MK_RecordManager::getNewRecord($user_module->getId());
			
			$new_user
				->setEmail($form->getField('email')->getValue())
				->setPassword($form->getField('password')->getValue())
				->setDisplayName($form->getField('display_name')->getValue())
				->save();
			
			$gm_cookie = new GM_Cookie();
			$gm_cookie->is_registered = true;
			$gm_cookie->WriteBackCookie();
				
			
			if( $config->extensions->core->email_verification )
			{
				$output .= '<p class="alert information email_var">Thank you for registering; you have been sent an email containing a link to verify your email address.<br /><br />Please click this link to complete your registration.</p>';
			}
			else
			{
				$session->login = $new_user->getId();
				$cookie->set('login', $new_user->getId(), $config->site->user_timeout);
				$output .= '<p class="alert success">Thank you for registering; you have been signed in and can <a href="edit-profile.php">make changes to your profile</a>.</p>';
			}
	
			if( $redirect = $config->extensions->core->register_url )
			{
				header('Location: '.MK_Utility::serverUrl($redirect), true, 302);
			}
	
		}else{
			$output .= '<div id="registration-form" class="rounded-corners">';
			$output .= '<legend class="form-title top-rounded-corners">Register</legend>';
			$output .= $form->render();
			$output .= '</div>';
			
			if(count($structure_login) > 0)
			{
				$login_form = new MK_Form($structure_login, $settings_login);
				$output .= '<h2>Or Login With</h2>';
				$output .= $login_form->render();
			}
		}
	}
	
	$output.='</div>';

	require_once('_header.php');
	print $output;
	require_once('_footer.php');
?>