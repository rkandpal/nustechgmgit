<?php
	session_start();
	require_once(dirname(__FILE__).'/../config.php');
	require_once(dirname(__FILE__).'/../functions.php');
	
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Unable to connect to database: '.$db_link->error);
	
	$current_list = (isset($_SESSION['list_id']) ? $_SESSION['list_id'] : get_last_id());
	//$current_user = $_SESSION['user_id'];
	
	$product_sale_id = $_POST['product_sale_id'];
	// Firstly, we need to know how many products were selected in the first place...
	$query = "SELECT shopping_list_product_count,shopping_list_product_id FROM shopping_lists_products WHERE shopping_list_id = $current_list AND shopping_list_product_sale_id = $product_sale_id";
	$result = $db_link->query($query);
	$result_row = $result->fetch_assoc();
	$number_of_items = $result_row['shopping_list_product_count'];
	$product_id = $result_row['shopping_list_product_id'];
	
	// Now delete them and remove them from the list totals.
	$query = "DELETE FROM shopping_lists_products WHERE shopping_list_id = $current_list AND shopping_list_product_sale_id = $product_sale_id";
	$query2 = "UPDATE shopping_lists SET total_rrp = total_rrp - ((SELECT price FROM product_sales WHERE id = $product_sale_id) * $number_of_items) WHERE id = $current_list;";
	$query3 = "UPDATE shopping_lists SET total_special = total_special - ((SELECT special_price FROM product_sales WHERE id = $product_sale_id) * $number_of_items) WHERE id = $current_list;";
	$output = '';
	
	// Update the total product sale count
	$strUpdateProductSaleQuery = "UPDATE products SET product_sale_cnt = product_sale_cnt - ('1') WHERE id='".$product_id."'";
	$strUpdateProductSaleQueryExe = mysql_query($strUpdateProductSaleQuery);
	
	
	$result = $db_link->query($query);
	if(!$result){
		$output .= "<tr><td colspan=\"5\">There was a problem removing the product to your shopping list</td></tr>";	
	}
	$result = $db_link->query($query2);	
	if(!$result){
		$output	.= "<tr><td colspan=\"5\">There was a problem updating the total rrp of your shopping list</td></tr>";
	}
	$result = $db_link->query($query3);
	if(!$result){
		$output	.= "<tr><td colspan=\"5\">There was a problem saving the total savings of your shopping list.</td></tr>";
	}
	if($result):
		echo true;
	else:
		echo false;
	endif;
	
	//echo $output;
?>