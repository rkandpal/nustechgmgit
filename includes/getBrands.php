<?php
	
	require_once('../config.php');
	require_once('../functions.php');
	
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME) 
		or die("There was an error connecting to the database: ".$db_link->error);
	$brand_id = $_POST['brandId'];
		
	$query = "SELECT products.id, product_sales.id, products.measure, products.uom, products.image,
					 product_sales.price, product_sales.special_price,
					 brands.name,
					 product_types.name,
					 supermarkets.name,
					 
			  FROM products, product_sales, brands, product_types, supermarkets, aisle 
			  WHERE products.brand = $brand_id AND 
			  		products.brand = brands.id AND 
					product_sales.product_id = products.id AND
					products.type = product_types.id AND
					product_types.aisle_id = aisle.id AND
					product_sales.supermarket_id = supermarkets.id";
			  
	$results = $db_link->prepare($query);

		$results->bind_result($id, $sale_id, $measure, $uom, $image, $price, $special, $brand, $product_type, $supermarket);
		$results->execute();
		$results->store_result();
		$row_cnt = $results->num_rows;
		
		$odd_even = "odd";
	$output = '';
	//echo $row_cnt;
	if(empty($row_cnt)):
		$output['result'] = false;
		$output['html'] = '<p>Sorry there are no products of this type on special</p>';
	else :
		$output['result'] = true;
		while($results->fetch())
		{	
			$product_image = ($image != '')?$image:"default_product.jpg";
			//$output['html'] = '<div class="hidden" id="product-type">'.ucwords($category).' &gt; '.ucwords($product_type).'</div>';
			$output['html'] .= '<div class="product rounded-corners '.$odd_even.'">';
			$output['html'] .= '<table cellpadding="0" cellspacing="0" width="100%">';
			$output['html'] .= '	<tr>';
			$output['html'] .= '		<td class="image"><img class="product-thumb rounded-corners" src="images/'.$product_image.'" style="vertical-align:top" width="130" /></td>';
			$output['html'] .= '		<td class="product-data">';
			$output['html'] .= '			<table cellpadding="0" cellspacing="0" width="100%">';
			$output['html'] .= '				<tr><td class="product-name">'.RewriteSmartQuotes($brand).'</td>';
			$output['html'] .= '				<tr><td class="weight">'.round($measure,2) . '' . $uom .'</td></tr>';
			$output['html'] .= '				<tr><td class="price">RRP: $' . $price . '</td></tr>';
			$output['html'] .= '				<tr><td class="special">Special: $'.$special.'</td></tr>';			
			$output['html'] .= 				'</table>';
			$output['html'] .= '		</td>';
			$output['html'] .= '	</tr>';
			$output['html'] .= '	<tr>';
			$output['html'] .= '		<td class="supermarket"><img src="images/'.strtolower($supermarket).'.png" width="80"/></td>';
			$output['html'] .= ' 		<td class="forms"><div class="product-form">';
			$output['html'] .= '				<form class="view-product" name="view-product" id="view-'.$id.'" method="get" action="product.php">';
			$output['html'] .= '					<input type="submit" class="rounded-corners" name="view" id="view-'.$id.'" value="VIEW" />';
			$output['html'] .= '					<input type="hidden" name="pid" value="'.$id.'" />';
			$output['html'] .= '				</form>';
			$output['html'] .= '				<form class="add-to-basket" name="add-to-basket" onsubmit="javascript:addToBasket('.$sale_id.'); return false;">';
            $output['html'] .= '					<input type="submit" class="rounded-corners" id="add" name="add" value="ADD" />';
            $output['html'] .= '	   			 	<input type="hidden" name="product_id" id="product_id" value="'.$sale_id.'" />';
            $output['html'] .= '				</form>';
			$output['html'] .= '			</div>';
			$output['html'] .= '		</td>';
			$output['html'] .= '	</tr>';	
			$output['html'] .= '</table>';
			$output['html'] .= '</div>';

			//$hash_url = ('#' . $aisle_id . '-' . $category_id);
			//$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
			//$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $category);
			//$output['parent_hash_url'] = $hash_url;
			
			$odd_even = ($odd_even == "odd")? "even" : "odd";          
		}
	endif;
	echo json_encode($output);
	
	
?>