<?php

	require_once(dirname(__FILE__).'/../functions.php');
	require_once(dirname(__FILE__).'/../config.php');
	
	StartSession();
	
	$output = array();
	
	if (! $_SESSION['user_id']) {
		$output['result'] = false;
	} else {
		$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		
		$list_id = $_POST['list_id'];
		$query = "UPDATE shopping_lists SET visible = 0 WHERE id = $list_id AND user_id=" . intval($_SESSION['user_id']);
		
		if($db_link->query($query)):
			$output['result'] = true;
			if($_SESSION['user_id']){
				$output['location'] = "members.php?profile=".$_SESSION['user_id'];
				if($_SESSION['list_id'] == $list_id){
					$_SESSION['list_id'] = get_last_id();
				}
			}
			else{
				$output['location'] = "index.php";	
			}
			
		else:
			$output['result'] = false;
		endif;
	}

	echo json_encode($output);
?>