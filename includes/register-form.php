<div id="register" class="rounded-corners">
    <form action="" method="post" name="registration" id="registration-form">
        <fieldset>
            <legend class="form-title top-rounded-corners">REGISTER</legend>
            <input size="40" type="text" name="username" id="username" placeholder="Name" />
            <input size="40" type="text" name="email" id="email" placeholder="Email" />
            <input size="40" type="password" name="password" id="password" placeholder="Password" />
            <input size="40" type="text" name="postcode" id="postcode" placeholder="Postcode" />
            <div>
                <select name="gender" id="gender" class="select">
                    <option value="NULL">Gender</option>
                    <option value="female">Female</option>
                    <option value="male">Male</option>
                </select>
            </div><!-- .styled-select -->
            <div>
                <select name="family" id="family" class="select">
                    <option value="NULL">Family Size</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10+</option>
                </select>
            </div>
            <br />
            <br />
            <input type="checkbox" name="adverts" id="adverts" class="styled" checked /><label for="adverts">Accepet Advertising &nbsp;</label><br />
            <input type="submit" name="register-submit" id="register-submit" value="Submit" class="rounded-corners" />
        </fieldset>
    </form>
</div>