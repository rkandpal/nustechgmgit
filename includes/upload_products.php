<?php
require_once(dirname(__FILE__).'/../_inc.php');
require_once(dirname(__FILE__).'/../config.php');
require_once(dirname(__FILE__).'/../functions.php');

define('PATH_TO_CSV_FILES', realpath(dirname(__FILE__) . '/../csv_files/') . DIRECTORY_SEPARATOR);
define('TOP_FAKE_EAN', 9999999999999);

function GetAisleMap() {
	static $the_map = null;
	
	if ($the_map === null) {
		$the_map = array();
		
		$link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$query = 'SELECT * FROM aisle ORDER BY aisle_name ASC';
		$result = $link->query($query);
		
		while ($row = $result->fetch_assoc()) {
			$the_map[intval($row['id'])] = trim($row['aisle_name']);
		}
	}

	return $the_map;
}

function GetStatePostcodeMap() {
	$return_val = array(
		'2000' => 'NSW / ACT',
		'3000' => 'VIC',
		'4000' => 'QLD',
		'5000' => 'SA',
		'6000' => 'WA',
		'7000' => 'TAS',
		'0800' => 'NT'
	);
	
	return $return_val;
}

function GetNextFakeEANTOUse() {
	static $next_fake_ean = null;
	
	if ($next_fake_ean === null) {
		$link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		
		// Firstly... does the 'top' one exist?
		$query = 'SELECT id FROM products WHERE ean=' . TOP_FAKE_EAN;
		$result = $link->query($query);
		
		if (! $result->num_rows) {
			$next_fake_ean = TOP_FAKE_EAN;
		} else {
			// Well at least one fake one exists...
			$query = 'SELECT MAX(ean) AS max_ean FROM products WHERE (ean - 1) NOT IN (SELECT ean FROM products);';
			$result = $link->query($query);
			$row = $result->fetch_assoc();
			$max_fake_ean = $row['max_ean'];
			$next_fake_ean = ($max_fake_ean - 1);
		}
	}
	
	return $next_fake_ean;
}

function GetAisleNameForID($the_id) {
	$the_id = intval($the_id);
	$the_map = GetAisleMap();
	return (isset($the_map[$the_id]) ? $the_map[$the_id] : null);
}

function GetAisleIDForName($the_name) {
	$the_name = trim($the_name);
	$the_map = GetAisleMap();
	
	foreach ($the_map as $key => $value) {
		if (strtolower($value) == strtolower($the_name)) {
			return $key;
		}
	}

	return null;
}

function GetProductTypeMap() {
	static $the_map = null;
	
	if ($the_map === null) {
		$the_map = array();
		
		$link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$query = 'SELECT product_types.id AS id, product_types.name AS name, 
						product_types.aisle_id AS aisle_id, aisle.aisle_name AS aisle_name
					FROM product_types 
					INNER JOIN aisle ON aisle.id=product_types.aisle_id
					ORDER BY product_types.name, aisle.aisle_name ASC';
		$result = $link->query($query);
		
		while ($row = $result->fetch_assoc()) {
			$type_information = array();
			$type_information['id'] = intval($row['id']);
			$type_information['name'] = trim(CleanUploadedString($row['name']));
			$type_information['aisle_id'] = intval($row['aisle_id']);
			$type_information['aisle_name'] = trim(CleanUploadedString($row['aisle_name']));
			$the_map[intval($row['id'])] = $type_information;
		}
	}

	return $the_map;
}

function GetProductTypeForID($the_id) {
	$the_id = intval($the_id);
	$the_map = GetProductTypeMap();
	return (isset($the_map[$the_id]) ? $the_map[$the_id] : null);
}

// This function will actually return an array, as there can be more than
// one product type with the same name.
function GetProductTypeForName($the_name) {
	$the_name = trim($the_name);
	$the_map = GetProductTypeMap();
	
	$the_list = array();
	foreach ($the_map as $key => $value) {
		if (strtolower($value['name']) == strtolower($the_name)) {
			$the_list[$value['id']] = $value;
		}
	}

	return $the_list;
}

function GetProductTypesForAisle($aisle_id) {
	$aisle_id = intval($aisle_id);
	$the_map = GetProductTypeMap();
	
	$product_types = array();
	foreach ($the_map as $key => $value) {
		if ($value['aisle_id'] == $aisle_id) {
			$product_types[$key] = $value;
		}
	}
	
	return $product_types;
}

function IsValidEAN($the_ean) {
	if ($the_ean == '') {
		return true;
	}
	
	if (preg_match('/^[0-9]+$/', $the_ean)) {
		if ((strlen($the_ean) == 8) || (strlen($the_ean) == 13)) {
			return true;
		}
	}
	
	return false;
}

// Copied from http://forums.solmetra.com/viewtopic.php?f=1&t=1116
function fix_fancy_quotes($text) {
	$p = array("\xBB","\xAB","\xAA","\xD2","\x93","\x94","\x8D",
			"\xBA","\xD3","\x8E","\xD4","\x92","\x8F","\xD5",
			"\x90","\xD0","\xD1","\x97","\x84", "\x85");
	$r = array(  ")" , "("  , '"'  , '"'  , '"'  , '"'  , '"'  , '"'  , '"'  , '"'  ,
			"'"  , "'"  , "'"  , "'"  , "'"  , "-"  , "-"  , "-"  , "-", "...");
	return str_replace($p,$r,$text);
}

function CleanUploadedString($the_string) {
	$given_string = $the_string;
	$the_string = fix_fancy_quotes($the_string);
	$the_string = RewriteSmartQuotes($the_string);
	
	// error_log('"' . $given_string . '" became "' . $the_string . '"');
	return $the_string;
}

function GetDefaultSaleDates($single_value_to_return = null) {
	$return_val = array();
	
	$now = new DateTime();
	$seven_days = new DateTime();
	$seven_days->add(new DateInterval('P7D'));	// add 7 days
	
	$return_val['start'] = $now->format('Y-m-d');
	$return_val['end'] = $seven_days->format('Y-m-d');
	
	if ($single_value_to_return && isset($return_val[$single_value_to_return])) {
		return $return_val[$single_value_to_return];
	} else {
		return $return_val;
	}
}

function mysql_query_and_log($i, $query, $link, $return_log = false) {
	// As per http://stackoverflow.com/a/2291156/74225, this file is getting included from somewhere else and 
	// therefore this is not in the global namespace... and we can't seem to get to it any other way outside of
	// this function. So, a static it is!
	static $_mysql_query_log = null;

	if ($_mysql_query_log === null) {
		$_mysql_query_log = array();
	}

	if ($return_log) {
		return $_mysql_query_log;
	}
	
	$readable_query = preg_replace('/\s{2,}/', ' ', $query);
	// We offset $i by 2: 1 for the zero index and 1 for the expected header line for the final CSV
	$_mysql_query_log[] = array(($i + 2), $readable_query);
	
	return mysql_query($query, $link);
}

$link = mysql_connect(DB_HOST, DB_USER, DB_PASS);
if (!$link) {
	die('Could not connect: ' . mysql_error());
}

// make foo the current db
$db_selected = mysql_select_db(DB_NAME, $link);
if (!$db_selected) {
	die('Can\'t use ' . DB_NAME . ' : ' . mysql_error());
}

$process = (isset($_POST['process']) ? true : false);

$uploadSuccess = false;
$errors = array();

// Maybe we're following a link to continue a file?
$file_to_load = ((isset($_POST['saved_filename']) && $_POST['saved_filename']) ?$_POST['saved_filename'] : '');

// we have first uploaded the file
// numRows will be set when we display the form which allows users to set the ean
if ($process && !isset($_POST['numRows']) && (! $file_to_load)) {
	$sale_start_date = (isset($_POST['sale_start_date']) ? $_POST['sale_start_date'] : '');
	$sale_end_date = (isset($_POST['sale_end_date']) ? $_POST['sale_end_date'] : '');
	$sale_do_override_postcodes = (isset($_POST['sale_do_override_postcodes']) ? $_POST['sale_do_override_postcodes'] : false);
	$sale_override_postcode_2000 = (isset($_POST['sale_override_postcode_2000']) ? $_POST['sale_override_postcode_2000'] : false);
	$sale_override_postcode_3000 = (isset($_POST['sale_override_postcode_3000']) ? $_POST['sale_override_postcode_3000'] : false);
	$sale_override_postcode_4000 = (isset($_POST['sale_override_postcode_4000']) ? $_POST['sale_override_postcode_4000'] : false);
	$sale_override_postcode_5000 = (isset($_POST['sale_override_postcode_5000']) ? $_POST['sale_override_postcode_5000'] : false);
	$sale_override_postcode_6000 = (isset($_POST['sale_override_postcode_6000']) ? $_POST['sale_override_postcode_6000'] : false);
	$sale_override_postcode_7000 = (isset($_POST['sale_override_postcode_7000']) ? $_POST['sale_override_postcode_7000'] : false);
	$sale_override_postcode_0800 = (isset($_POST['sale_override_postcode_0800']) ? $_POST['sale_override_postcode_0800'] : false);
	
	$dates_ok = true;
	if ($dates_ok) {
		$dates_ok = false;
		if (! $sale_start_date) {
			$errors[] = 'Please enter a sale start date.';
		} else if (! $sale_end_date) {
			$errors[] = 'Please enter a sale end date.';
		} else {
			$dates_ok = true;
		}
	}
	
	if ($dates_ok) {
		$dates_ok = false;
		if (! preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $sale_start_date)) {
			$errors[] = 'Please enter a valid start date in the form of "yyyy-mm-dd".';
		} else if (! preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $sale_end_date)) {
			$errors[] = 'Please enter a valid end date in the form of "yyyy-mm-dd".';
		} else {
			$dates_ok = true;
		}
	}

	if ($dates_ok) {
		$dates_ok = false;
		$sale_start_date_parts = explode('-', $sale_start_date);
		$sale_end_date_parts = explode('-', $sale_end_date);
		
		if (! checkdate($sale_start_date_parts[1], $sale_start_date_parts[2], $sale_start_date_parts[0])) {
			$errors[] = 'Please enter a valid start date in the form of "yyyy-mm-dd".';
		} else if (! checkdate($sale_end_date_parts[1], $sale_end_date_parts[2], $sale_end_date_parts[0])) {
			$errors[] = 'Please enter a valid end date in the form of "yyyy-mm-dd".';
		} else {
			$dates_ok = true;
		}
	}

	if ($dates_ok) {
		$dates_ok = false;
		
		// Only check for the end date being before the start date: we will
		// allow them to be equal, essentially giving us 'one day sales'
		if (strtotime($sale_start_date) > strtotime($sale_end_date)) {
			$errors[] = 'The given start date is later than the given end date.';
		} else {
			$dates_ok = true;
		}
	}
	
	if (! $dates_ok) {
		// do nothing... it's all been done	
	} else if (!isset($_FILES['uploadedfile']['type']) || !$_FILES['uploadedfile']['type']) {
		$errors[] = 'Please select a file.';
	} else if (! in_array($_FILES['uploadedfile']['type'], array('application/octet-stream', 'application/binary-stream', 'application/vnd.ms-excel', 'text/csv', 'text/x-csv'))) {
		$errors[] = 'The file must be a CSV: file type given was "' . $_FILES['uploadedfile']['type'] . '"';
	} else {
		$target_path = PATH_TO_CSV_FILES;
		
		$_SESSION['uploadedDateTime'] = time();
		$_SESSION['uploadedOriginalFilename'] = basename( $_FILES['uploadedfile']['name']);
		$_SESSION['uploadedSavedFilename'] = date('Ymd-His-', $_SESSION['uploadedDateTime']) . $_SESSION['uploadedOriginalFilename'];
		
		/* Add the original filename to our target path.  
		Result is "uploads/filename.extension" */
		$target_path = $target_path . $_SESSION['uploadedSavedFilename']; 
		
		$file = $target_path;//basename( $_FILES['uploadedfile']['name']);
		echo '<div style="position: relative; width: 100%;text-align: left; margin: 30px 20px 10px 50px;">';
		if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
			$uploadSuccess = true;
		} else {
			$errors[] = "There was an error uploading the file, please try again.";
		}		
		echo '</div>';
	}
	
}

// if we have successfully uploaded the file
if ($uploadSuccess) {
	ini_set('auto_detect_line_endings', true);
	$tableData = array();
	
	$handle = fopen($file, "r");

	// there are two header rows
	$data = fgetcsv($handle, 1000);
	$data = fgetcsv($handle, 1000);

	$row=3;
	$count=0;
	while (($data = fgetcsv($handle, 1000)) !== false) {
		// What if this is an empty line?
		if ((count($data) == 1) && ($data[0] === null)) {
			continue;
		}
		
		$tableData[$count] = array();
		
		// for easy reference
		$supermarketName =	(isset($data[0]) ? trim($data[0]) : "");
		$postCode_2000 =	(isset($data[1]) ? trim($data[1]) : "");
		$postCode_3000 =	(isset($data[2]) ? trim($data[2]) : "");
		$postCode_4000 =	(isset($data[3]) ? trim($data[3]) : "");
		$postCode_5000 =	(isset($data[4]) ? trim($data[4]) : "");
		$postCode_6000 =	(isset($data[5]) ? trim($data[5]) : "");
		$postCode_7000 =	(isset($data[6]) ? trim($data[6]) : "");
		$postCode_0800 =	(isset($data[7]) ? trim($data[7]) : "");
		
		if ($sale_do_override_postcodes) {
			$postCode_2000 = $sale_override_postcode_2000;
			$postCode_3000 = $sale_override_postcode_3000;
			$postCode_4000 = $sale_override_postcode_4000;
			$postCode_5000 = $sale_override_postcode_5000;
			$postCode_6000 = $sale_override_postcode_6000;
			$postCode_7000 = $sale_override_postcode_7000;
			$postCode_0800 = $sale_override_postcode_0800;
		}
		
		$ean =			(isset($data[8]) ? trim($data[8]) : "");
		
		$given_aisle_name = CleanUploadedString(isset($data[9]) ? trim($data[9]) : "");
		$aisle =		($given_aisle_name ? GetAisleIDForName($given_aisle_name) : "");
		
		$product_type =		CleanUploadedString(isset($data[10]) ? trim($data[10]) : "");
		$name =			CleanUploadedString(isset($data[11]) ? trim($data[11]) : "");
		$technical_name = CleanUploadedString(isset($data[12]) ? trim($data[12]) : "");
		$brandName =		CleanUploadedString(isset($data[13]) ? trim($data[13]) : "");
		$measure =		(isset($data[14]) ? $data[14] : 0);
		$uom =			(isset($data[15]) ? trim($data[15]) : "");
		$description =		CleanUploadedString(isset($data[16]) ? trim($data[16]) : "");
		$price =		(isset($data[19]) ? preg_replace("/[^0-9.]/", "", $data[19]) : 0); // just want numbers and the period
		$specialPrice =		(isset($data[20]) ? preg_replace("/[^0-9.]/", "", $data[20]) : 0); // just want numbers and the period
		$promoType =		(isset($data[21]) ? trim($data[21]) : "");
		
		$highlighted = (isset($data[22]) ? trim($data[22]) : "");
		
		// Let's check the validity of the EAN
		if (! IsValidEAN($ean)) {
			$errors[] = 'Product row ' . ($count + 1) . ': ' . $ean . ' is not a valid EAN';
			$ean = '';
		}

		// Is the given product type a valid one?
		if ($product_type) {
			$product_type_information = GetProductTypeForName($product_type);
			
			if (! count($product_type_information)) {
				$errors[] = 'Product row ' . ($count + 1) . ': ' . $product_type . ' is not a valid product type';
				$product_type = '';
			}
		}
		
		/*
		 * If given a product type and given a aisle, let's check that the aisle is
		 * valid for the product type. If it is, great, but if it's not, just reset the aisle 
		 * to empty. Even if there's only one possible option as let's face it, there's a 
		 * problem...
		 * 
		 * If we are not given a aisle, then we will set it if the given product type only
		 * exists in one aisle.
		 */
		if ($product_type) {
			$product_type_information = GetProductTypeForName($product_type);
			
			if ($aisle) {
				$found_valid_aisle = false;
				
				foreach ($product_type_information as $product_type_row) {
					if ($product_type_row['aisle_id'] == $aisle) {
						$found_valid_aisle = true;
						// We can correct the product name too if we like :)
						$product_type = $product_type_row['name'];
					}
				}
				
				if (! $found_valid_aisle) {
					$errors[] = 'Product row ' . ($count + 1) . ': ' . $given_aisle_name . ' is not a valid aisle for product type ' . $product_type;
					$aisle = '';
				}
			} else {
				if (count($product_type_information) == 1) {
					$first_product_type = reset($product_type_information);
					$aisle = $first_product_type['aisle_id'];
					// We can correct the product name too if we like :)
					$product_type = $first_product_type['name'];
				}
			}
		}
		
		// lets normalize these units of measure a bit
		if (in_array(strtolower($uom), array('g', 'grams', 'gram', 'Grams', 'Gram'))) {
			$uom = 'g';
		} else if (in_array(strtolower($uom), array('kg', 'kilogram', 'kilograms'))) {
			$uom = 'kg';
		} else if (in_array(strtolower($uom), array('ml', 'mls', 'milliliter', 'milliliters', 'millilitre', 'millilitres'))) {
			$uom = 'mL';
		} else if (in_array(strtolower($uom), array('l', 'liter', 'liters', 'litre', 'litres'))) {
			$uom = 'L';
		}else if (in_array(strtolower($uom), array('pack'))) {
			$uom = 'Pack';
		}
		
		$image_filename = '';
		$image_default = false;
		
		// If given an EAN in the upload...
		if ($ean && IsValidEAN($ean)) {
			$query = "SELECT 	products.image AS product_image, 
								aisle.id AS aisle_id,products.highlighted AS highlight,
								product_types.name AS product_type_name,  
								products.name AS product_name,
								products.technical_name AS product_technical_name,
								brands.name AS brand_name,
								products.measure AS product_measure,
								products.uom AS product_uom,
								products.description AS product_description
			
						FROM products 
						INNER JOIN aisle ON aisle.id=products.aisle_id
						INNER JOIN product_types ON product_types.id=products.type
						INNER JOIN brands ON brands.id=products.brand
						WHERE ean='" . mysql_real_escape_string($ean) ."'";
			
			$result = mysql_query($query, $link);
			
			// If we already have this product, we can ignore a fair amount of information
			if (mysql_num_rows($result)) {
				$row = mysql_fetch_assoc($result);
				$image_filename = $row['product_image'];
				$image_default = ($image_filename == '');

				$aisle = intval($row['aisle_id']);
				$product_type = CleanUploadedString($row['product_type_name']);
				$name = CleanUploadedString($row['product_name']);
				$technical_name = CleanUploadedString($row['product_technical_name']);
				$brandName = CleanUploadedString($row['brand_name']);
				$measure = $row['product_measure'];
				$uom = $row['product_uom'];
				$description = CleanUploadedString($row['product_description']);
				$highlighted = $row['highlight'];
			}
		}

		// If we don't have an EAN, but we can find a product that matches closely, we will
		// use it, instead...
		if ((! $ean) && ($name || $technical_name) && $brandName && $measure && $uom) {
			$query = "SELECT 	
							products.ean AS product_ean,
							products.image AS product_image,
							aisle.id AS aisle_id, products.highlighted AS highlight,
							product_types.name AS product_type_name,
							products.name AS product_name,
							products.technical_name AS product_technical_name,
							products.description AS product_description
			
						FROM products
							INNER JOIN aisle ON aisle.id=products.aisle_id
							INNER JOIN product_types ON product_types.id=products.type
							INNER JOIN brands ON brands.id=products.brand
						
						WHERE (products.name='" . mysql_real_escape_string($name) ."' 
								OR products.technical_name='" . mysql_real_escape_string($technical_name) ."')
							AND brands.name='" . mysql_real_escape_string($brandName) . "' 
							AND products.measure='" . mysql_real_escape_string($measure) . "'
							AND products.uom='" . mysql_real_escape_string($uom) . "'";
			
			$result = mysql_query($query, $link);

			// If we already have this product, we can ignore a fair amount of information
			if (mysql_num_rows($result)) {
				$row = mysql_fetch_assoc($result);
				$image_filename = $row['product_image'];
				$image_default = ($image_filename == '');
			
				$ean = $row['ean'];
				$aisle = intval($row['aisle_id']);
				$product_type = CleanUploadedString($row['product_type_name']);
				$name = CleanUploadedString($row['product_name']);
				$technical_name = CleanUploadedString($row['product_technical_name']);
				$description = CleanUploadedString($row['product_description']);
				$highlighted = $row['highlight'];
			}
		}
		
		$tableData[$count]["Supermarket"] =	$supermarketName;
		$tableData[$count]["PostCode_2000"] =	$postCode_2000;
		$tableData[$count]["PostCode_3000"] =	$postCode_3000;
		$tableData[$count]["PostCode_4000"] =	$postCode_4000;
		$tableData[$count]["PostCode_5000"] =	$postCode_5000;
		$tableData[$count]["PostCode_6000"] =	$postCode_6000;
		$tableData[$count]["PostCode_7000"] =	$postCode_7000;
		$tableData[$count]["PostCode_0800"] =	$postCode_0800;
		$tableData[$count]["EAN"] =		$ean;
		$tableData[$count]["Image"] =		$image_filename;
		$tableData[$count]["ImageDefault"] = $image_default;
		$tableData[$count]["Aisle"] =	intval($aisle);
		$tableData[$count]["ProductType"] =	$product_type;
		$tableData[$count]["Name"] =		$name;
		$tableData[$count]["TechnicalName"] = $technical_name;
				$image_default = ($image_filename == '');
		$tableData[$count]["Brand"] =		$brandName;
		$tableData[$count]["Measure"] =		$measure;
		$tableData[$count]["UOM"] =		$uom;
		$tableData[$count]["Description"] =	$description;
		$tableData[$count]["Price"] =		$price;
		$tableData[$count]["Special"] =		$specialPrice;
		$tableData[$count]["Promo"] =		$promoType;
		$tableData[$count]["highlighted"] =		$highlighted;
		$count++;
	}
	fclose($handle);
}

/*
 * This is probably as good a point as any to load from (or write to!) 
 * the 'deferred' file if need be...
 */
if ($process && $file_to_load) {
	// We're going to store the majority of the file into the POST array... we used
	// the same keys so that we can just copy over the top of the original POST.
	$file_contents = file_get_contents(PATH_TO_CSV_FILES . $file_to_load);
	$store_array = unserialize($file_contents);

	$values_to_copy = array('Supermarket', 
			'PostCode_2000', 'PostCode_3000', 'PostCode_4000', 'PostCode_5000', 'PostCode_6000', 'PostCode_7000', 'PostCode_0800',  
			'EAN', 'Image', 'ImageDefault',
			'Aisle', 'ProductType', 'Name', 'TechnicalName', 'Brand', 'Measure',
			'UOM', 'Description', 'Price', 'Special', 'Promo','highlighted');
	
	$_POST['numRows'] = $store_array['numRows'];
	$_POST['sale_start_date'] = (isset($store_array['sale_start_date']) ? $store_array['sale_start_date'] : null);
	$_POST['sale_end_date'] = (isset($store_array['sale_end_date']) ? $store_array['sale_end_date'] : null);
	$_POST['sale_do_override_postcodes'] = (isset($store_array['sale_do_override_postcodes']) ? $store_array['sale_do_override_postcodes'] : null);
	$_POST['sale_override_postcode_2000'] = (isset($store_array['sale_override_postcode_2000']) ? $store_array['sale_override_postcode_2000'] : null);
	$_POST['sale_override_postcode_3000'] = (isset($store_array['sale_override_postcode_3000']) ? $store_array['sale_override_postcode_3000'] : null);
	$_POST['sale_override_postcode_4000'] = (isset($store_array['sale_override_postcode_4000']) ? $store_array['sale_override_postcode_4000'] : null);
	$_POST['sale_override_postcode_5000'] = (isset($store_array['sale_override_postcode_5000']) ? $store_array['sale_override_postcode_5000'] : null);
	$_POST['sale_override_postcode_6000'] = (isset($store_array['sale_override_postcode_6000']) ? $store_array['sale_override_postcode_6000'] : null);
	$_POST['sale_override_postcode_7000'] = (isset($store_array['sale_override_postcode_7000']) ? $store_array['sale_override_postcode_7000'] : null);
	$_POST['sale_override_postcode_0800'] = (isset($store_array['sale_override_postcode_0800']) ? $store_array['sale_override_postcode_0800'] : null);
	
	if (! $_POST['sale_start_date']) {
		$_POST['sale_start_date'] = GetDefaultSaleDates('start');
	}
	if (! $_POST['sale_end_date']) {
		$_POST['sale_end_date'] = GetDefaultSaleDates('end');
	}
	
	for ($i = 0; ($i < $_POST['numRows']); $i++) {
		// Maybe we deleted it?
		if (! isset($store_array['Supermarket-' . $i])) {
			continue;
		}
		
		foreach ($values_to_copy as $value_to_copy) {
			$post_key = ($value_to_copy . '-' . $i);
	
			if ($value_to_copy == 'ImageDefault') {
				if (isset($store_array[$post_key]) && $store_array[$post_key]) {
					$_POST[$post_key] = $store_array[$post_key];
				}
			} else {
				$_POST[$post_key] = $store_array[$post_key];
			}
		}
	}
}

if ($process && isset($_POST['csv_save']) && $_POST['csv_save']) {
	// We're going to store the majority of the POST into a new array... we will use
	// the same keys so that we can just copy over the top of the original POST if
	// need be.
	$values_to_copy = array('Supermarket', 
								'PostCode_2000', 'PostCode_3000', 'PostCode_4000', 'PostCode_5000', 'PostCode_6000', 'PostCode_7000', 'PostCode_0800',  
								'EAN', 'Image', 'ImageDefault',
								'Aisle', 'ProductType', 'Name', 'TechnicalName', 'Brand', 'Measure',
								'UOM', 'Description', 'Price', 'Special', 'Promo','highlighted');
	
	$store_array = array(
						'numRows' => $_POST['numRows'],
						'sale_start_date' => $_POST['sale_start_date'],
						'sale_end_date' => $_POST['sale_end_date'],
						'sale_do_override_postcodes' => $_POST['sale_do_override_postcodes'],
						'sale_override_postcode_2000' => $_POST['sale_override_postcode_2000'],
						'sale_override_postcode_3000' => $_POST['sale_override_postcode_3000'],
						'sale_override_postcode_4000' => $_POST['sale_override_postcode_4000'],
						'sale_override_postcode_5000' => $_POST['sale_override_postcode_5000'],
						'sale_override_postcode_6000' => $_POST['sale_override_postcode_6000'],
						'sale_override_postcode_7000' => $_POST['sale_override_postcode_7000'],
						'sale_override_postcode_0800' => $_POST['sale_override_postcode_0800'],
		);
	
	for ($i = 0; ($i < $_POST['numRows']); $i++) {
		// Maybe we deleted it?
		if (! isset($_POST['Supermarket-' . $i])) {
			continue;
		}
		
		foreach ($values_to_copy as $value_to_copy) {
			$post_key = ($value_to_copy . '-' . $i);
			
			if ($value_to_copy == 'ImageDefault') {
				if (isset($_POST[$post_key]) && $_POST[$post_key]) {
					$store_array[$post_key] = $_POST[$post_key];
				}
			} else {
				$store_array[$post_key] = $_POST[$post_key];
			}
		}
	}
	
	$save_filename = (PATH_TO_CSV_FILES . $_SESSION['uploadedSavedFilename'] . '.save');
	$handle = fopen($save_filename, 'w');
	fwrite($handle, serialize($store_array));
	fclose($handle);
}

// if our table data is set (we just uploaded the file)
// or we are submitting from the form where we set the eans 
if (isset($tableData) || isset($_POST['numRows'])) {
		
	$missingEANs = array();
	$missingImages = array();
	$existingImages = array();
	$missingAisles = array();
	$missingProductTypes = array();
	$errorsExist = false;
	
	$uploadedData = array();
	
	// we just submitted the form, add to uploadedData array
	if (isset($tableData)) {
		foreach ($tableData as $count => $row) {
			$uploadedData[] = $row;
		}
	} else if (isset($_POST['numRows'])) {
		// else, grab the values from the post array
		$numRows = $_POST['numRows'];
		for ($i=0; $i<$numRows; $i++) {
			// Maybe we deleted it?
			if (! isset($_POST['Supermarket-' . $i])) {
				continue;
			}
			
			$uploadedData[] = array(	'Supermarket' =>	$_POST['Supermarket-'.$i],
							'PostCode_2000' =>		$_POST['PostCode_2000-'.$i],
							'PostCode_3000' =>		$_POST['PostCode_3000-'.$i],
							'PostCode_4000' =>		$_POST['PostCode_4000-'.$i],
							'PostCode_5000' =>		$_POST['PostCode_5000-'.$i],
							'PostCode_6000' =>		$_POST['PostCode_6000-'.$i],
							'PostCode_7000' =>		$_POST['PostCode_7000-'.$i],
							'PostCode_0800' =>		$_POST['PostCode_0800-'.$i],
							'EAN' =>		$_POST['EAN-'.$i],
							'Image' =>		$_POST['Image-'.$i],
							'ImageDefault' => (isset($_POST['Image-Default-'.$i]) && $_POST['Image-Default-'.$i]),
							'Aisle' =>		intval($_POST['Aisle-'.$i]),
							'ProductType' =>	CleanUploadedString($_POST['ProductType-'.$i]),
							'Name' =>		CleanUploadedString($_POST['Name-'.$i]),
							'TechnicalName' =>	CleanUploadedString($_POST['TechnicalName-'.$i]),
							'Brand' =>		CleanUploadedString($_POST['Brand-'.$i]),
							'Measure' =>		$_POST['Measure-'.$i],
							'UOM' =>		$_POST['UOM-'.$i],
							'Description' =>	CleanUploadedString($_POST['Description-'.$i]),
							'Price' =>		$_POST['Price-'.$i],
							'Special' =>		$_POST['Special-'.$i],
							'Promo' =>		$_POST['Promo-'.$i],
							'highlighted' =>		$_POST['highlighted-'.$i],
						);
		}
	}

	// make sure we have both eans and aisles
	for ($i=0; $i<count($uploadedData); $i++) {
		if ($uploadedData[$i]["EAN"] == "") {
			$errorsExist = true;
			$missingEANs[$i] = true;
			$missingImages[$i] = true;
		}

		if ($uploadedData[$i]["EAN"]) {
			// Do we have the image for this EAN?
			if ($uploadedData[$i]["Image"] == "") {
				$result = mysql_query("SELECT image FROM products WHERE ean='" . mysql_real_escape_string($uploadedData[$i]["EAN"], $link) . "'", $link);
				$row = mysql_fetch_assoc($result);
				
				// if this id doesn't exist yet (or it doesn't have an image)...
				if (($row === false) || (strlen(trim($row['image'])) == 0)) {
					// This only becomes a *real* problem if we're not using the default...
					$missingImages[$i] = true;
					if (! $uploadedData[$i]["ImageDefault"]) {
						$errorsExist = true;
					}
				} else {
					$existingImages[$i] = trim($row['image']);
				}
			} else {
				$existingImages[$i] = trim($uploadedData[$i]["Image"]);
			}
		}
		
		// from an email with adrian, we'll assume the the aisle are hard coded
		// we won't insert a aisle if it doesn't exist?
		// Aisle is now always numeric...
		$aisle_is_valid = false;
		
		if ($uploadedData[$i]['Aisle']) {
			$aisle_map = GetAisleMap();

			if (isset($aisle_map[$uploadedData[$i]['Aisle']])) {
				if (GetAisleNameForID($uploadedData[$i]['Aisle']) != '') {
					$aisle_is_valid = true;
				}
			}
		}
		
		if (! $aisle_is_valid) {
			$errorsExist = true;
			$missingAisles[$i] = true;
		}

		// We won't be adding product types dynamically either...
		$product_type_is_valid = false;
		if ($uploadedData[$i]['ProductType']) {
			$product_type_information = GetProductTypeForName($uploadedData[$i]['ProductType']);
			if (count($product_type_information)) {
				$product_type_is_valid = true;
			}
		}
		
		if (! $product_type_is_valid) {
			$errorsExist = true;
			$missingProductTypes[$i] = true;
		}
	}
}	

// we are ready to upload to the db!
if ((isset($_POST['numRows']) || isset($tableData)) && !$errorsExist) {
	for ($i=0; $i<count($uploadedData); $i++) {
		// Aisles are always numerical now
		$aisleID = intval($uploadedData[$i]['Aisle']);

		// Product types must exist in the database first...
		$productTypeID = null;
		$product_type_information = GetProductTypeForName($uploadedData[$i]['ProductType']);
		foreach ($product_type_information as $product_type_row) {
			if ($product_type_row['aisle_id'] == $aisleID) {
				$productTypeID = $product_type_row['id'];
			}
		}
		
		// if the brand doesn't exist, create it
		// grab the ID either way
		$result = mysql_query_and_log($i, "SELECT * FROM brands WHERE name='" . mysql_real_escape_string($uploadedData[$i]['Brand']) . "'", $link);

		if (mysql_num_rows($result)==0) {
			$result = mysql_query_and_log($i, "INSERT INTO brands (name) VALUES ('" . mysql_real_escape_string($uploadedData[$i]['Brand']) . "')", $link);
			$brandID = mysql_insert_id($link);
		} else {
			$row = mysql_fetch_assoc($result);
			$brandID = $row['id'];
		}

		// if the supermarket name doesn't exist, create it
		// grab the ID either way
		$result = mysql_query_and_log($i, "SELECT * FROM supermarkets WHERE name='" . mysql_real_escape_string($uploadedData[$i]["Supermarket"]) . "'", $link);

		if (mysql_num_rows($result)==0) {
			$result = mysql_query_and_log($i, "INSERT INTO supermarkets (name) VALUES ('" . mysql_real_escape_string($uploadedData[$i]["Supermarket"]) . "')", $link);
			$supermarketID = mysql_insert_id($link);
		} else {
			$row = mysql_fetch_assoc($result);
			$supermarketID = $row['id'];
		}
		
		// This store may need to exist in any number of states...
		$state_postcode_map = GetStatePostcodeMap();
		foreach ($state_postcode_map as $state_postcode => $state_name) {
			$postcode_key = 'PostCode_' . $state_postcode;
			
			// Do we even want it here?
			if ($uploadedData[$i][$postcode_key]) {
				// we aren't using the store IDs anywhere yet
				// if the store doesn't exist, create it
				// grab the IDs either way
				$result = mysql_query_and_log($i, "SELECT * FROM stores WHERE supermarket_id=" . intval($supermarketID) . " AND postcode='" . mysql_real_escape_string($uploadedData[$i][$postcode_key]) . "'", $link);
		
				$storeIDs = array(); // in case there are more than one locations with the same postcode
				if (mysql_num_rows($result)==0) {
					$result = mysql_query_and_log($i, "INSERT INTO stores (supermarket_id, postcode) VALUES (".intval($supermarketID).", '" . mysql_real_escape_string($uploadedData[$i][$postcode_key]) . "')", $link);
					$storeIDs[] = mysql_insert_id($link);
				} else {
					while ($row = mysql_fetch_assoc($result)) {
						$storeIDs[] = $row['id'];
					}
		
				}
			}
		}

		
		


		// add or grab out product
		$result = mysql_query_and_log($i, "SELECT * FROM products WHERE ean='" . mysql_real_escape_string($uploadedData[$i]["EAN"]) . "'", $link);
		$image_to_use = '';
		if ($uploadedData[$i]["Image"] && (! $uploadedData[$i]["ImageDefault"])) {
			$image_to_use = $uploadedData[$i]["Image"];
		}
		
		// we are adding a product
		if (mysql_num_rows($result)==0) {
			$result = mysql_query_and_log($i, "INSERT INTO products (	type, 
									ean, 
									image,
									brand, 
									measure, 
									uom, 
									aisle_id,
									name,
									technical_name,
									description,
									highlighted
							) VALUES (	" . intval($productTypeID) . ", 
									'" . mysql_real_escape_string($uploadedData[$i]["EAN"]) . "', 
									'" . mysql_real_escape_string($image_to_use) . "', 
									" . intval($brandID) . ", 
									'" . mysql_real_escape_string($uploadedData[$i]['Measure']) . "', 
									'" . mysql_real_escape_string($uploadedData[$i]["UOM"]) . "', 
									" . intval($aisleID) . ",
									'" . mysql_real_escape_string($uploadedData[$i]['Name']) . "',
									'" . mysql_real_escape_string($uploadedData[$i]['TechnicalName']) . "',
									'" . mysql_real_escape_string($uploadedData[$i]['Description']) ."',
									'" . mysql_real_escape_string($uploadedData[$i]['highlighted']) ."'

			)", $link);
			$productID = mysql_insert_id($link);
		} else { // we already have the product, so fetch it
			$row = mysql_fetch_assoc($result);
			$productID = $row['id'];
			$update_query = "UPDATE products SET		type=" . intval($productTypeID) . ", 
									ean='" . mysql_real_escape_string($uploadedData[$i]["EAN"]) . "', 
									image='" . mysql_real_escape_string($image_to_use) . "', 
									brand=" . intval($brandID) . ",  
									measure='" . mysql_real_escape_string($uploadedData[$i]['Measure']) . "', 
									uom='" . mysql_real_escape_string($uploadedData[$i]["UOM"]) . "',  
									aisle_id=" . intval($aisleID) . ",
									name='" . mysql_real_escape_string($uploadedData[$i]['Name']) . "',
									technical_name='" . mysql_real_escape_string($uploadedData[$i]['TechnicalName']) . "',
									description='" . mysql_real_escape_string($uploadedData[$i]['Description']) ."',
									highlighted='" . mysql_real_escape_string($uploadedData[$i]['highlighted']) ."' 
							WHERE id=".intval($productID);
			
			// update our product with the new information (ie if the brand, description, etc has changed
			$result = mysql_query_and_log($i, $update_query, $link);
			
			
		}

		/*
		 * If the price is not valid, set it to equal the special price
		 */
		$is_valid_price = true;
		$the_price = $uploadedData[$i]['Price'];
		$the_special = $uploadedData[$i]['Special'];
		$is_valid_price = ($is_valid_price && is_numeric($the_price));
		$is_valid_price = ($is_valid_price && ($the_price > 0));
		$unit_price = @round(($the_price / mysql_real_escape_string($uploadedData[$i]['Measure'])),2);
		if (! $is_valid_price) {
			$uploadedData[$i]['Price'] = $uploadedData[$i]['Special'];
		}
		
		// Again, for each possible state...
		$state_postcode_map = GetStatePostcodeMap();
		foreach ($state_postcode_map as $state_postcode => $state_name) {
			$postcode_key = 'PostCode_' . $state_postcode;
			$postcode_is_selected = $uploadedData[$i][$postcode_key];
			
			if (! $postcode_is_selected) {
				continue;
			}
			
			// add our sale to the db... or are we just updating it?
			/*
			$query = "SELECT * FROM product_sales WHERE product_id=" . intval($productID) . " AND supermarket_id=" . intval($supermarketID) . " AND postcode=" . $state_postcode;
			$result = mysql_query_and_log($i, $query, $link);
	
			if (mysql_num_rows($result) == 0) {
				$result = mysql_query_and_log($i, "INSERT INTO product_sales (	product_id, 
											supermarket_id, 
											postcode,
											start_date,
											end_date,
											price, 
											special_price
								) VALUES (	" . intval($productID) . ", 
										" . intval($supermarketID) . ", 
										" . $state_postcode . ", 
										'" . $_POST['sale_start_date'] . "',
										'" . $_POST['sale_end_date'] . "',
										'" . mysql_real_escape_string($uploadedData[$i]['Price']) . "', 
										'" . mysql_real_escape_string($uploadedData[$i]['Special']) . "'
								)", $link);
			} else {
				$result = mysql_query_and_log($i, "UPDATE product_sales SET
										start_date='" . $_POST['sale_start_date'] . "', end_date='" . $_POST['sale_end_date'] . "',
										price='" . mysql_real_escape_string($uploadedData[$i]['Price']) . "',
										special_price='" . mysql_real_escape_string($uploadedData[$i]['Special']) . "'
										WHERE product_id=" . intval($productID). " AND supermarket_id=" . intval($supermarketID). " AND postcode=" . $state_postcode, $link);
			}
			*/
			
			// added_csv_line_number is ($i + 2) because we offset the zero index, as well as the header line of the CSV file
			$result = mysql_query_and_log($i, "INSERT INTO product_sales (	product_id, 
										supermarket_id, 
										postcode,
										start_date,
										end_date,
										price, 
										special_price,
										unit_price,
										added_datetime,
										added_by_user_id,
										added_session_id,
										added_csv_filename,
										added_csv_line_number
							) VALUES (	" . intval($productID) . ", 
									" . intval($supermarketID) . ", 
									" . $state_postcode . ", 
									'" . $_POST['sale_start_date'] . "',
									'" . $_POST['sale_end_date'] . "',
									'" . mysql_real_escape_string($uploadedData[$i]['Price']) . "', 
									'" . mysql_real_escape_string($uploadedData[$i]['Special']) . "',
									'" .$unit_price."',
									NOW(),
									" . $session->login . ",
									'" . mysql_real_escape_string(session_id()) . "', 
									'" . mysql_real_escape_string($_SESSION['uploadedSavedFilename']) . "', 
									" . ($i + 2) . " 
							)", $link);
		}
		
 		$row++;
 		$count++;
	}

	$message = "The file ".$_SESSION['uploadedOriginalFilename']." has been successfully uploaded to the database.";
	
	$maybe_save_filename = (PATH_TO_CSV_FILES . $_SESSION['uploadedSavedFilename'] . '.save');

	if (file_exists($maybe_save_filename)) {
		unlink($maybe_save_filename);
	}
	
	// Let's save a copy of what we ended up with...
	$handle = fopen(PATH_TO_CSV_FILES . $_SESSION['uploadedSavedFilename'] . '.final.csv', 'w');
	$csv_header_row = array(
			'Retailer', 'NSW (2000)', 'VIC(3000)', 'QLD(4000)', 'SA(5000)', 'WA(6000)', 'TAS(7000)', 'NT(0800)', 
			'EAN', 'Category', 'Product_Type', 'Name', 'DLibrary Name', 'name', 'weight', 'measurement', 'description', 
			'image', 'similar', 'RRP', 'Special Price', 'Promo Type', 'highlighted'
		);
	fputcsv($handle, $csv_header_row);
	
	/*
	 * The $uploadedData array doesn't match the same order of data as the original CSV...
	 */
	foreach ($uploadedData as $uploadedDataRow) {
		$new_csv_row = array();
		$new_csv_row[] = $uploadedDataRow['Supermarket'];
		$new_csv_row[] = $uploadedDataRow['PostCode_2000'];
		$new_csv_row[] = $uploadedDataRow['PostCode_3000'];
		$new_csv_row[] = $uploadedDataRow['PostCode_4000'];
		$new_csv_row[] = $uploadedDataRow['PostCode_5000'];
		$new_csv_row[] = $uploadedDataRow['PostCode_6000'];
		$new_csv_row[] = $uploadedDataRow['PostCode_7000'];
		$new_csv_row[] = $uploadedDataRow['PostCode_0800'];
		$new_csv_row[] = $uploadedDataRow['EAN'];
		$new_csv_row[] = ''; // No specified category any more
		$new_csv_row[] = $uploadedDataRow['ProductType'];
		$new_csv_row[] = $uploadedDataRow['Name'];
		$new_csv_row[] = $uploadedDataRow['TechnicalName'];
		$new_csv_row[] = $uploadedDataRow['Brand'];
		$new_csv_row[] = $uploadedDataRow['Measure'];
		$new_csv_row[] = $uploadedDataRow['UOM'];
		$new_csv_row[] = $uploadedDataRow['Description'];
		$new_csv_row[] = $uploadedDataRow['Image'];
		$new_csv_row[] = ''; // No value for 'similar'
		$new_csv_row[] = $uploadedDataRow['Price'];
		$new_csv_row[] = $uploadedDataRow['Special'];
		$new_csv_row[] = $uploadedDataRow['Promo'];
		$new_csv_row[] = $uploadedDataRow['highlighted'];
		
		fputcsv($handle, $new_csv_row);
	}
	fclose($handle);

	// And while we're here, let's save a copy of the queries that we called while saving to the database...
	$handle = fopen(PATH_TO_CSV_FILES . $_SESSION['uploadedSavedFilename'] . '.queries.csv', 'w');
	$csv_header_row = array('Uploaded Row Number', 'Query');
	fputcsv($handle, $csv_header_row);
	
	$_mysql_query_log = mysql_query_and_log(null, null, null, true);
	foreach ($_mysql_query_log as $_mysql_query_log_row) {
		fputcsv($handle, $_mysql_query_log_row);
	}
	fclose($handle);
	
	unset($_SESSION['uploadedDateTime']);
	unset($_SESSION['uploadedOriginalFilename']);
	unset($_SESSION['uploadedSavedFilename']);
}

// function for a fancy dropdown
function GetSelect2Box ($name, $default, $db_display_field, $db_value_field, $db_table, $db_link, &$original_variable, $extra_where="") {
	$html="";
	
	$default = (isset($_POST[$name]) ? $_POST[$name] : $default);
	
	$query = "SELECT * FROM ".$db_table;
	if ($extra_where) {
		$query = $query .' WHERE '.$extra_where;
	}
	$result = mysql_query($query, $db_link);

	$inputStyle = "border: 1px solid #039; padding 2px; width: auto; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;";
	
	if (mysql_num_rows($result)==1) {
		$data = mysql_fetch_assoc($result);
		$original_variable = $data[$db_value_field];
		$html .= '<span id="' . $name . '-readonly-span">' . $original_variable . '</span>';
		$html .= '<input type="hidden" name="'.$name.'" value="'.$original_variable.'" />';
	} else if (mysql_num_rows($result)>0) {
		$html .= '<select class="select2" name="'.$name.'" id="'.$name.'">';	
		$html .= '<option value=""></option>';
		for ($i=0; $i<mysql_num_rows($result); $i++) {
			$data = mysql_fetch_assoc($result);

			$html .= '<option value="'.$data[$db_value_field].'"';
				if ( strtolower(trim($default)) == strtolower(trim($data[$db_display_field] )) ) {
					$html .= ' SELECTED ';
				}
			$html .= '>'.$data[$db_display_field].'</option>';

		}
		$html .= '</select>';
	} else {
		$html .= '<input type="text" class="rounded-corners" name="'.$name.'" id="'.$name.'" value="'.$default.'" style="'.$inputStyle.'" />';
	}
	return $html;
}
?>

<? echo (count($errors)>0 ? '<p class="simple-message simple-message-error">'.implode('<br/>', $errors).'</p>' : '');?>
<? echo (isset($message) ? '<p class="simple-message simple-message-information">'.$message.'</p>' : '');?>

<? if (!$errorsExist) { ?>
<div style="position: relative; width: 100%;text-align: left; margin: 30px 20px 40px 50px;">
	<form enctype="multipart/form-data" action="" name="form" id="form" method="POST">
		Sale Start Date? <input style="border: 1px solid grey;" type="text" name="sale_start_date" id="sale_start_date" value="<?php echo (isset($_POST['sale_start_date']) ? $_POST['sale_start_date'] : GetDefaultSaleDates('start')); ?>" /><br />
		Sale End Date? <input style="border: 1px solid grey;" type="text" name="sale_end_date" id="sale_end_date" value="<?php echo (isset($_POST['sale_end_date']) ? $_POST['sale_end_date'] : GetDefaultSaleDates('end')); ?>" /><br />
		<label><input type="checkbox" name="sale_do_override_postcodes" id="sale_do_override_postcodes" <?php echo ((isset($_POST['sale_do_override_postcodes']) && $_POST['sale_do_override_postcodes']) ? 'checked ' : ''); ?>/>
		Force one or more postcode/s on all of the sales to be uploaded?</label><br />
		
		<div id="sale_do_override_postcodes_div">
		<?php 
		$state_postcode_map = GetStatePostcodeMap();
		foreach ($state_postcode_map as $state_postcode => $state_name) {
			$this_state_postcode_field_name = 'sale_override_postcode_' . $state_postcode;
			
			echo '<label><input type="checkbox" name="' . $this_state_postcode_field_name . '" id="' . $this_state_postcode_field_name . '" ';
			if (isset($_POST[$this_state_postcode_field_name]) && $_POST[$this_state_postcode_field_name]) {
				echo 'checked ';
			}
			echo '/>' . $state_name . ' - ' . $state_postcode . '</label><br />';
		}
		?>
		</div>
		
		<script>
		function HideOrShowStateCheckboxes() {
			if ($('#sale_do_override_postcodes').attr('checked')) {
				$('#sale_do_override_postcodes_div').show();
			} else {
				$('#sale_do_override_postcodes_div').hide();
			}
		}
		
		$(document).ready(function() {
			HideOrShowStateCheckboxes();

			$('#sale_do_override_postcodes').change(function() {
				HideOrShowStateCheckboxes();
			});
		});
		</script>
		<input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
		Choose a file to upload: <br><br><input name="uploadedfile" style="" type="file" /><br /><br />
		<input type="hidden" name="saved_filename" id="saved_filename" value="" />
		<input type="hidden" name="process" value="1" />
		<div class="input">
		<input type="submit" id="csv_submit" name="csv_submit" value="Upload File" style="display:inline" />
		</div>
		<?php 
		$save_files = glob(PATH_TO_CSV_FILES . '*.save');
		
		if (count($save_files)) {
			?>
			There are some files that have not been completely processed: click any of the files listed
			below to continue them:<br />
			
			<script>
			function SubmitUseSavedFile(the_filename) {
				$('#saved_filename').val(the_filename);
				$('#form').submit();
			}
			</script>
			<ul>
			<?php 
			foreach ($save_files as $save_file) {
				$save_file = str_replace(PATH_TO_CSV_FILES, '', $save_file);
				$save_file_to_submit = $save_file;
				$save_file_datetime_string = substr($save_file, 0, 16);
				$save_file = substr($save_file, 16, -5);
				
				$save_file_datetime = substr($save_file_datetime_string, 6, 2) . '/';
				$save_file_datetime .= substr($save_file_datetime_string, 4, 2) . '/';
				$save_file_datetime .= substr($save_file_datetime_string, 0, 4) . ' ';
				$save_file_datetime .= substr($save_file_datetime_string, 9, 2) . ':';
				$save_file_datetime .= substr($save_file_datetime_string, 11, 2) . ':';
				$save_file_datetime .= substr($save_file_datetime_string, 13, 2);
				echo '<li>';
				echo '<a href="#" onclick="SubmitUseSavedFile(\'' . $save_file_to_submit . '\'); return false;">';
				echo $save_file;
				echo '</a>';
				echo ' (uploaded ' . $save_file_datetime . ')';
				echo '</li>';
			}
			echo '</ul>';
		}
		?>
	</form>
</div>
<? } else if (isset($uploadedData) && is_array($uploadedData)) { ?>
<div style="position: relative; width: 100%;text-align: left; margin: 30px 20px 20px 50px;">
	You have successfully uploaded your file, but some of the rows are missing some data.  Please fill out the required information below.
</div>
<div id="overlay"></div>
<div id="ajax-lookups" style="display: none;">
	<input type="button" value="Close" onclick="CloseAJAXLookups();" />
	<span id="ajax-lookups-description"></span><span id="ajax-lookups-loader"></span><br />
	<div id="ajax-lookups-result"></div><br />
</div>
<script>
function CloseAJAXLookups() {
	$('#ajax-lookups-description').html('');
	$('#ajax-lookups-loader').html('');
	$('#ajax-lookups-result').html('');

	$('#ajax-lookups').fadeOut('fast');
	$('#overlay').fadeOut('fast');
	//$('#ajax-lookups').css('visibility', 'hidden');
	
	if (theAJAX != null) {
		theAJAX.abort();
		theAJAX = null;
	}
}

var theAJAX = null;
var theAJAXTimer = null;
function GetMatchingProducts(row_id, use_dlibrary) {
	$('#ajax-lookups').css({
		/*'margin-left': '-'+$('#ajax-lookups').width()/2+'px',
		'margin-top': '-'+$('#ajax-lookups').height()/2+'px'*/
	});
	$('#ajax-lookups').fadeIn('fast');
	//$('#ajax-lookups').css('visibility', 'visible');
	$('#overlay').fadeIn('fast');

	var simple_name = $('input[name=Name-' + row_id + ']').val().trim();
	var technical_name = $('input[name=TechnicalName-' + row_id + ']').val().trim();
	// If given a technical name, use it. Otherwise, the simple name.
	var product_name = ((technical_name != '') ? technical_name : simple_name);
	
	var supplier = $('input[name=Brand-' + row_id + ']').val();
	$('#ajax-lookups-description').html('Looking up "' + product_name + '" products by "' + supplier + '"<br/>');
	theAJAXTimer = (new Date().getTime() / 1000);

	theAJAX = $.ajax({
					type: 'POST',
					url: '../fieclient/fie_ajax.php',
					data: {
						action: 'get_matching_products',
						supplier: supplier,
						product_name: product_name,
						use_dlibrary: (use_dlibrary ? '1' : '0')
					},
					dataType: 'json',
					
					success: function(data) {
						var message = '';
						if (data.success) {
							message = 'Results for lookup in ' + (use_dlibrary ? 'DLibrary' : 'database') + '<br />';
							
							if (data.result.length == 0) {
								message += 'No products were found that match your request.<br />';

								if (use_dlibrary) {
									message += '<span id="fake-ean-option">';
									message += '[<a href="#" onclick="javascript:MaybeInsertFakeEAN(\'' + row_id + '\'); return false;">';
									message += 'Use fake EAN';
									message += '</a>]';
									message += '</span>';
								}
							} else {
								message += 'The following products match your request:<br />';
								for (var i in data.result) {
									var escaped_description = escape(data.result[i].description);
									message += '<span class="ean-num" onclick="javascript:SetProductDetails(';
									message += "'" + row_id + "', ";
									message += "'" + data.result[i].ean + "', ";
									if (use_dlibrary) {
										message += "null, null, ";
									} else {
										message += data.result[i].aisle_id + ", '" + data.result[i].product_type_name + "', ";
									}
									message += "'" + escaped_description + "');";
									message += ' return false;">' + data.result[i].ean + '</span>: ' + data.result[i].description + '<br />';
								}
							}

							if (! use_dlibrary) {
								message += '<span id="force-dlibrary-option">';
								message += '[<a href="#" onclick="javascript:GetMatchingProducts(\'' + row_id + '\', true); return false;">';
								message += 'Lookup in DLibrary';
								message += '</a>]';
								message += '</span>';
							}
						} else {
							message = 'There was an error processing your request: ';
							message += '"' + data.error_text + '"';
						}

						$('#ajax-lookups-loader').html('');
						$('#ajax-lookups-result').html(message);
						theAJAX = null;
						$('.scroll-bar').mCustomScrollbar("update");
					}
				});
	
	UpdateLookupLoader();
}

function SetProductDetails(row_id, ean_value, aisle_id, product_type_name, description_value) {
	$('input[name=EAN-' + row_id + ']').val(ean_value); 
	$('input[name=Description-' + row_id + ']').val(unescape(description_value)); 
	
	// Reset the images...
	$('input[name=Image-' + row_id + ']').val('');
	$('#Image-' + row_id + '-label').html('No');
	$('input[name=Image-Default-' + row_id + ']').attr('checked', true);
	$('#Image-Default-' + row_id + '-span').css('display', '');

	if (aisle_id) {
		$('#Aisle-' + row_id).select2('val', aisle_id);
		SetProductTypeDropdownFromAisle('Aisle-' + row_id);
	}

	if (product_type_name) {
		$('input[name=ProductType-' + row_id + ']').select2('val', product_type_name);
	}
	
	ShowOrGetImageForRow(row_id);
}

function ShowOrGetImageForRow(the_row_id) {
	the_ean = $('input[name=EAN-' + the_row_id + ']').val();
	the_image = $('input[name=Image-' + the_row_id + ']').val();
	
	if (the_image != '') {
		ShowImageForRow(the_row_id);
	} else {
		GetImageForRow(the_row_id);
	}
}

function ShowImageForRow(the_row_id) {
	the_ean = $('input[name=EAN-' + the_row_id + ']').val();
	the_image = $('input[name=Image-' + the_row_id + ']').val();
	
	$('#ajax-lookups').fadeIn('fast');
	$('#overlay').fadeIn('fast');
	$('#ajax-lookups-description').html('Image for EAN ' + the_ean);
	$('#ajax-lookups-loader').html('');
	$('#ajax-lookups-result').html('<img src="../images/' + the_image + '" />');
}

function GetImageForRow(the_row_id) {
	the_ean = $('input[name=EAN-' + the_row_id + ']').val();
	the_image = $('input[name=Image-' + the_row_id + ']').val();

	if (the_ean == '') {
		alert('A valid EAN is required before looking for an image');
		return;
	}

	$('#ajax-lookups').fadeIn('fast');
	$('#overlay').fadeIn('fast');
	$('#ajax-lookups-description').html('Looking for image for EAN ' + the_ean + '<br />');
	$('#ajax-lookups-loader').html('');
	theAJAXTimer = (new Date().getTime() / 1000);

	theAJAX = $.ajax({
					type: 'POST',
					url: '../fieclient/fie_ajax.php',
					data: {
						action: 'get_image_for_ean',
						ean: the_ean
					},
					dataType: 'json',
					
					success: function(data) {
						var message = '';
						if (data.success) {
							message = 'The following image was loaded for this product:<br />';
							message += '<img src="../images/' + data.result + '" />';
							$('input[name=Image-' + the_row_id + ']').val(data.result);
							$('#Image-' + the_row_id + '-label').html('Yes');
							$('input[name=Image-Default-' + the_row_id + ']').attr('checked', false);
							$('#Image-Default-' + the_row_id + '-span').css('display', 'none');
						} else {
							message = 'There was an error processing your request: ';
							message += '"' + data.error_text + '"';
							$('input[name=Image-' + the_row_id + ']').val('');
							$('input[name=Image-Default-' + the_row_id + ']').attr('checked', true);
						}

						if (data.result == '') {
							message += '<br /><br />If you would like to upload your own file for this EAN, ';
							message += 'choose it below and click "Upload"';
							message += '<br />';

							message += '<form id="upload_file_for_ean_form" method="post" enctype="multipart/form-data"  action="../ajax-upload-for-ean.php">';  
							message += '<input type="file" name="uploaded_file" id="uploaded_file" />';
							message += '<input type="hidden" name="the_ean" id="the_ean" value="' + the_ean + '" />';
							message += '<br />';
							message += '<input type="button" class="button_submit" value="Upload" onclick="UploadFileForEAN(' + the_row_id + ', ' + the_ean + ');" />';
							message += '</form>';
							message += '<div id="upload_for_ean_result"></div>';
						}
						
						$('#ajax-lookups-loader').html('');
						$('#ajax-lookups-result').html(message);
						theAJAX = null;
					}
				});
	
	UpdateLookupLoader();
	
	$('#ajax-lookups-result').html('<img src="../images/' + the_image + '" />');
}

function UploadFileForEAN(the_row_id, the_ean) {
	$('#upload_for_ean_result').html('Uploading');
	theAJAXTimer = (new Date().getTime() / 1000);
	var the_file = document.getElementById('uploaded_file').files[0];

	if (the_file.name.length == 0) {
		alert('Please select a file before uploading it');
		return;
	}

	var formData = new FormData($('#upload_file_for_ean_form')[0]);
	theAJAX = $.ajax({
					type: 'POST',
					url: '../ajax-upload-for-ean.php',
                    data: formData,
                    //Options to tell JQuery not to process data or worry about content-type
                    cache: false,
                    contentType: false,
                    processData: false,
					dataType: 'json',
					
					success: function(data) {
						var message = '';
						if (data.success) {
							message = 'The following image was uploaded for this product:<br />';
							message += '<img src="../images/' + data.result + '" />';
							$('input[name=Image-' + the_row_id + ']').val(data.result);
							$('#Image-' + the_row_id + '-label').html('Yes');
							$('input[name=Image-Default-' + the_row_id + ']').attr('checked', false);
							$('#Image-Default-' + the_row_id + '-span').css('display', 'none');
						} else {
							message = 'There was an error processing your request: ';
							message += '"' + data.error_text + '"';
							$('input[name=Image-' + the_row_id + ']').val('');
							$('input[name=Image-Default-' + the_row_id + ']').attr('checked', true);
						}
						
						$('#ajax-lookups-loader').html('');
						$('#ajax-lookups-result').html(message);
						theAJAX = null;
					}
				});
	
	UpdateLookupLoader();
}

function UpdateLookupLoader() {
	if (! theAJAX) {
		return;
	}
	
	var now = (new Date().getTime() / 1000);
	var dots = ((now - theAJAXTimer) % 3);

	var dotsString = '';
	for (var i = 0; (i < dots); i++) {
		dotsString += '.';
	}
	
	$('#ajax-lookups-loader').html(dotsString);
	setTimeout(UpdateLookupLoader, 1000);
}

function IsValidEAN(the_ean) {
	var the_length = the_ean.length;
	var the_regexp = /^[0-9]+$/;

	if ((the_length == 8) || (the_length == 13)) {
		if (the_regexp.exec(the_ean) == the_ean) {
			return true;
		}
	}

	return false;
}

function ValidateEANAndGetImage(the_row_id) {
	var the_ean = $('input[name=EAN-' + the_row_id + ']').val();

	if (the_ean == '') {
		return true;
	}
	
	if (IsValidEAN(the_ean)) {
		GetImageForRow(the_row_id);
		return true;
	}

	alert('The given value "' + the_ean + '" is not a valid EAN.');
	$('input[name=EAN-' + the_row_id + ']').focus();
	return false;
}
</script>
<form enctype="multipart/form-data" action="" name="form" id="form" method="POST">
<?php 
// Start by splitting up the data into "things that have passed" and
// things that are still not valid...
$valid_rows = array();
$invalid_rows = array();

foreach ($uploadedData as $i => $data) {
	if (isset($missingEANs[$i]) || isset($missingAisles[$i]) || isset($missingProductTypes[$i]) || isset($missingImages[$i])) {
		$invalid_rows[$i] = $data;
	} else {
		$valid_rows[$i] = $data;
	}
}

if (count($invalid_rows)) {
	// As we loop through these rows, we're going to be trying to see if the given brand
	// is actually one that we already have. Therefore, we should just do the query the
	// one time.
	$brands_array = array();
	$result = mysql_query("SELECT * FROM brands ORDER BY id ASC");
	
	while ($row = mysql_fetch_assoc($result)) {
		$brands_array[intval($row['id'])] = CleanUploadedString($row['name']);
	}
	
	$select2_brands_array = array();
	foreach ($brands_array as $brand_name) {
		$brand_name = CleanUploadedString($brand_name);
		$select2_brands_array[] = array('id' => $brand_name, 'text' => $brand_name);
	}
	?>
	<script>
		var aisle_product_types = new Array();
		var next_fake_ean_to_use = <?php echo GetNextFakeEANTOUse(); ?>;
		<?php
		$aisle_map = GetAisleMap();
		
		foreach ($aisle_map as $aisle_id => $aisle_value) {
			$aisle_product_type_names = array();
			$aisle_product_types = GetProductTypesForAisle($aisle_id);
			
			if (count($aisle_product_types)) {
				foreach ($aisle_product_types as $aisle_product_type_information) {
					$aisle_product_type_names[] = $aisle_product_type_information['name'];
				}
				
				echo 'aisle_product_types[' . $aisle_id . '] = ["' . implode('", "', $aisle_product_type_names) . '"];' . "\n";
			}
		}			
		?>
		function SetProductTypeDropdownFromAisle(the_dropdown_id) {
			the_dropdown_id = the_dropdown_id.replace('Aisle-', 'ProductType-');
			SetDataForProductTypeDropdown(the_dropdown_id, true);
		}
		
		function SetDataForProductTypeDropdown(the_dropdown_id, reset_value) {
			var aisle_id = the_dropdown_id.replace('ProductType-', 'Aisle-');
			var current_aisle = parseInt($("#" + aisle_id).val());
			var product_types_to_use = [{id: "", text: ""}];

			for (var i in aisle_product_types[current_aisle]) {
				var this_product_type = aisle_product_types[current_aisle][i];
				product_types_to_use.push({id: this_product_type, text: this_product_type});
			}
			
			if (reset_value) {
				$("#" + the_dropdown_id).val("");
			}

			$("#" + the_dropdown_id).select2({
				width: '100px',
				dropdownCss: { width: '200px' },
				data: product_types_to_use,

				initSelection: function (element, callback) {
					var data = new Object();
					var the_value = element.val();
					data.id = the_value;
					data.text = the_value;

					callback(data);
				}
			});

		}

		function MaybeDeleteRow(the_row_id) {
			if (confirm('Are you sure you wish to remove this row?')) {
				$('#uploaded-list-' + the_row_id).remove();
				return true;
			} else {
				return false;					
			}
		}

		function MaybeInsertFakeEAN(the_row_id) {
			if (confirm('Would you like to assign the fake EAN "' + next_fake_ean_to_use + '" to this product?')) {
				$('#EAN-' + the_row_id).val(next_fake_ean_to_use);
				$('#fake-ean-option').html('Using "' + next_fake_ean_to_use + '" as the EAN.');
				next_fake_ean_to_use--;
			}
		}
		
		$(document).ready(function() {
			$(".brand-select").select2({
				width: '100px',
				dropdownCss: { width: '200px' },
				data: <?php echo json_encode($select2_brands_array); ?>,
						
				createSearchChoice: function(term, data) {
					if ($(data).filter(function() {
						return this.text.localeCompare(term)===0;
					}).length===0) {
						return {id:term, text:term};
					}
				},

				initSelection: function (element, callback) {
					if (element.val() != '') {
						var data = new Object();
						var the_value = element.val();
						data.id = the_value;
						data.text = the_value;

						callback(data);
					}
				}				
			});

			$(".aisle-select").select2({
				width: '100px',
				dropdownCss: { width: '250px' }
			});

			$(".product-type-select").each(function () {
				SetDataForProductTypeDropdown(this.id, false);
			});
		});
	</script>
	
	<table id="hor-minimalist-b" summary="Employee Pay Sheet">
		<thead>
		<tr>
			<th scope="col">&nbsp;</th>
			<th scope="col">Supermarket</th>
			<?php 
			$state_postcode_map = GetStatePostcodeMap();
			foreach ($state_postcode_map as $state_postcode => $state_name) {
				echo '<th scope="col">' . $state_name . '</th>' . "\n";
			}
			?>
			<th scope="col">EAN</th>
			<th scope="col">Image?</th>
			<th scope="col">Aisle</th>
			<th scope="col">Product Type</th>
			<th scope="col">Name</th>
			<!--<th scope="col">Technical Name</th>-->
			<th scope="col">Brand</th>
			<th scope="col">Measure</th>
			<th scope="col">UOM</th>
			<!--<th scope="col">Description</th>-->
			<th scope="col">Price</th>
			<th scope="col">Special</th>
			<!--<th scope="col">Promo</th>-->
			<th scope="col">Highlighted</th>
		</tr>
		</thead>
		<tbody>
		<?php
			$state_postcode_map = GetStatePostcodeMap();
			
			$read_write_input_style = 'style="border: 1px solid black;"';
			foreach ($invalid_rows as $i => $data) {
				echo '<tr id="uploaded-list-' . $i .'">' . "\n";

				echo '<td>' . "\n";
				echo '[<a href="#" onclick="MaybeDeleteRow(' . $i . '); return false;">X</a>]' . "\n";
				echo '</td>' . "\n";
				
				// Some of these elements are editable... some are not.
				echo '<td>' . "\n";
				echo '<input type="text" name="Supermarket-' . $i . '" value="' . $data["Supermarket"] . '" readonly />' . "\n";
				echo '</td>' . "\n";
				
				
				foreach ($state_postcode_map as $state_postcode => $state_name) {
					echo '<td>' . "\n";
					echo '<input type="checkbox" name="PostCode_' . $state_postcode . '-' . $i . '" value="1" readonly ';
					echo ($data["PostCode_" . $state_postcode] ? 'checked ' : '');
					echo '/>' . "\n";
					echo '</td>' . "\n";
				}
								
				echo '<td>' . "\n";
				if (! isset($missingEANs[$i])) {
					echo '<input type="text" name="EAN-' . $i . '" value="' . $data["EAN"] . '" readonly />' . "\n";
				} else {
					/*
					// get brand id
					// if the brand doesn't exist, create it
					// grab the ID either way
					$result = mysql_query("SELECT * FROM brands WHERE name='" . mysql_real_escape_string($data['Brand']) . "'", $link);

					if (mysql_num_rows($result)==0) {
						$result = mysql_query("INSERT INTO brands (name) VALUES ('" . mysql_real_escape_string($data['Brand']) . "')", $link);
						$brandID = mysql_insert_id($link);
					} else {
						$row = mysql_fetch_assoc($result);
						$brandID = $row['id'];
					}
					
					$where = "brand = '".intval($brandID)."' AND description LIKE '%".mysql_real_escape_string($data["Description"])."%'";
					echo GetSelect2Box ('EAN-'.$i, '', 'ean', 'ean', 'products', $link, $data["EAN"], $where);
					*/
					echo '<input type="text" maxlength="13" size="13" ' . $read_write_input_style . ' id="EAN-' . $i . '" name="EAN-' . $i . '" value="' . $data["EAN"] . '" onchange="return ValidateEANAndGetImage(\'' . $i . '\');" />' . "\n";
					
					// Add the lookup popup
					echo '[<a href="#" onclick="javascript:GetMatchingProducts(\'' . $i . '\', false); return false;">?</a>]';
				}
				echo '</td>';
					
				// Show whether or not we have an image for this EAN...
				echo '<td>';
				$image_name = ((isset($existingImages[$i]) && $existingImages[$i]) ? $existingImages[$i] : '');
				echo '<input type="hidden" name="Image-' . $i . '" value="' . $image_name . '" />';
				echo '<a href="#" onclick="javascript:ShowOrGetImageForRow(' . $i . '); return false;">';
				echo '<span id="Image-' . $i . '-label">';
				echo (isset($missingImages[$i]) ? 'No' : 'Yes');
				echo '</span>';
				echo '</a>';
				if (isset($missingImages[$i])) {
					echo '<br />';
					echo '<span id="Image-Default-' . $i . '-span">';
					echo '<label><input type="checkbox" name="Image-Default-' . $i . '" ';
					echo 'id="Image-Default-' . $i . '" value="1" ';
					echo 'checked /> Default</label>';
					echo '</span>';
				}
				echo '</td>';
					
				echo '<td>';
				echo '<select name="Aisle-' . $i . '" id="Aisle-' . $i . '" class="aisle-select" onchange="SetProductTypeDropdownFromAisle(this.id);">';
				echo '<option value=""' . (($data['Aisle'] == 0) ? ' selected' : '') . '>&nbsp;</option>';
				$aisle_map = GetAisleMap();
				
				/*
				 * If we were given a product type and no aisle, and that product type belongs
				 * to more than one aisle, then we need to obviously have the user select the
				 * aisle. What we will do, though, is highlight this fact and show the appropriate
				 * aisles first...
				 */
				$possible_aisles_for_product_type = array();
				if (! $data['Aisle']) {
					$product_type_information = GetProductTypeForName($data['ProductType']);
					
					foreach ($product_type_information as $product_type_row) {
						$possible_aisles_for_product_type[] = $product_type_row['aisle_id'];
					}
				}

				// If we have some aisles that are possible, we will break the list up into two...
				if (count($possible_aisles_for_product_type)) {
					echo '<optgroup label="Aisles with Product Type">';
					foreach ($aisle_map as $aisle_key => $aisle_value) {
						if (in_array($aisle_key, $possible_aisles_for_product_type)) {
							echo '<option value="' . $aisle_key . '"' . (($data['Aisle'] == $aisle_key) ? ' selected' : '') . '>';
							echo $aisle_value;
							echo '</option>';
						}
					}
					echo '</optgroup>';
				}

				if (count($possible_aisles_for_product_type)) {
					echo '<optgroup label="Aisles without Product Type">';
				}
				
				foreach ($aisle_map as $aisle_key => $aisle_value) {
					if (! in_array($aisle_key, $possible_aisles_for_product_type)) {
						echo '<option value="' . $aisle_key . '"' . (($data['Aisle'] == $aisle_key) ? ' selected' : '') . '>';
						echo $aisle_value;
						echo '</option>';
					}
				}
				
				if (count($possible_aisles_for_product_type)) {
					echo '</optgroup>';
				}
				echo '</select>';
				echo '</td>';

				echo '<td>' . "\n";
				echo '<input type="text" name="ProductType-' . $i . '" id="ProductType-' . $i . '" class="product-type-select" value=';
				echo '"' . (isset($missingProductTypes[$i]) ? '' : $data["ProductType"]) . '"';
				echo ' readonly />' . "\n";
				echo '</td>' . "\n";
				echo '<td>' . "\n";
				echo '<input name="Name-'.$i.'" value="'.$data['Name'].'" />' . "\n";
				echo '<input type="hidden" name="TechnicalName-'.$i.'" value="'.$data['TechnicalName'].'" />' . "\n";
				echo '</td>' . "\n";

				echo '<td>' . "\n";
				// Brand value
				// If the brand already exists, we just use it. Otherwise, we change the type and
				// add a class, to allow Select2 to make it a dropdown for us. Also, let's try
				// and be smart about case-sensitivity. Therefore, if the brand exists but with
				// a different casing, change the data to use that...
				$brand_exists = false;
				foreach ($brands_array as $brand_value) {
					if (strtolower($brand_value) == strtolower($data["Brand"])) {
						$brand_exists = true;
						$data["Brand"] = CleanUploadedString($brand_value);
						break;
					}
				}
				
				$brand_exists = in_array($data["Brand"], $brands_array);
				$brand_input_type = ($brand_exists ? 'text' : 'hidden');
				$brand_input_class_string = ($brand_exists ? '' : 'class="brand-select" ');
				
				echo '<input type="' . $brand_input_type . '" ';
				echo $brand_input_class_string;
				echo 'name="Brand-' . $i . '" value="' . $data["Brand"] . '" readonly />' . "\n";
				
				echo '<input type="hidden" name="Description-'.$i.'" value="'.$data['Description'].'" />' . "\n";
				echo '</td>' . "\n";

				echo '<td>' . "\n";
				echo '<input type="text" size="6" name="Measure-' . $i . '" value="' . $data["Measure"] . '" readonly />' . "\n";
				echo '</td>' . "\n";
				
				echo '<td>' . "\n";
				echo '<input type="text" size="6" name="UOM-' . $i . '" value="' . $data["UOM"] . '" readonly />' . "\n";
				echo '</td>' . "\n";

				echo '<td>' . "\n";
				echo '<input type="text" size="6" name="Price-' . $i . '" value="' . $data["Price"] . '" readonly />' . "\n";
				echo '</td>' . "\n";

				echo '<td>' . "\n";
				echo '<input type="text" size="6" name="Special-' . $i . '" value="' . $data["Special"] . '" readonly />' . "\n";
				echo '<input type="hidden" name="Promo-'.$i.'" value="'.$data['Promo'].'" />' . "\n";
				echo '</td>' . "\n";
				echo '<td>' . "\n";
				echo '<input type="text" size="6" name="highlighted-' . $i . '" value="' . $data["highlighted"] . '" readonly />' . "\n";
				echo '</td>' . "\n";
				echo '</tr>';
			}
			
			
		?>
		</tbody>
	</table>
<?php 
}

foreach ($valid_rows as $i => $data) {
	foreach ($data as $data_key => $data_value) {
		echo '<input type="hidden" name="' . $data_key . '-' . $i . '" value="' . $data_value. '" />' . "\n";
	}
}
?>
<div style="position: relative; width: 100%;text-align: left; margin: 30px 20px 40px 50px;">
<?php
echo '<input type="hidden" name="sale_start_date" value="' . $_POST['sale_start_date'] . '" />';
echo '<input type="hidden" name="sale_end_date" value="' . $_POST['sale_end_date'] . '" />';
echo '<input type="hidden" name="sale_do_override_postcodes" value="' . $_POST['sale_do_override_postcodes'] . '" />';
echo '<input type="hidden" name="sale_override_postcode_2000" value="' . $_POST['sale_override_postcode_2000'] . '" />';
echo '<input type="hidden" name="sale_override_postcode_3000" value="' . $_POST['sale_override_postcode_3000'] . '" />';
echo '<input type="hidden" name="sale_override_postcode_4000" value="' . $_POST['sale_override_postcode_4000'] . '" />';
echo '<input type="hidden" name="sale_override_postcode_5000" value="' . $_POST['sale_override_postcode_5000'] . '" />';
echo '<input type="hidden" name="sale_override_postcode_6000" value="' . $_POST['sale_override_postcode_6000'] . '" />';
echo '<input type="hidden" name="sale_override_postcode_7000" value="' . $_POST['sale_override_postcode_7000'] . '" />';
echo '<input type="hidden" name="sale_override_postcode_0800" value="' . $_POST['sale_override_postcode_0800'] . '" />';
echo '<input type="hidden" name="numRows" value="'.count($uploadedData).'" />';
echo '<input type="hidden" name="process" value="1" />';
echo '<input type="submit" id="csv_submit" name="csv_submit" value="Submit" style="display:inline" />';
echo '<input type="submit" id="csv_save" name="csv_save" class="button_submit" value="Save File" style="display:inline" />';
?>
</div>
</form>
<? } ?>