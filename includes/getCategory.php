<?php 
	$aisle_id = $_POST['aisle_id'];
	$level = $_POST['level'];
	require_once(dirname(__FILE__) . '/../functions.php');
	require_once '../config.php'; //config to connect for poduct info not users info
	$demographics = GetDemographicsOfProductsForSale();
	$categories_with_no_product_types = GetCategoriesWithNoProductTypes();
	
	$db_link = new MySQLi (DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	$query = "SELECT category.id AS id, category_name, aisle_id, aisle_name 
				FROM category 
				INNER JOIN aisle ON aisle.id=aisle_id
				WHERE $aisle_id = aisle_id 
				ORDER BY category_name ASC";

	$results = $db_link->prepare($query);
	$results->bind_result($id, $name, $aisle_id, $aisle_name);
	$results->execute();
	$results->store_result();
	$row_cnt = $results->num_rows;
?>
<?php
						
			$output['html'] = '<div id="category" class="browse-list rounded-corners">';
			$output['html'] .= 	'<ul class="category  rounded-corners">';
			if (empty($row_cnt)):
				$output['result'] = false;
				$output['html'] .= '<li>Sorry there are no categories or products here</li>';
			else:
				$output['result'] = true;
				while ($results->fetch())
				{
					$aisle_name = RewriteSmartQuotes($aisle_name);
					$name = RewriteSmartQuotes($name);
					
					// Is this category one of those categories that has no product type underneath it?
					if (isset($categories_with_no_product_types[intval($id)])) {
						continue;
					}
					
					$hash_url = ('#' . $aisle_id . '-' . $id);
					$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
					$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $name);
					$sale_class = (isset($demographics['categories'][intval($id)]) ? 'has-sales' : 'has-no-sales');
					
					$output['html'] .=		'<li><a href="' . $hash_url . '" class="' . $id . ' ' . $sale_class . '">'.ucwords($name).'</a></li>';
				}
			endif;
			$output['html'] .=	'</ul>';
			$output['html'] .= '</div>';
			
			echo json_encode($output);
?>
