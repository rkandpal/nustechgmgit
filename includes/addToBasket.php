<?php
	//session_start();
	require_once(dirname(__FILE__).'/../config.php');
	require_once(dirname(__FILE__).'/../functions.php');
	StartSession();
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Unable to connect to database: '.$db_link->error);
	
	$current_list = (isset($_SESSION['list_id']) ? $_SESSION['list_id'] : get_last_id());
	//$current_user = $_SESSION['user_id'];
	
	$product_sale_id = intval($_POST['product_sale_id']);
	$product_count = null;
	$product_add = null;
	
	$current_product_count = 0;
	$target_product_count = 0;
	
	if (isset($_POST['product_count'])) {
		$product_count = intval($_POST['product_count']);
	}
	if (isset($_POST['product_add'])) {
		$product_add = intval($_POST['product_add']);
	}
	
	if ($product_count && $product_add) {
		die('Cannot pass a product count and a product add field at the same time.');
	}

	// To deal with the 'legacy' approach...
	if ((! $product_count) && (! $product_add)) {
		$product_add = 1;
	}
	
	//Get the data on the product
	$query = "SELECT product_sales.product_id, product_sales.supermarket_id, product_sales.postcode, product_sales.price, product_sales.special_price,
						products.type, products.brand, products.measure, products.uom, products.image,
						products.name, products.description
					FROM products INNER JOIN product_sales ON product_sales.product_id=products.id
					WHERE product_sales.id=$product_sale_id";

	$result = $db_link->query($query);
	if (! $result->num_rows) {
		die('Failed to store product in shopping list: '.$db_link->error);
	}

	// At least we have a product...
	$row = $result->fetch_assoc();
	$product_id = $row['product_id'];
	$supermarket = $row['supermarket_id'];
	$postcode = $row['postcode'];
	$price = $row['price'];
	$special = $row['special_price'];
	$type = $row['type'];
	$brand = $row['brand'];
	$measure = $row['measure'];
	$uom = $row['uom'];
	$image = $row['image'];
	$name = $row['name'];
	$description = $row['description'];

	$escaped_uom = $db_link->real_escape_string($row['uom']);
	$escaped_image = $db_link->real_escape_string($row['image']);
	$escaped_name = $db_link->real_escape_string($row['name']);
	$escaped_description = $db_link->real_escape_string($row['description']);
	
	// get the total product count
	$strTotalProductCountQuery = "SELECT SUM(shopping_list_product_count) as total_product_count FROM shopping_lists_products WHERE shopping_list_product_id='".$product_id."'";
	$strTotalProductCountQueryExe = mysql_query($strTotalProductCountQuery);
	
	$intTotalCurrentProductSale = "";
	if($strTotalProductCountQueryExe)
	{
			$arrProductSaleData = mysql_fetch_array($strTotalProductCountQueryExe);
			$intTotalCurrentProductSale = $arrProductSaleData['total_product_count'];
	}
        
	// Do we already have this product in the current list?
	$current_product_count = 0;
	$product_already_on_list = false;
	$query = "SELECT shopping_list_product_count FROM shopping_lists_products 
				WHERE shopping_list_product_sale_id=$product_sale_id AND	
				shopping_list_id=$current_list AND
				supermarket_id=$supermarket";
	
	$result = $db_link->query($query);
	
	if ($result->num_rows) {
		$row = $result->fetch_assoc();
		$current_product_count = intval($row['shopping_list_product_count']);
		$product_already_on_list = true;
	}

	// How many do we want to have?
	$target_product_count = null;
	if ($product_add) {
		$target_product_count = ($current_product_count + $product_add);
	} else {
		$target_product_count = $product_count;
	}

	// new product total sale count
	$intNewTotalProductSaleCount = 0;
	if($target_product_count)
	{
			$intNewTotalProductSaleCount = ($intNewTotalProductSaleCount + $target_product_count);
	}
	
	/* $fh = fopen("check.txt","w");
	fwrite($fh,"---".$intNewTotalProductSaleCount);
	fclose($fh); */
	
	// Update Total Product Total Sale Count
	if($intNewTotalProductSaleCount)
	{
			if($product_add)
			{
					$strUpdateTotalProductSaleCountQuery = "UPDATE products SET product_sale_cnt= product_sale_cnt + '".$intNewTotalProductSaleCount."' WHERE id='".$product_id."'";
			}
			else
			{
					//$strUpdateTotalProductSaleCountQuery = "UPDATE products SET product_sale_cnt= '".$intNewTotalProductSaleCount."' WHERE id='".$product_id."'";
			}
			$strUpdateTotalProductSaleCountQueryExe = mysql_query($strUpdateTotalProductSaleCountQuery);
	}
	
	// Are we updating the item, or inserting it?
	if ($product_already_on_list) {
		$query = "UPDATE shopping_lists_products SET shopping_list_product_count=$target_product_count
								WHERE shopping_list_product_sale_id=$product_sale_id AND
								shopping_list_id=$current_list AND
								supermarket_id=$supermarket";
		$db_link->query($query);
	} else {
		$query = "INSERT INTO shopping_lists_products
						(shopping_list_product_count, shopping_list_product_id, shopping_list_product_sale_id, shopping_list_id, 
						supermarket_id, postcode, price, special_price, type, brand, 
						measure, uom, image, name, description)
					VALUES ($target_product_count, $product_id, $product_sale_id, $current_list, 
						$supermarket, $postcode, $price, $special, $type, $brand, 
						$measure, '$escaped_uom', '$escaped_image', '$escaped_name', '$escaped_description');";
		$db_link->query($query);
	}

	// How many items are we *actually* adding to the list?
	$product_increase_count = ($target_product_count - $current_product_count);
	$query2 = "UPDATE shopping_lists SET total_rrp = total_rrp + ($product_increase_count * (SELECT price FROM product_sales WHERE id=$product_sale_id)) WHERE id = $current_list;";
	$query3 = "UPDATE shopping_lists SET total_special = total_special + ($product_increase_count * (SELECT special_price FROM product_sales WHERE id=$product_sale_id)) WHERE id = $current_list;";
	$query4 = "UPDATE shopping-lists SET user_id = IF(user_id IS NULL,".$_SESSION['user_id'].", user_id) WHERE id = $current_list;";
	$output = '';
	if(isset($_SESSION['user_id']))
	{
		$result = $db_link->query($query4);	
	}
	if(!$result){
		$output .= "<tr><td colspan = \"5\">There was a problem including your user information with the shopping list: ".$db_link->error."</td></tr>";
	}
// 	$result = $db_link->query($query);
// 	if(!$result){
// 		$output .= "<tr><td colspan=\"5\">There was a problem adding the product to your shopping list: ".$db_link->error."</td></tr>";	
// 	}
	$result = $db_link->query($query2);	
	if(!$result){
		$output	.= "<tr><td colspan=\"5\">There was a problem updating the total rrp of your shopping list: ".$db_link->error."</td></tr>";
	}
	$result = $db_link->query($query3);
	if(!$result){
		$output	.= "<tr><td colspan=\"5\">There was a problem saving the total savings of your shopping list: ".$db_link->error."</td></tr>";
	}
	
	if($result):
		$product_query = "SELECT products.id, brands.name, product_sales.price, product_sales.special_price 
							FROM products, product_sales, brands
							WHERE product_sales.id=$product_sale_id
								AND products.id = product_sales.product_id
								AND brands.id = products.brand";
		$product_data = $db_link->prepare($product_query);
		$product_data->bind_result($id, $brand, $price, $special);
		$product_data->execute();
		$product_data->store_result();
		while ($product_data->fetch()):  
			$output .= '<tr>';
			$output .= '	<td align="center">' . $target_product_count . 'x ' . $name . '</td>';
			$output .= '	<td align="center">'.$price.'</td>';
			$output .= '	<td align="center">'.$special.'</td>';
			$output .= '	<td align="center"><input type="checkbox" name="product[]" id="'.$id.'" /></td>';
			$output .= '</tr>';
		endwhile;
	else:
		die('Failed to store product in shopping list: '.$db_link->error);
	endif;
	
	echo $output;
?>
