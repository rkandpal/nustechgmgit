<?php
require_once(dirname(__FILE__).'/../_inc.php');
require_once(dirname(__FILE__).'/../config.php');

$link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if ($link->error) {
	die('Could not connect: ' . $link->error);
}

$process = (isset($_REQUEST['process']) ? true : false);
$errors = array();
$show_form = true;

$still_ok = true;
$brand_id = null;
$brand_name = null;
$merge_to_brand_id = null;
$make_a_synonym = null;

/*
 * Test that the brand that we are merging is valid, before really showing anything of interest...
 */
if ($still_ok) {
	if (! (isset($_REQUEST['brand_id']) && $_REQUEST['brand_id'])) {
		$errors[] = 'The brand that we are trying to merge is missing: please click the merge button again on the "Manage Brands" page.';
		$still_ok = false;
	} else {
		$brand_id = intval($_REQUEST['brand_id']);
		$query = "SELECT name FROM brands WHERE id=$brand_id";
		$result = $link->query($query);

		if (! $result->num_rows) {
			$errors[] = 'The given brand to merge is not valid: please click the merge button again on the "Manage Brands" page.';
			$still_ok = false;
		} else {
			$row = $result->fetch_assoc();
			$brand_name = $row['name'];
		}
	}
}

/*
 * If we have errors at the moment, it's because of the brand that we are merging. In this
 * case, we show the errors and then ensure that we are not going to process *or* show the form.
 */
if (count($errors)) {
	echo '<p class="simple-message simple-message-error">';
	echo implode('<br />', $errors);
	echo '</p>';
	
	$process = false;
	$show_form = false;
}

if ($process) {
	// Test that the brand that we are merging to is valid...
	if ($still_ok) {
		if (! (isset($_REQUEST['merge_to_brand_id']) && $_REQUEST['merge_to_brand_id'])) {
			$errors[] = 'Please select the brand that we are merging "' . $brand_name . '" with.';
			$still_ok = false;
		} else {
			$merge_to_brand_id = intval($_REQUEST['merge_to_brand_id']);
			$query = "SELECT id FROM brands WHERE id=$merge_to_brand_id";
			$result = $link->query($query);
	
			if (! $result->num_rows) {
				$errors[] = 'The given brand that we are merging "' . $brand_name . '" with is not valid.';
				$still_ok = false;
			}
		}
	}

	// Determine if we are creating a synonym or not...
	if ($still_ok) {
		$make_a_synonym = (isset($_REQUEST['make_a_synonym']) && $_REQUEST['make_a_synonym']);
	}
	
	if ($still_ok) {
		// Update the products...
		$query = "UPDATE products SET brand=$merge_to_brand_id WHERE brand=$brand_id";
		$result = $link->query($query);
		$updated_product_count = $link->affected_rows;
		
		$query = "DELETE FROM brands WHERE id=$brand_id";
		$result = $link->query($query);

		echo '<p class="simple-message simple-message-information">';
		echo 'Deleted "' . $brand_name . '" from brands list.<br />';
		echo 'Changed ' . $updated_product_count . ' product/s to point to the new brand.<br />';
		echo '</p>';
		
		$show_form = false;
	} else {
		echo '<p class="simple-message simple-message-error">';
		echo implode('<br />', $errors);
		echo '</p>';
	}
}

if ($show_form) { 
	// Get all of the brands that we may be merging the selected brand into.
	$brands_array = array(0 => '');
	$result = mysql_query("SELECT id, name FROM brands WHERE id != " . intval($brand_id) . " ORDER BY name ASC");
	
	while ($row = mysql_fetch_assoc($result)) {
		$brands_array[intval($row['id'])] = $row['name'];
	}
	?>
	<p class="simple-message simple-message-information">Merging "<?php echo $brand_name; ?>" into another brand.</p>
	<div style="position: relative; width: 100%;text-align: left; margin: 30px 20px 40px 50px;">
		<script>
			$(document).ready(function() {
				$("#merge_to_brand_id").select2({
					width: '200px',
					dropdownCss: { width: 'auto' }
				});
			});
		</script>
	
		<form enctype="multipart/form-data" action="" name="form"  method="POST">
			<input type="hidden" name="process" value="1" />
			Select the brand that we are merging "<?php echo $brand_name; ?>" into:<br />
			<select name="merge_to_brand_id" id="merge_to_brand_id">
				<?php 
				foreach ($brands_array as $brand_key => $brand_value) {
					echo '<option value="' . $brand_key . '"';
					if ($brand_key == $merge_to_brand_id) {
						echo ' selected';
					}
					echo '>' . $brand_value . '</option>';
				}
				?>
			</select>
			<br /><br />
			<!--
			<label><input type="checkbox" name="make_a_synonym" id="make_a_synonym" value="1" /> Make "<?php echo $brand_name; ?>" a synonym for the selected brand.</label>
			<br /><br />
			-->
			<div class="input">
			<input type="submit" id="csv_submit" value="Merge Brands" style="display:inline" />
			</div>
		</form>
	</div>
	
	<p class="simple-message simple-message-information">
	By merging one brand into another, we will be taking all of the products that assigned to the first
	brand, and assigning them to the new brand. Then, we will remove the original brand from the database.
	<!--
	If desired, we can make the original brand a "synonym" for the first: this will enable us to use the
	original brand for the product still, but the product will actually be assigned to the new brand. If
	making a synonym, typing the original product into dropdowns on the upload page will select the new
	brand, as well.
	-->
	</p>
<?php } ?>