<?php
	require_once(dirname(__FILE__) . '/../functions.php');
	require_once(dirname(__FILE__) . '/../config.php');
	StartSession();
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('There was an error connecting to the database: '.$db_link->error);
	//echo $_SESSION['list_id'];
	$current_list = (isset($_SESSION['list_id']) ? $_SESSION['list_id'] : get_last_id());
	if($current_list != false):
		$query = "SELECT product_sales.id, shopping_lists_products.shopping_list_product_count, products.name, product_sales.price, product_sales.special_price, supermarkets.name 
				FROM products, product_sales, shopping_lists_products, supermarkets 
				WHERE  shopping_lists_products.shopping_list_id = $current_list AND 
						products.id = shopping_lists_products.shopping_list_product_id AND 
						product_sales.product_id = shopping_lists_products.shopping_list_product_id AND 
						product_sales.id = shopping_lists_products.shopping_list_product_sale_id AND 
						product_sales.supermarket_id = supermarkets.id
				ORDER BY supermarkets.name ASC";
		if(!$results = $db_link->prepare($query)):
			die('No results found for this shopping list');
		endif;
		
		$results->bind_result($id, $product_count, $product_name, $price, $special, $supermarket);
		$results->execute();
		$results->store_result();
		
		$total_price = 0;
		$total_special = 0;
		$savings = array();
		$product_sales_ids = array();
	endif;
?>	

	<div class="tab">
    	<img src="images/mini-shopping-list-tab.jpg" width="45" height="220" />
    </div>
    
    <form action="main_list_page.php" method="post" name="shoppinglist" id="shoppinglist-list-form">
        <fieldset>
            <legend class="form-title top-rounded-corners cart">Shopping List</legend>
            <div class="mini-products">
            <div id="mini-shopping-list-header">
            	<span class="mini-shopping-list-item">Item</span>
                <span class="mini-shopping-list-available">Available</span>
                <span class="mini-shopping-list-rrp">RRP</span>
                <span class="mini-shopping-list-special">Special</span>
                <span class="mini-shopping-list-list">List</span>
            </div><!-- mini-shopping-list-header -->
            <div id="mini-shopping-list-products">
            <?php
				if($results->num_rows > 0 && $current_list):
					while($results->fetch()):
				
                		if(!array_search($id, $product_sales_ids)):
							$total_price += ($product_count * $price);
							$total_special += ($product_count * $special);
							$product_sales_ids[] = $id;
						endif;
						
						$dropdown_html = '<select name="product_count[' . $id . ']" id="product_count-' . $id . '" onchange="javascript:ChangeBasketProductCount(' . $id . ');">';
						for ($i = 1; ($i <= 20); $i++) {
							$dropdown_html .= '<option value="' . $i . '"';
							if ($i == $product_count) {
								$dropdown_html .= ' selected';
							}
							$dropdown_html .= '>' . $i . '</option>';
						}	
						$dropdown_html .= '</select>';
						$product_name = ucwords($product_name);
						if (strlen($product_name) > 14){
							$product_short_name = substr($product_name, 0, 13);
							$product_short_name .= '...';	
						} else {
							$product_short_name = $product_name;	
						}
				?>						
						<span class="mini-shopping-list-item" <?php echo ($product_short_name)? 'label="'.addslashes($product_name).'"' : '' ?>><?php echo $dropdown_html . 'x '; ?> <?php echo $product_short_name; ?></span>
                        <span class="mini-shopping-list-available"><?php echo $supermarket ?></span>
						<span class="mini-shopping-list-rrp">$<?php echo $price; ?></span>
						<span class="mini-shopping-list-special">$<?php echo $special; ?></span>
						<span class="mini-shopping-list-list"><button type="button" name="product[]" id="<?php echo $id; ?>" class="delete-product-button" onClick="javascript:removeFromBasket(<?php echo $id ?>);" ><img src="images/checkbox-x.png" width="10" height="10" /></button></span>
						<br />
				<?php							
						$savings = store_saving($price, $special, $supermarket, $savings, $product_count);
					endwhile;
				else:
				?>
                	<span class="mini-shopping-list-no-products">There are no items in your basket</span>
                <?php
				endif;
            ?>	
            	</div><!-- #mini-shopping-list-products -->
                
                </div><!-- .mini-products -->
                <div id="mini-shopping-list-totals">
                	<?php if ($results->num_rows > 0): ?>
                    	<br />
                       	<span class="mini-shopping-list-totals-heading">Total</span>
                        <span class="mini-shopping-list-rrp-total">$<span id="total"><?php echo number_format($total_price, 2) ?></span></span>
                        <span class="mini-shopping-list-special-total">$<span id="special-total"><?php echo number_format($total_special, 2) ?></span></span>
                    <?php endif; ?>
                    <table width="98%" cellpadding="0" cellspacing="0">                    	
                      <?php foreach ($savings as $supermarket_name => $value): ?>                            
                        <tr>
                            <td class="no-border" align="center"><img src="images/supermarket/<?php echo strtolower(str_replace(' ', '_', $supermarket_name)); ?>.png" /></td>
                            <td class="no-border" align="left">savings of <span class="green">&nbsp;$<?php echo number_format($value, 2) ?></span>
                            </td>
                        </tr>                                   
					  <?php endforeach; ?>                        		                
            		</table>
            </div><!-- .mini-totals -->
            <p>Your total savings are $<?php echo number_format(floatval($total_price) - floatval($total_special), 2) ?>.</p>
            <input type="submit" name="update-list" id="update-list" value="View Basket" class="rounded-corners" />
        </fieldset>
    </form>
	<script language="javascript" type="text/javascript">
	$(window).load(function(){
		if($('.scroll-bar').size() > 0){
			if (appleDevice){
				$('.scroll-bar').mCustomScrollbar({
					set_height: 180	
				});
			}
			else
			{
				$('.scroll-bar').mCustomScrollbar({
					set_height: 250	
				});
			}
		}
	})
</script>