<script>
function DisplayProductTypeDivAndProductType(type_id) {
	//$('#browes-categories').css('display', '');
	getProducts(type_id, true);
	//$('.browse-list').fadeOut();
}

$('#search-input').live('keyup', function(event) {
	var search_value = $('#search-input').val();
	
	setTimeout(function() {
		if ($('#search-input').val() == search_value) {
			$('#searching').css('display', '');
			
			$('#search-brands').css('display', 'none');
			$('#search-results-brand-header').css('display', 'none');
			$('#search-results-brand-results').css('display', 'none');
			
			$('#search-product-type').css('display', 'none');
			$('#search-results-product-type-header').css('display', 'none');
			$('#search-results-product-type-results').css('display', 'none');

			$('#search-product').css('display', 'none');
			$('#search-results-product-header').css('display', 'none');
			$('#search-results-product-results').css('display', 'none');
			if($('#search-input').val().length > 0)
			{	
				searchRequest = $.ajax({
					url: 'search-ajax.php',
					type: 'POST',
					dataType: 'json',
					data: { search: $('#search-input').val() },
					success: function(data) {
						if (data.status == 'fail') {
							alert(data.status_text);
						} else {
							$('#searching').css('display', 'none');
							
							$('#search-brands').css('display', 'inline-block');
							$('#search-results-brand-header').css('display', '');
							$('#search-results-brand-results').css('display', '');

							$('#search-product-type').css('display', 'inline-block');
							$('#search-results-product-type-header').css('display', '');
							$('#search-results-product-type-results').css('display', '');
							
							$('#search-product').css('display', 'inline-block');
							$('#search-results-product-header').css('display', '');
							$('#search-results-product-results').css('display', '');

							if (data.data.brand.length) {
								var html = '<ul class="search-results-list">';

								for (var i in data.data.brand) {
									var the_item = data.data.brand[i];
									html += '<li><a href="#" onclick="getProductsForBrand('+the_item.id+')">';
									html += the_item.name;
									html += '</a></li>';
								}
								html += '</ul>';
								$('#search-results-brand-results').html(html);
							} else {
								$('#search-results-brand-results').html('No matching brands found');
							}

							if (data.data.product_type.length) {
								var html = '<ul class="search-results-list">';

								for (var i in data.data.product_type) {
									var the_item = data.data.product_type[i];
									html += '<li><a href="#" onclick="DisplayProductTypeDivAndProductType(' + the_item.id + '); return false;">';
									html += the_item.name;
									html += '</a></li>';
								}
								html += '</ul>';
								$('#search-results-product-type-results').html(html);
							} else {
								$('#search-results-product-type-results').html('No matching product types found');
							}
							
							if (data.data.product.length) {
								var html = '<ul class="search-results-list">';

								for (var i in data.data.product) {
									var the_item = data.data.product[i];
									html += '<li><a href="product.php?view=VIEW&pid=' + the_item.id + '">';
									html += the_item.name;
									html += '</a></li>';
								}
								html += '</ul>';
								$('#search-results-product-results').html(html);
							} else {
								$('#search-results-product-results').html('No matching products found');
							}
							// alert(data);
						}
						// $('body').append('<div>'+ data +'</div>');
					}	
				});
			}
			else
			{
				$('#searching').css('display', 'none');	
			}
		}
	}, 500);
});
</script>

<div id="search">
    <form action="javascript: return false;" method="post" name="search" id="search-form">
        <input type="text" name="search-input" id="search-input" placeholder="Product Search" size="40" />
		<div id="filter_block_fields" style="display:none;">
		  <div>
			Filter With <select name='filterwith' id="filterwith">
			<option value="0">Select</option>
			<option value="price">Price</option>
			<option value="unit_price">Unit Price</option>
			<option value="popular">Popular</option>
			<option value="a-z">Alphabetical Order</option>
			</select>
			<br />
			From <select name='filterorder' onchange="fnFilter()" id="filterorder">
			<option value="0">Select Order</option>
			<option value="low_to_high">Low to High</option>
			<option value="high_to_low">High to Low</option>
			</select>
		</div>
		
	</div>
    </form>
</div><!-- #search -->
<div id="search-results" class="rounded-corners">
    <div id="searching" style="display: none;">Searching...</div>
    <div id="search-brands" class="rounded-corners" style="display:none">
    	<div id="search-results-brand-header" style="display: none;">Brands</div>
    	<div id="search-results-brand-results" style="display: none;"></div>
    </div>
    <div id="search-product-type" class="rounded-corners" style="display:none">
    	<div id="search-results-product-type-header" style="display: none;">Product Types</div>
    	<div id="search-results-product-type-results" style="display: none;"></div>
    </div>
    <div id="search-product" class="rounded-corners" style="display:none">
    	<div id="search-results-product-header" style="display: none;">Products</div>
    	<div id="search-results-product-results" style="display: none;"></div>
    </div>
</div>
<!--<div id="search-browse">
	<div id="browes-categories" style="display: none;">
        <div id="aisle" class="browse-list rounded-corners">
            <ul class="category rounded-corners">
             </ul>
        </div>
    </div>
</div>-->

