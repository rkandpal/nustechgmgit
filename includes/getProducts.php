<?php
	require_once(dirname(__FILE__).'/../functions.php');
	require_once(dirname(__FILE__).'/../config.php');
	StartSession();
	 $arrSuperMarketDisplayDetail = array('2'=>array('pricelabel'=>'Norm Price'),'1'=>array('pricelabel'=>'Norm Price'),'3'=>array('pricelabel'=>'Norm Price'));
	/* print("<pre>");
	print_r($arrBrandDisplayDetail);
	exit; */
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME) 
		or die("There was an error connecting to the database: ".$db_link->error);
	$product_type_id = $_POST['productType'];
	$on_sale_only = (isset($_POST['onSaleOnly']) && $_POST['onSaleOnly']);
	$strFilterWith = $_POST['filterWith'];
	$strFilterOrder = $_POST['filterOrder'];
	$intAsileId = $_POST['asile'];
	$now_date_string = date('Y-m-d');
	
	if($product_type_id)
	{
			$query = "SELECT products.id, products.highlighted, product_sales.id, products.measure, products.uom, products.image,
									 product_sales.price, product_sales.special_price, product_sales.end_date,
									 brands.name, products.name, products.technical_name,
									 product_types.name,
									 supermarkets.name,supermarkets.id,
									 aisle.id AS aisle_id, aisle_name 
					  FROM products, product_sales, brands, product_types, supermarkets, aisle 
					  WHERE products.type = $product_type_id AND 
									products.brand = brands.id AND 
									product_sales.product_id = products.id AND
									products.type = product_types.id AND
									product_types.aisle_id = aisle.id AND
									product_sales.supermarket_id = supermarkets.id";
	}
	
	if($intAsileId)
	{
			$query = "SELECT products.id, products.highlighted, product_sales.id, products.measure, products.uom, products.image,
									 product_sales.price, product_sales.special_price, product_sales.end_date,
									 brands.name, products.name, products.technical_name,
									 product_types.name,
									 supermarkets.name,supermarkets.id,
									 aisle.id AS aisle_id, aisle_name 
					  FROM products, product_sales, brands, product_types, supermarkets, aisle 
					  WHERE aisle.id = $intAsileId AND 
									products.brand = brands.id AND 
									product_sales.product_id = products.id AND
									products.type = product_types.id AND
									product_types.aisle_id = aisle.id AND
									product_sales.supermarket_id = supermarkets.id";
	}

	if ($on_sale_only) {
		$query .= " AND product_sales.start_date<='$now_date_string' AND product_sales.end_date>='$now_date_string'";
	}
	
	if (true /*$filter_on_user_postcode*/) {
		$gm_cookie = new GM_Cookie();
		if($user->uid)
		{
			$postcode = $user->get_meta('post_code');
			
			
		} else {
			$postcode = $gm_cookie->registered_postcode;
		}
		
			
		if ($postcode && is_numeric($postcode)) {
			$state_code = substr($postcode, 0, 1);
			$state_code_minimum = ($state_code * 1000);
			$state_code_maximum = (($state_code * 1000) + 999);
			$query .= " AND product_sales.postcode >= $state_code_minimum AND product_sales.postcode <= $state_code_maximum";
		} else {
			$query .= " GROUP BY products.id, supermarkets.name";
		}
		// error_log($query);
	}
	
	if($strFilterWith)
	{
			switch($strFilterWith)
			{
					case"price": 
											 $query .= " ORDER BY product_sales.price";
											 break;
											 
					case"unit_price": $query .= " ORDER BY product_sales.unit_price";
													  break;
													  
					case"popular": $query .= " ORDER BY products.product_sale_cnt";
													  break;
					
					case"a-z":
											$query .= " ORDER BY products.name";
											break;
			}
	}
	
	if($strFilterOrder)
	{
			switch($strFilterOrder)
			{
					case"low_to_high": 
											 $query .= " ASC";
											 break;
					case"high_to_low":
											$query .= " DESC";
											break;
			}
	}
	
	//echo "--".$query;
	
	$results = $db_link->prepare($query);

		//$results->bind_result($id, $sale_id, $measure, $uom, $image, $price, $special, $sale_end_date, $brand, $name, $technical_name, $product_type, $supermarket, $aisle_id, $aisle_name);
		$results->bind_result($id, $highlighted, $sale_id, $measure, $uom, $image, $price, $special, $sale_end_date, $brand, $name, $technical_name, $product_type, $supermarket, $supermarket_id, $aisle_id, $aisle_name);
		$results->execute();
		$results->store_result();
		$row_cnt = $results->num_rows;
		
		$odd_even_highlight = "odd";
		$odd_even = "odd";
	$output = array();
	//echo $row_cnt;
	if(empty($row_cnt)):
		$output['result'] = false;
		$output['html'] = '<p>Sorry we cant find any products on special in this Aisle.</p>';
		$output['header'] = '<p></p>';
		$output['ishighlightHtml'] = false;
	else :
		$output['result'] = true;
		$output['highlighthtml'] = '';
		$output['html'] = '';
		$first_row = true;
		$strHighlightedClass = "";
		while($results->fetch())
		{	
			$product_image = ($image != '')?$image:"default_product.jpg";
			if ($first_row) {
				$output['html'] .= '<div class="hidden" id="product-type">'.ucwords($aisle_name).' &gt; '.ucwords($product_type).'</div>';
				$first_row = false;
			}
			//get image size
			
			$image_size = array();
			$image_width = "";
			$image_height = "";
			
			if(file_exists(dirname(__FILE__).'/../images/'.$product_image))
			{
					$image_size = getimagesize(dirname(__FILE__).'/../images/'.$product_image);
					$image_width = $image_size[0];
					$image_height = $image_size[1];
			}
			
			$brand = RewriteSmartQuotes($brand);
			$name = RewriteSmartQuotes($name);
			$technical_name = RewriteSmartQuotes($technical_name);
			$product_type = RewriteSmartQuotes($product_type);
			$aisle_name = RewriteSmartQuotes($aisle_name);
				
			if($highlighted)
			{
					$output['ishighlightHtml'] = true;
					$strHighlightedClass = "highlighted";
					$output['highlighthtml'] .= '<div class="product rounded-corners'.$strHighlightedClass.'" style="width:96%;border: 5px solid red;margin-bottom:5px;">';
					$output['highlighthtml'] .= '<table cellpadding="0" cellspacing="0" width="55%" style="margin-left:23%;">';
					$output['highlighthtml'] .= '   <tr>';
					$output['highlighthtml'] .= '           <td class="image"><img class="product-thumb rounded-corners" src="images/'.$product_image.'" style="vertical-align:top"';
					$output['highlighthtml'] .= ($image_size[0] > $image_size[1])? 'width="130"' : 'height=""';
					$output['highlighthtml'] .= ' /></td>';
					$output['highlighthtml'] .= '           <td class="product-data">';
					$output['highlighthtml'] .= '                   <table cellpadding="0" cellspacing="0" width="100%">';
					$output['highlighthtml'] .= '                           <tr><td class="product-name">'.ucwords($brand . ' ' . ($name ? $name : $technical_name)).'</td>';
					$output['highlighthtml'] .= '                           <tr><td class="weight">'.round($measure,2) . ' ' . $uom .'</td></tr>';
					$output['highlighthtml'] .= fnPrintProductPrice($supermarket_id,$price);
					if($price && round($measure,2))
					{
							$output['highlighthtml'] .= fnCalculateUnitPrice($price,round($measure,2),$uom);
					}
					//$output['html'] .= '                          <tr><td class="price">RRP: $' . $price . '</td></tr>';
					if($special)
					{
							$output['highlighthtml'] .= '                           <tr><td class="special">Special: $'.$special.'</td></tr>';
							$output['highlighthtml'] .= fnCalculateSavingsOnThisItem($price,$special);
					}
					
					
					/*$formatted_sale_date = GetFormattedDBDate($sale_end_date);
					if ($formatted_sale_date) {             
							$output['html'] .= '                            <tr><td class="sale-date">On sale until ' . $formatted_sale_date . '</td></tr>';                        
					}*/
					$output['highlighthtml'] .=                             '</table>';
					$output['highlighthtml'] .= '           </td>';
					$output['highlighthtml'] .= '   </tr>';
					$output['highlighthtml'] .= '   <tr>';
					$output['highlighthtml'] .= '           <td class="supermarket"><img src="images/supermarket/'.strtolower(str_replace(' ', '_',$supermarket)).'.png" width="80"/></td>';
					$output['highlighthtml'] .= '           <td class="forms"><div class="product-form">';
					$output['highlighthtml'] .= '                           <form class="view-product" name="view-product" id="view-'.$id.'" method="get" action="product.php">';
					$output['highlighthtml'] .= '                                   <input type="submit" class="rounded-corners" name="view" id="view-'.$id.'" value="VIEW" />';
					$output['highlighthtml'] .= '                                   <input type="hidden" name="pid" value="'.$id.'" />';
					$output['highlighthtml'] .= '                           </form>';
					$output['highlighthtml'] .= '                           <form class="add-to-basket" name="add-to-basket" onsubmit="javascript:addToBasket('.$sale_id.'); return false;">';
					$output['highlighthtml'] .= '                                   <input type="submit" class="rounded-corners" id="add" name="add" value="ADD" />';
					$output['highlighthtml'] .= '                                   <input type="hidden" name="product_id" id="product_id" value="'.$sale_id.'" />';
					$output['highlighthtml'] .= '                           </form>';
					$output['highlighthtml'] .= '                   </div>';
					$output['highlighthtml'] .= '           </td>';
					$output['highlighthtml'] .= '   </tr>'; 
					$output['highlighthtml'] .= '</table>';
					$output['highlighthtml'] .= '</div>';
					
					$_SESSION['url'] = $_POST['url'];
					$hash_url = '';
					if($product_type_id)
					{
							$hash_url = ('#' . $aisle_id."-".$product_type_id."-"."1");
							//$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
							$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
					}
					$output['parent_hash_url'] = $hash_url;
					
					$odd_even_highlight = ($odd_even_highlight == "odd")? "even" : "odd";
			}
			else
			{
					$output['html'] .= '<div class="product rounded-corners '.$odd_even.' '.$strHighlightedClass.'">';
					$output['html'] .= '<table cellpadding="0" cellspacing="0" width="100%">';
					$output['html'] .= '    <tr>';
					$output['html'] .= '            <td class="image"><img class="product-thumb rounded-corners" src="images/'.$product_image.'" style="vertical-align:top"';
					$output['html'] .= ($image_size[0] > $image_size[1])? 'width="130"' : 'height=""';
					$output['html'] .= ' /></td>';
					$output['html'] .= '            <td class="product-data">';
					$output['html'] .= '                    <table cellpadding="0" cellspacing="0" width="100%">';
					$output['html'] .= '                            <tr><td class="product-name">'.ucwords($brand . ' ' . ($name ? $name : $technical_name)).'</td>';
					$output['html'] .= '                            <tr><td class="weight">'.round($measure,2) . ' ' . $uom .'</td></tr>';
					$output['html'] .= fnPrintProductPrice($supermarket_id,$price);
					if($price && round($measure,2))
					{
							$output['html'] .= fnCalculateUnitPrice($price,round($measure,2),$uom);
					}
					//$output['html'] .= '                          <tr><td class="price">RRP: $' . $price . '</td></tr>';
					if($special)
					{
							$output['html'] .= '                            <tr><td class="special">Special: $'.$special.'</td></tr>';
							$output['html'] .= fnCalculateSavingsOnThisItem($price,$special);
					}
					
					
					/*$formatted_sale_date = GetFormattedDBDate($sale_end_date);
					if ($formatted_sale_date) {             
							$output['html'] .= '                            <tr><td class="sale-date">On sale until ' . $formatted_sale_date . '</td></tr>';                        
					}*/
					$output['html'] .=                              '</table>';
					$output['html'] .= '            </td>';
					$output['html'] .= '    </tr>';
					$output['html'] .= '    <tr>';
					$output['html'] .= '            <td class="supermarket"><img src="images/supermarket/'.strtolower(str_replace(' ', '_',$supermarket)).'.png" width="80"/></td>';
					$output['html'] .= '            <td class="forms"><div class="product-form">';
					$output['html'] .= '                            <form class="view-product" name="view-product" id="view-'.$id.'" method="get" action="product.php">';
					$output['html'] .= '                                    <input type="submit" class="rounded-corners" name="view" id="view-'.$id.'" value="VIEW" />';
					$output['html'] .= '                                    <input type="hidden" name="pid" value="'.$id.'" />';
					$output['html'] .= '                            </form>';
					$output['html'] .= '                            <form class="add-to-basket" name="add-to-basket" onsubmit="javascript:addToBasket('.$sale_id.'); return false;">';
					$output['html'] .= '                                    <input type="submit" class="rounded-corners" id="add" name="add" value="ADD" />';
					$output['html'] .= '                                    <input type="hidden" name="product_id" id="product_id" value="'.$sale_id.'" />';
					$output['html'] .= '                            </form>';
					$output['html'] .= '                    </div>';
					$output['html'] .= '            </td>';
					$output['html'] .= '    </tr>'; 
					$output['html'] .= '</table>';
					$output['html'] .= '</div>';
					
					$_SESSION['url'] = $_POST['url'];
					$hash_url = '';
					if($product_type_id)
					{
							$hash_url = ('#' .$aisle_id."-".$product_type_id."-"."1");
							//$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
							$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
					}
					
					$output['parent_hash_url'] = $hash_url;
					
					$odd_even = ($odd_even == "odd")? "even" : "odd";  
			
			}
		}
	endif;
	echo json_encode($output);
        
        

function fnCalculateUnitPrice($intActualPrice = "", $intTotalQty = "", $strMeasuringUnit = "")
{
        $strUnitPriceHtml = "";
        $intUnitPriceAmount = "";
        if($intActualPrice && $intTotalQty)
        {
                $intUnitPriceAmount = round(($intActualPrice / $intTotalQty),2);
        }
        
        $strUnitPriceHtml = '<tr><td>Unit Price: $' . $intUnitPriceAmount . '/'.$strMeasuringUnit.'</td></tr>';
        return $strUnitPriceHtml;
}       
        
        
function fnCalculateSavingsOnThisItem($intNormalPrice = "", $intSpecialPrice = "")
{
        $intSavingAmount = "";
        $intSavingAmountHtml = "";
        if($intSpecialPrice)
        {
                $intSavingAmount = ($intNormalPrice - $intSpecialPrice);
        }
        $intSavingAmountHtml = '<tr><td style="color:green;">Savings: $' . $intSavingAmount . '</td></tr>';
        return $intSavingAmountHtml;
}
        
function fnPrintProductPrice($intSuperMarketIdIs = "", $intAmnt = "")
{
        global $arrSuperMarketDisplayDetail;
        $strProductPriceHtml = "";
        if($intSuperMarketIdIs)
        {
                $arrSuperMarkets = array_keys($arrSuperMarketDisplayDetail);
                if(in_array($intSuperMarketIdIs,$arrSuperMarkets))
                {
                        $strProductPriceHtml = '<tr><td class="price">'.$arrSuperMarketDisplayDetail[$intSuperMarketIdIs]['pricelabel'].': $' . $intAmnt . '</td></tr>';
                }
                else
                {
                        $strProductPriceHtml = '<tr><td class="price">RRP: $' . $intAmnt . '</td></tr>';
                }
        }
        else
        {
                $strProductPriceHtml = '<tr><td class="price">RRP: $' . $intAmnt . '</td></tr>';
        }
        return $strProductPriceHtml;
}
        
        
?>