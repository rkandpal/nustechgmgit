<?php
	/*
	/*	This file includes the panels for each layer of the category breakdown,
	/* 	each time a user clicks on an item it the page will use ajax to display 
	/*	next layer of categories. In total there are 3 layers at most to go through
	/* 	then the brand is displayed.
	*/
	require_once(dirname(__FILE__) . '/../functions.php');
	require_once 'config.php'; //config to connect for poduct info not users info
	
?>

<?php
	
	$demographics = GetDemographicsOfProductsForSale();

	/*
	/* 	show_list will return the list of categories in a new div	
	*/
	$db_link = new MySQLi (DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	$query = "SELECT id, aisle_name FROM aisle ORDER BY aisle_name ASC";
	
	$results = $db_link->prepare($query);
	$results->bind_result($id, $name);
	$results->execute();
	$results->store_result();
	$row_cnt = $results->num_rows;
	
	if(! $results){
		die("There was an error getting data: ". $db_link->error);	
	}
	
?>
<div id="browseHeadline">

    		Shop By Aisle
</div>
<br />
<div id="browse">
	<div id="browes-categories">
		<div id="arrowjpg" class="picturePosition"><img src="images/arrow.png"></div>
        <div id="aisle" class="browse-list rounded-corners">
            <ul class="category rounded-corners">
                <!--<li><a href="#" id="pantry" onclick="showNextCategory('pantry')">Pantry</a></li>
                <li><a href="#" id="kitchen" onclick="showNextCategory('kitchen')">Kitchen</a></li>
                <li><a href="#" id="drinks" onclick="showNextCategory('drinks')">Drinks</a></li>
                <li><a href="#" id="cleaning" onclick="showNextCategory('cleaning')">Cleaning</a></li>-->
                <?php 
					if(empty($row_cnt)):
				?>
                		<li>Sorry There are no aisles to select</li>
                <?php
					else:
						while($results->fetch())
						{
							$display_name = ucwords($name);
							$url_name = preg_replace('/[^a-z0-9]/i', '-', $name);
							$sale_class = (isset($demographics['aisles'][intval($id)]) ? 'has-sales' : 'has-no-sales');
							$strShowCategorySelection = "";
							$strMouseClickCategoryData = "";
							if(isset($demographics['aisles'][intval($id)]))
							{
								$strMouseHoverCategorySelection = "onmouseover=showProductType('".$id."',true)";
								//$strMouseClickCategoryData = "onclick=/'getProducts('','','','','".$id."')/'";
								$strMouseClickCategoryData = "onclick=fnShowAsileProducts('".$id."')";
							}
							
					?>
							<!--<li><a  <?php echo $strShowCategorySelection; ?> href="#<?php echo $id . '/' . $url_name; ?>" class="<?php echo $id . ' ' . $sale_class ?>"><?php echo $display_name; ?></a></li>-->
							<li><a  <?php echo $strMouseHoverCategorySelection; ?>  href="#<?php echo $id ?>" class="<?php echo $id . ' ' . $sale_class ?>"><?php echo $display_name; ?></a></li>
							<!--<li><a  <?php echo $strMouseHoverCategorySelection; ?>  <?php echo $strMouseClickCategoryData; ?> class="<?php echo $id . ' ' . $sale_class ?>"><?php echo $display_name; ?></a></li>-->
					<?php
						}
					endif;
				?>
            </ul>
        </div>
    </div>
</div>
<div id="mini-shopping-list" class="rounded-corners">
<?php include_once(dirname(__FILE__) . '/mini_shopping_list.php'); ?>
</div>