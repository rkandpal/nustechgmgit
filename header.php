<?php //include('functions.php');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Grocery Mate <?php echo get_page_title(); ?></title>
<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/search.css" />
<link type="text/css" rel="stylesheet" href="css/forms.css" />
<link type="text/css" rel="stylesheet" href="css/shopoping-list.css" />
<?php 
$document_ready_js_stats = stat(dirname(__FILE__) . '/js/document.ready.js');
$functions_js_stats = stat(dirname(__FILE__) . '/js/functions.js');
?>
<script language="javascript" type="text/javascript" src="js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.ba-hashchange.min.js"></script>
<script language="javascript" type="text/javascript" src="js/document.ready.js?<?php echo $document_ready_js_stats['mtime']; ?>"></script>
<script language="javascript" type="text/javascript" src="js/custom.form.elements.js"></script>
<script language="javascript" type="text/javascript" src="js/functions.js?<?php echo $functions_js_stats['mtime']; ?>"></script>
</head>

<body>
	<div id="toolbar" class="shadow dark-panel">
    	
        <?php get_navigation() ?>      	
        
    </div><!-- #toolbar -->
