<?php
	require_once('_inc.php');
	require_once('functions.php');
	$head_title = array();
	$head_title[] = 'Login';

	// If the user is already logged in then return them to the homepage
	if($user->isAuthorized())
	{
		header('Location: '.MK_Utility::serverUrl('/'), true, 302);
		exit;
	}

	$output='<div id="login" class="rounded-corners">';

	// If the user clicked the 'sign in with Yahoo' link
	if( $config->site->yahoo->login && MK_Request::getQuery('platform') === 'yahoo' )
	{
		$config = MK_Config::getInstance();
		$callback = YahooUtil::current_url();
		$auth_url = YahooSession::createAuthorizationUrl($config->site->yahoo->consumer_key, $config->site->yahoo->consumer_secret, $callback);

		header('Location: '.$auth_url, true, 302);
		exit;
	}
	// If the user clicked the 'sign in with Windows Live' link
	elseif( $config->site->windowslive->login && MK_Request::getQuery('platform') === 'windowslive' )
	{
		header('Location: '.$config->windowslive->getConsentToken(), true, 302);
	}
	// If the user clicked the 'sign in with Facebook' link
	elseif( $config->site->facebook->login && MK_Request::getQuery('platform') === 'facebook' )
	{
		$config = MK_Config::getInstance();
		$facebook_return = MK_Utility::serverUrl( $config->site->referer );
		$facebook_url = $config->facebook->getLoginUrl(array(
			'next' => $facebook_return,
			'cancel_url' => $facebook_return,
			'req_perms' => 'email, user_photos'
		));

		header('Location: '.$facebook_url, true, 302);
		exit;
	}
	// If the user clicked the 'sign in with Twitter' link
	elseif( $config->site->twitter->login && MK_Request::getQuery('platform') === 'twitter' )
	{
		$config = MK_Config::getInstance();

		$twitter_request_token = $config->twitter->getRequestToken( MK_Utility::serverUrl( 'login.php' ) );

		$session->twitter_oauth_token = $twitter_request_token['oauth_token'];
		$session->twitter_oauth_token_secret = $twitter_request_token['oauth_token_secret'];

		$twitter_url = $config->twitter->getAuthorizeURL($session->twitter_oauth_token);

		header('Location: '.$twitter_url, true, 302);
		exit;
	}
	// If the user clicked the 'sign in with Google' link
	elseif( $config->site->google->login && MK_Request::getQuery('platform') === 'google' )
	{
		$config = MK_Config::getInstance();

		$google_request_token = $config->google->getRequestToken( MK_Utility::serverUrl( 'login.php' ) );

		$session->google_oauth_token = $google_request_token['oauth_token'];
		$session->google_oauth_token_secret = $google_request_token['oauth_token_secret'];

		$google_url = $config->google->getAuthorizeURL($session->google_oauth_token);

		header('Location: '.$google_url, true, 302);
		exit;
	}
	// User is logging in with their site account
	elseif( MK_Request::getQuery('platform') === 'core' )
	{
		unset( $session->registration_details );
	}

	$user_module = MK_RecordModuleManager::getFromType('user');
	$field_module = MK_RecordModuleManager::getFromType('module_field');
	$criteria = array(
		array('field' => 'module_id', 'value' => $user_module->getId()),
		array('field' => 'name', 'value' => 'email')
	);
	
	$user_email_field = $field_module->searchRecords($criteria);
	$user_email_field = array_pop( $user_email_field );

	if( !empty( $session->registration_details ) )
	{
		$user_details = unserialize( $session->registration_details );
		$settings = array(
			'attributes' => array(
				'class' => 'narrow clear-fix standard',
				'action' => 'login.php'
			)
		);
	
		$structure = array(
			'email' => array(
				'label' => 'Email',
				'validation' => array(
					'email' => array(),
					'instance' => array(),
					'unique' => array(null, $user_email_field, $user_module)
				)
			),
			'cancel' => array(
				'type' => 'link',
				'text' => 'Cancel Login',
				'attributes' => array(
					'href' => 'login.php?platform=core'
				)
			),
		);
		
		$complete_field = array(
			'type' => 'submit',
			'attributes' => array(
				'value' => 'Complete Login'
			)
		);

		if( $user_details['google_id'] )
		{
			$output .= '<h2>Google Login</h2>';
			$structure['google'] = $complete_field;
		}
		elseif( $user_details['twitter_id'] )
		{
			$output .= '<h2>Twitter Login</h2>';
			$structure['twitter'] = $complete_field;
		}
	
		$form = new MK_Form($structure, $settings);
		
		$output .= '<p>Please enter your email address to finish logging in.</p>';
		if($form->isSuccessful())
		{
			$user_details['email'] = $form->getField('email')->getValue();
			$session->registration_details = serialize( $user_details );
			header('Location: '.MK_Utility::serverUrl('login.php'), true, 302);
			exit;
		}
	}
	else
	{
		$settings = array(
			'attributes' => array(
				'class' => 'narrow clear-fix standard'
			)			
		);
		
		
		$structure = array(
			
		
			'email' => array(
				'label' => 'Email',
				'validation' => array(
					'email' => array(),
					'instance' => array()
				),
				'attributes' => array(
					'type' => 'text',
					'size' => '40'
				)
			),
			'password' => array(
				'label' => 'Password',
				'validation' => array(
					'instance' => array(),
				),
				'attributes' => array(
					'type' => 'password',
					'size' => '40'
				)
			),
			'login-submit' => array(
				'type' => 'submit',
				'attributes' => array(
					'value' => 'Login',
					'size' => '40'
				)
			)
		);
		
		$settings_login = array(
			'attributes' => array(
				'class' => 'narrow clear-fix standard social'
			)
		);
	
		$structure_login = array();
	
		if($config->site->facebook->login)
		{
			$structure_login['facebook'] = array(
				'type' => 'link',
				'text' => 'Facebook',
				'attributes' => array(
					'href' => 'login.php?platform=facebook'
				)
			);	
		}
	
		if($config->site->twitter->login)
		{
			$structure_login['twitter'] = array(
				'type' => 'link',
				'text' => 'Twitter',
				'attributes' => array(
					'href' => 'login.php?platform=twitter'
				)
			);	
		}
	
		if($config->site->windowslive->login)
		{
			$structure_login['windowslive'] = array(
				'type' => 'link',
				'text' => 'Windows Live',
				'attributes' => array(
					'href' => 'login.php?platform=windowslive'
				)
			);	
		}
	
		if($config->site->yahoo->login)
		{
			$structure_login['yahoo'] = array(
				'type' => 'link',
				'text' => 'Yahoo',
				'attributes' => array(
					'href' => 'login.php?platform=yahoo'
				)
			);	
		}
	
		if($config->site->google->login)
		{
			$structure_login['google'] = array(
				'type' => 'link',
				'text' => 'Google',
				'attributes' => array(
					'href' => 'login.php?platform=google'
				)
			);	
		}
	
		$form = new MK_Form($structure, $settings);
		
		

		
		if($form->isSuccessful())
		{
			$user = MK_Authorizer::authorizeByEmailPass(
				$form->getField('email')->getValue(),
				$form->getField('password')->getValue()
			);
			
			if($user->isAuthorized())
			{
				$session->login = $user->getId();
				$cookie->set('login', $user->getId(), $config->site->user_timeout);
	
				if( !$redirect = $config->extensions->core->login_url )
				{
					$redirect = $logical_redirect;
				}
				header('Location: '.MK_Utility::serverUrl($redirect), true, 302);
				exit;
			}
			else
			{
				$form->getField('email')->getValidator()->addError('This username and password combination does not match our records. Please try again.');
			}
		}
	}
	$output .= '<div id="login-form" class="top-rounded-corners">';
	$output .= '<legend class="form-title top-rounded-corners">Login</legend>';
	$output .= '<p>Please enter your email address and password. </p>';
	$output.=$form->render();
	$output .= '<p><a class="forgot_pass" href="forgot-pass.php">Forgotten password?</a></p>';
	$output .= '</div>';

	if(count($structure_login) > 0)
	{
		$login_form = new MK_Form($structure_login, $settings_login);
		$output .= '<h2>Or Login With</h2>';
		$output .= $login_form->render();
	}

	$output.='</div>';

	require_once('_header.php');
	print $output;
	require_once('_footer.php');
?>