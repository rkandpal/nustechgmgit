<?php 
	require_once '_inc.php';
	require_once 'functions.php';
	$head_title = array();
	$head_title[] = 'Privacy Policy';
	require_once '_header.php';
	
?>
<div id="basic-content">
    <h2>WEBSITE PRIVACY POLICY</h2>    
    <h4>DATED: THE 03 DAY OF October  2012 WEBSITE PRIVACY POLICY</h4>
    <p>This web site is owned and operated by Iron Logic Pty Ltd and will be referred to as "We", "our" and "us" in this Internet Privacy Policy. By using this site, you agree to the Internet Pri-vacy Policy of this web site ("the web site"), which is set out on this web site page. The In-ternet Privacy Policy relates to the collection and use of personal information you may supply to us through your conduct on the web site.</p>
    <p>We reserve the right, at our discretion, to modify or remove portions of this Internet Privacy Policy at any time. This Internet Privacy Policy is in addition to any other terms and condi-tions applicable to the web site. We do not make any representations about third party web sites that may be linked to the web site. </p>
    <p>We recognize the importance of protecting the privacy of information collected about visitors to our web site, in particular information that is capable of identifying an individual ("personal information"). This Internet Privacy Policy governs the manner in which your personal in-formation, obtained through the web site, will be dealt with. This Internet Privacy Policy should be reviewed periodically so that you are updated on any changes. We welcome your comments and feedback. </p>
    <p><strong>Personal Information</strong><br />
    1.	Personal information about visitors to our site is collected only when knowingly and voluntarily submitted. For example, we may need to collect such information to provide you with further services or to answer or forward any requests or enquiries. It is our in-tention that this policy will protect your personal information from being dealt with in any way that is inconsistent with applicable privacy laws in Australia. <br /><br>
    <strong>Use of Information</strong><br  />
    2.	Personal information that visitors submit to our site is used only for the purpose for which it is submitted or for such other secondary purposes that are related to the prima-ry purpose, unless we disclose other uses in this Internet Privacy Policy or at the time of collection. Copies of correspondence sent from the web site, that may contain personal information, are stored as archives for record-keeping and back-up purposes only. <br /><br>
    <strong>Collecting information on Registered members</strong><br />
    3.	As part of registering with us, we collect personal information about you in order for you to take full advantage of our services. To do this it may be necessary for you to provide additional information to us as detailed below.<br />
    4.	Registration Registration is completely optional. Registration may include submitting your name, email address, address, telephone numbers, option on receiving updates and promotion-al material and other information. You may access this information at any time by log-ging in and going to your account. <br /><br>
    <strong>Credit Card Details</strong><br />
    5.	Credit Card details are only stored for the processing of payment and will be deleted once payment is processed.<br /><br>
    <strong>Disclosure</strong><br />
    6.	Apart from where you have consented or disclosure is necessary to achieve the purpose for which it was submitted, personal information may be disclosed in special situations where we have reason to believe that doing so is necessary to identify, contact or bring legal action against anyone damaging, injuring, or interfering (intentionally or uninten-tionally) with our rights or property, users, or anyone else who could be harmed by such activities. Also, we may disclose personal information when we believe in good faith that the law requires disclosure. <br />
    7.	We may engage third parties to provide you with goods or services on our behalf. In that circumstance, we may disclose your personal information to those third parties in order to meet your request for goods or services. <br /><br>
    <strong>Security</strong><br />
    8.	We strive to ensure the security, integrity and privacy of personal information submitted to our sites, and we review and update our security measures in light of current technologies. Unfortunately, no data transmission over the Internet can be guaranteed to be totally secure. <br />
    9.	However, we will endeavor to take all reasonable steps to protect the personal infor-mation you may transmit to us or from our online products and services. Once we do receive your transmission, we will also make our best efforts to ensure its security on our systems. <br />
    10.	In addition, our employees and the contractors who provide services related to our in-formation systems are obliged to respect the confidentiality of any personal information held by us. However, we will not be held responsible for events arising from unauthor-ised access to your personal information.<br /><br>
    <strong>Collecting Information from Users</strong><br /> 
    11.	<em>IP Addresses</em><br />
    Our web servers gather your IP address to assist with the diagnosis of problems or sup-port issues with our services. Again, information is gathered in aggregate only and can-not be traced to an individual user. <br /><br>
    12.	<em>Cookies and Applets</em><br />
    We use cookies to provide you with a better experience. These cookies allow us to in-crease your security by storing your session ID and are a way of monitoring single user access. This aggregate, non-personal information is collated and provided to us to assist in ana-lysing the usage of the site. <br /><br>
    <strong>Access to Information</strong><br />
    13.	We will endeavor to take all reasonable steps to keep secure any information which we hold about you, and to keep this information accurate and up to date. If, at any time, you discover that information held about you is incorrect, you may contact us to have the information corrected. <br />
    14.	In addition, our employees and the contractors who provide services related to our in-formation systems are obliged to respect the confidentiality of any personal information held by us. <br /><br />
    <strong>Links to other sites</strong><br />
    15.	We provide links to Web sites outside of our web sites, as well as to third party Web sites. These linked sites are not under our control, and we cannot accept responsibility for the conduct of companies linked to our website. Before disclosing your personal in-formation on any other website, we advise you to examine the terms and conditions of using that Web site and its privacy statement. <br /><br />
    <strong>Problems or questions</strong><br />
    16.	If we become aware of any ongoing concerns or problems with our web sites, we will take these issues seriously and work to address these concerns. If you have any further queries relating to our Privacy Policy, or you have a problem or complaint, please con-tact us.<br />
    17.	For more information about privacy issues in Australia and protecting your privacy, visit the Australian Federal Privacy Commissioner's web site; http://www.privacy.gov.au/.<br /><br />
    </p>
    <p>This Privacy Policy has been specifically drafted for and provided to Iron Logic Pty Ltd by LawLive Pty Ltd (<a href="http://www.lawlive.com.au" target="_blank">www.lawlive.com.au</a>).</p>
</div><!-- basic-content -->

<?php

	require_once '_footer.php';
	
?>