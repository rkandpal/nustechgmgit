<?php 
error_reporting(0);
require_once('config.php');
require_once('_inc.php');
require_once('functions.php');

$json_response = array('success' => false, 'message' => null);
if (isset($_POST['postcode'])) {
	$postcode = $_POST['postcode'];
	if (($postcode >= 1000) && ($postcode <= 9999)) {
		$gm_cookie = new GM_Cookie();
		$gm_cookie->registered_postcode = $postcode;
		$gm_cookie->has_visited_before = true;
		$gm_cookie->WriteBackCookie();
		
		$json_response['success'] = true;
		$json_response['message'] = '';
		die(json_encode($json_response));
	}
}

$json_response['message'] = 'The given postcode is not valid';
die(json_encode($json_response));
