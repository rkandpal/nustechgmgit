<?php
require_once '_inc.php';

$session = MK_Session::getInstance();
$cookie = MK_Cookie::getInstance();
unset($session->login, $cookie->login);

if( !$redirect = $config->extensions->core->logout_url )
{
	$redirect = $logical_redirect;
}

header('Location: '.MK_Utility::serverUrl($redirect), true, 302);
exit;
?>