<?php 
	
	require_once '_inc.php';
	require_once 'functions.php';
	$head_title = array();
	$head_title[] = 'About';
	require_once '_header.php';
	
?>

<div id="basic-content">
	<h2>About</h2>
	<p>grocerymate is a new type of supermarket that allows you to compare specials from the main Australian grocery retailers in one place. Our aim is to help you save time and money while giving you the best possible shopping experience.</p>
	<p>It's up to you how you use our site: You can print your basket as a shopping list and take it with you to your local supermarket or simply just compare different supermarket special prices online to determine the best value shop for you.</p>
	<p>When comparing between stores, our only aim is to ensure that our shoppers get the best value checkout possible.</p>
	<p>We are 100% independent from the retailers featured on our site. None of the stores or any manufacturers own a stake in the company. Our pricing information and inventory are updated weekly according to your State.</p>


</div><!-- basic-content -->

<?php

	require_once '_footer.php';
	
?>