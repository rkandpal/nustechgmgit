<?php 
	
	require_once '_inc.php';
	require_once 'functions.php';
	$head_title = array();
	$head_title[] = 'Contact';
	require_once '_header.php';
	
?>

<div id="basic-content">

	<h2>Contact Us</h2>
    <p><strong>Grocerymate Support:</strong><br />
    grocerymate was designed to provide the best shopping experience for online shoppers. If you have experienced a problem with our service or you think of a way we can improve it, please let us know...</p>
    <p><strong>Contacting grocerymate support:</strong><br />
    By email: <a href="mailto:support@grocerymate.com.au">support@grocerymate.com.au</a></p>
	
    <p><strong>Business Enquiries:</strong><br />
	grocerymate provides a range of services for businesses. To find out more please contact us (Mon - Fri, 9am - 5pm):<br />
    Press:<br />
    By email: <a href="mailto:press@grocerymate.com.au">press@grocerymate.com.au</a><br /><br />
    Advertisers:<br />
    By email: <a href="mailto:affiliates@grocerymate.com.au">advertising@grocerymate.com.au</a><br /><br />
    Manufacturers:<br />
    By email: <a href="mailto:affiliates@grocerymate.com.au">manufacturers@grocerymate.com.au</a><br /><br />
    Affiliates:<br />
    By email: <a href="mailto:affiliates@grocerymate.com.au">affiliates@grocerymate.com.au</a><br /><br />
    All other enquiries:<br />
    By email: <a href="mailto:enquiries@grocerymate.com.au">enquiries@grocerymate.com.au</a>


</div><!-- basic-content -->

<?php

	require_once '_footer.php';
	
?>