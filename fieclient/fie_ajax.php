<?php
/**
 * A simple wrapper to provide AJAX access to the functions in 
 * fieclient.php.
 * 
 * @author Lincoln Maskey <lincoln@maskey.net>
 * @copyright 2012
 */
require_once(dirname(__FILE__) . '/fieclient.php');
require_once(dirname(__FILE__) . '/../config.php');

function SendJSONOutput($the_array) {
	echo json_encode($the_array);
	die();
}

$result = array();
$result['success'] = false;
$result['error_text'] = '';
$result['result'] = array();

if (! (isset($_REQUEST) && isset($_REQUEST['action']))) {
	$result['error_text'] = 'Action not posted';
	SendJSONOutput($result);
}

if ($_REQUEST['action'] == 'get_matching_products') {
	if ($_REQUEST['use_dlibrary']) {
		$call_array = array();
		$products = GetMatchingProducts($_REQUEST['supplier'], $_REQUEST['product_name'], $call_array);
	
		if ($call_array['return_val'] == 0) {
			$result['success'] = true;
			$result['result'] = array_values($products);
		} else {
			$result['error_text'] = $call_array['result_text'];
		}
	} else {
		$attempt_number = 1;
		while ((count($result['result']) == 0) && ($attempt_number <= 5)) {
			$result = GetMatchingProducts_Database($_REQUEST['supplier'], $_REQUEST['product_name'], $attempt_number);
			$attempt_number++;
		}
		
		// 'success' just means there was no errors...
	}
	SendJSONOutput($result);
}

if ($_REQUEST['action'] == 'get_image_for_ean') {
	$call_array = array();
	$the_ean = (isset($_REQUEST['ean']) ? trim($_REQUEST['ean']) : '');
	
	if ((strlen($the_ean) != 8) && (strlen($the_ean) != 13)) {
		$result['error_text'] = 'Length of EAN ' . $the_ean . ' is not valid';
		SendJSONOutput($result);
	}

	// What would be the target directory here?
	$target_directory = 'EAN-' . strlen($the_ean) . '/';
	$exploded_ean = str_split($the_ean, 4);
	$target_directory .= implode('/', $exploded_ean);
	$images_root_directory = realpath(dirname(__FILE__) . '/../images/') . '/';
	$full_directory = ($images_root_directory . $target_directory);
	
	if (! file_exists($full_directory)) {
		mkdir($full_directory, 0777, true);
	}
	
	$image_path = CopyImageForGivenEANToGivenDirectory($_REQUEST['ean'], $full_directory, $call_array);
	if ($image_path && ($call_array['return_val'] == 0)) {
		$result['success'] = true;
		
		// We need to remove the stuff that doesn't matter to the AJAX client from the path
		$image_path = realpath($image_path);
		$image_path = str_ireplace($images_root_directory, '', $image_path);
		$result['result'] = $image_path;
	} else {
		$result['error_text'] = $call_array['result_text'];
	}
	SendJSONOutput($result);
}

$result['error_text'] = 'Unknown action: "' . $_REQUEST['action'] . '"';
SendJSONOutput($result);


function GetMatchingProducts_Database($supplier_name, $product_name, $attempt_number) {
	$result = array();
	$result['success'] = true;
	$result['error_text'] = '';
	$result['result'] = array();
	
	$db_link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$lookup_strings = array('brands' => array(), 'products' => array());
	
	$supplier_name = trim($supplier_name);
	$product_name = trim($product_name);
	
	if ($attempt_number == 1) {
		// Look for the supplier in the supplier field, and the description in the description field...
		if (strlen($supplier_name) > 0) {
			$lookup_strings['brands'][] = $supplier_name;
		}
		if (strlen($product_name) > 0) {
			$lookup_strings['products'][] = $product_name;
		}
	} else if ($attempt_number == 2) {
		// Look for both the supplier and the description in the description field...
			if (strlen($supplier_name) > 0) {
			$lookup_strings['products'][] = $supplier_name;
		}
		if (strlen($product_name) > 0) {
			$lookup_strings['products'][] = $product_name;
		}
	} else if ($attempt_number == 3) {
		// Just look for the description in the description field...
		if (strlen($product_name) > 0) {
			$lookup_strings['products'][] = $product_name;
		}
	} else if ($attempt_number == 4) {
		// Take the supplier and the description, and just look for anything with the same
		// words in the appropriate spot...
		$supplier_name_parts = explode(' ', $supplier_name);
		foreach ($supplier_name_parts as $supplier_name_part) {
			$supplier_name_part = trim($supplier_name_part);
			if (preg_match('/[a-zA-Z0-9]/', $supplier_name_part)) {
				$lookup_strings['brands'][] = $supplier_name_part;
			}
		}
		
		$product_name_parts = explode(' ', $product_name);
		foreach ($product_name_parts as $product_name_part) {
			$product_name_part = trim($product_name_part);
			if (preg_match('/[a-zA-Z0-9]/', $product_name_part)) {
				$lookup_strings['products'][] = $product_name_part;
			}
		}
	} else if ($attempt_number == 5) {
		// Take the description, and just look for anything with the same words...
		$product_name_parts = explode(' ', $product_name);
		foreach ($product_name_parts as $product_name_part) {
			$product_name_part = trim($product_name_part);
			if (preg_match('/[a-zA-Z0-9]/', $product_name_part)) {
				$lookup_strings['products'][] = $product_name_part;
			}
		}
	}
	
	if (count($lookup_strings['brands']) || count($lookup_strings['products'])) {
		$query = "SELECT products.name AS product_name, brands.name AS brand_name, product_types.name AS product_type_name, products.aisle_id AS aisle_id, ean, description, brands.name AS supplier
					FROM products
					INNER JOIN brands ON brands.id=products.brand
					INNER JOIN product_types ON product_types.id=products.type
					WHERE ";
		
		if (count($lookup_strings['brands'])) {
			$query_parts = array();
			foreach ($lookup_strings['brands'] as $lookup_brand) {
				$query_parts[] = "brands.name LIKE '%" . $db_link->real_escape_string($lookup_brand) . "%'";
			}
			
			$query .= '(' . implode(' OR ', $query_parts) . ') ';
			if (count($lookup_strings['products'])) {
				$query .= 'AND ';
			}
		}

		if (count($lookup_strings['products'])) {
			$query_parts_name = array();
			$query_parts_technical_name = array();
			$query_parts_description = array();
			foreach ($lookup_strings['products'] as $lookup_product) {
				$query_parts_name[] = "products.name LIKE '%" . $db_link->real_escape_string($lookup_product) . "%'";
				$query_parts_technical_name[] = "products.technical_name LIKE '%" . $db_link->real_escape_string($lookup_product) . "%'";
				$query_parts_description[] = "products.description LIKE '%" . $db_link->real_escape_string($lookup_product) . "%'";
			}
			
			$query .= '(';
			$query .= '(' . implode(' OR ', $query_parts_name) . ') OR ';
			$query .= '(' . implode(' OR ', $query_parts_technical_name) . ') OR ';
			$query .= '(' . implode(' OR ', $query_parts_description) . ')';
			$query .= ') ';
		}
		
		$query .= 'ORDER BY brands.name, products.name';
		$db_result = $db_link->query($query);
		if (! $db_result) {
			$result['success'] = false;
			$result['error_text'] = 'There was an error looking up this product';
		} else if ($db_result->num_rows) {
			while ($db_row = $db_result->fetch_assoc()) {
				$db_row['aisle_id'] = intval($db_row['aisle_id']);
				
				// We want to return a usable description... and not the other fields we just pulled
				$db_row_description = $db_row['description'];
				$db_row_product_name = $db_row['product_name'];
				$db_row_brand_name = $db_row['brand_name'];
				unset($db_row['product_name']);
				unset($db_row['brand_name']);
				
				if (! $db_row_description) {
					$db_row_description = $db_row_product_name;
					if ($db_row_brand_name) {
						$db_row_description .= ' (by ' . $db_row_brand_name . ')';
					}
					
					$db_row['description'] = $db_row_description;
				}
				
				$result['result'][] = $db_row;
			}
		}
	}
	
	return $result;
}