function showProductType(aisle_id, async){
	//alert(aisle_id);
	// store the level that was clicked on by drilling up to the div.
	current_level = 'aisle' //parseInt($('#'+category_id).parent('li').parent('ul').parent('div').attr('id').replace('level', ''));
		
	//remove the previous current category
	$('#'+current_level+' ul li').each(function(index, element) {
        $(this).removeClass('current-category');
    });
	
	//alert($('#productType'));
	if($('#productType').size() > 0)
	{
		$('#productType').remove();	
	}
	
	//add the selected one
	$('#'+current_level+' .'+aisle_id).parent('li').addClass('current-category');
	
	
	
	
	//Add the new data that is taken from the database
	$.ajax({
		type: 'POST',
		url: 'includes/getProductType.php',
		async: async,
		dataType: "json",
		data: {
			aisle_id : aisle_id,
			level : $('.browse-list').size()+1	
		}		
	}).done(function(msg){
		//animation goes here
		if (msg.result == true)
		{
			$('#productType').remove();
			$('#browes-categories').append(msg.html);
			$('#browes-categories').fadeIn("fast");
			//var list_width = $('.browse-list').outerWidth();
			$('#arrowjpg').removeClass('picturePosition');
			$('#arrowjpg').addClass('picturePositionToggle');
			
			$('#browseHeadline').html('Select a Product Type');
			
			
			//$('#browseHeadline').html('Select a Product Type');
			// $('.browse-list:last-child').css('display','none');
			if(!appleDevice)
			{
				var list_width = 270;
				var number_of_lists = $('.browse-list').size();
				$('#browes-categories').animate({'width': ((number_of_lists*list_width))+'px'}, 'fast', function(){
					$('.browse-list:last-child').fadeIn('slow')
				});
			} else {
				$('.browse-list:first-child').fadeOut('fast', function(){
					$('.browse-list:last-child').fadeIn('fast');	
				});
			}
		}
		else
		{
			$('#overlay-content #message').html(msg.html).css('display', 'block');
			$('#overlay-content #overlay-header p').html(msg.header);
			$('#overlay').fadeIn('fast');
			$('#overlay-content').fadeIn('fast');
			//alert(msg.html)	
		}
	});
}
function getProducts(productType, onSaleOnly, filtrationCriteria, filterationOrder,asile_id)
{
	//do an ajax call to get all the products that have the same product type id.
	//Eventually this will be sorted in a weighted way.
	
	//alert(asile_id);
	
	
	$('#browes-categories').fadeOut('fast');
	//$('#aisle').fadeOut('fast');
	//$('#productType').fadeOut('fast');
	
	if($('#browse').has('#product-list')){
		$('#product-list').remove();
	}
	
	if($('#browse').has('#highlight_product_list')){
		$('#highlight_product_list').remove();
	}
	if(productType == '')
	{
		/* alert($('.'+asile_id).html());
		return false; */
		$('#browse').append(getLoading($('.'+asile_id).html()));
	}
	else
	{
		$('#browse').append(getLoading($('#productType ul li .'+productType).html()));
	}
	
	$('#browseHeadline').html('Loading...');
	
	
	
	$.ajax({
		type: 'POST',
		url: 'includes/getProducts.php',
		dataType: "json",
		data:{
			productType : productType,
			onSaleOnly : onSaleOnly,
			filterWith : filtrationCriteria,
			filterOrder : filterationOrder,
			asile: asile_id,
			url : document.URL
		},
	}).done(function(data){
		if(data.result == true)
		{
			//$('#filter_block_fields').show();
			//$('#productType').fadeOut('fast');
			$('#browes-categories').fadeOut('fast');
			
			if(data.ishighlightHtml == true)
			{
				$('#browse').append(buildHighlightProductList(data.highlighthtml));
			}
			$('#browse').append(buildProductList(data.html));
			$('#lodingProductList').remove();
			var filterHtml = $('#filter_block_fields').html();
			//alert(filterHtml);
			$('#filter_block_fields').remove();
			$('#browseHeadline').html('<div style="width:38.33%;float:left;padding-left:2%;"><a href="' + data.parent_hash_url + '" id="go-back">Back To Browse</a></div><div id="product-title" style="width:23.33%;float:left;padding-left:2%;">'+$('#product-type').html()+'</div><div id="filter_block_fields" style="width:32.34%;float:left;padding-left:2%;text-align:left;">'+fnGiveFilterHtml()+'</div>');
			//$('#browseHeadline').html('<div style="width:33.33%;float:left;"><a href="' + data.parent_hash_url + '" id="go-back">Back To Browse</a></div><div id="product-title" style="width:33.33%;float:left;">'+$('#product-type').html()+'</div>');
		}
		else
		{
			//$('#filter_block_fields').hide();
			$('#lodingProductList').remove();
			$('#browes-categories').fadeIn('fast');
			$('#browes-categories').children('.browse-list').each(function(){
				$(this).fadeIn('fast');
			});
			$('#browseHeadline').html('Shop by Aisle');			
			$('#overlay-content #overlay-header p').html(data.header);
			$('#overlay-content #message').html(data.html).css('display', 'block');
			$('#overlay').fadeIn('fast');
			$('#overlay-content').fadeIn('fast');
		}
	})
	return false;
	//$('products').fadeIn('fast');//this will be done on ajax success.	
}

function fnGiveFilterHtml()
{
	var filterHtml = '<div> <div style="float:left;width:80%;margin-left:35%;min-height:91px;">Sort By ';
	filterHtml += '<select name="filterwith" id="filterwith">';
	filterHtml += '<option value="0">Select</option>';
	filterHtml += '<option value="price">Price</option>';
	filterHtml += '<option value="unit_price">Unit Price</option>';
	filterHtml += '<option value="popular">Popular</option>';
	filterHtml += '<option value="a-z">Alphabetical Order</option>';
	filterHtml += '</select>';
	filterHtml += '<br />';
	filterHtml += 'From <br />';
	filterHtml += '<select name="filterorder" onchange="fnFilter()" id="filterorder">';
	filterHtml += '<option value="0">Select Order</option>';
	filterHtml += '<option value="low_to_high">Low to High</option>';
	filterHtml += '<option value="high_to_low">High to Low</option>';
	filterHtml += '</select>';
	filterHtml += '</div></div>';			
	return filterHtml;
}

function getProductsForBrand(brandID, brandName)
{
	//do an ajax call to get all the products that have the same product type id.
	//Eventually this will be sorted in a weighted way.
	
	
	
	$('#browse').append(getLoading(brandName));
	$('#browseHeadline').html('Loading...');
	
	$.ajax({
		type: 'POST',
		url: 'includes/getProductsForBrand.php',
		dataType: "json",
		data:{
			brandID : brandID
		},
	}).done(function(data){
		if(data.result == true)
		{
			if($('#browse').has('#product-list')){
				$('#product-list').remove();	
			}
			$('#browes-categories').fadeOut('fast');
			$('#browse').append(buildProductList(data.html));
			$('#lodingProductList').remove();
			$('#browseHeadline').html('<div id="product-title">'+$('#product-type').html()+'</div>');	
		}
		else
		{
			$('#lodingProductList').remove();
			$('#browes-categories').fadeIn('fast');
			$('#browseHeadline').html('Start Shopping');
			$('#overlay-content #message').html(data.html);
			$('#overlay-content #overlay-header p').html(data.header);
			$('#overlay').fadeIn('fast');
			$('#overlay-content').fadeIn('fast');
		}
	})
	return false;
	//$('products').fadeIn('fast');//this will be done on ajax success.	
}

function getLoading(productTypeName){
	var html = '';
	html += '<div id="lodingProductList">';
	html += '	<img src="images/loading.gif" align="center" />';
	html += '	<p>Please wait loading list of <strong>'+productTypeName+'</strong></p>';
	html += '</div>';
	return html;
}
function buildProductList(data){
	
	return '<div id="product-list" class="rounded-corners">'+data+'</div>'; 	
}

function buildHighlightProductList(data){
	
	return '<div id="highlight_product_list" class="rounded-corners" style="background: none repeat scroll 0 0 #FFFFFF;">'+data+'</div>'; 	
}
function addToBasket (product_sale_id){
	//alert('Adding poduct to basket');
	$.ajax({
		type: 'POST',
		url: 'includes/addToBasket.php',
		data: {
			product_sale_id: product_sale_id	
		}
	}).done(function(data){
		/* Add product to minishopping list */
		//$('#mini-shopping-list table tbody').append(data);
		$('#mini-shopping-list').load('includes/mini_shopping_list.php', function(){
			$('#mini-shopping-list').animate({'right':0},'fast', function(){
				if(appleDevice){
					$('.scroll-bar').mCustomScrollbar({ set_height: 180});
				}
				else{
					$('.scroll-bar').mCustomScrollbar({ set_height: 250});
				}
				$(this).removeClass('in');					
			});
		});		
	}).fail(function(){
		/* Show error message */
		alert(data)
	});
	
	return false;	
}

function removeFromBasket (product_sale_id){
	$.ajax({
		type: "POST",
		url: "includes/removeFromBasket.php",
		data:{
			product_sale_id : product_sale_id	
		}
	}).done(function(){
		/*remove item from mini-shopping-list*/
		$('#mini-shopping-list').load('includes/mini_shopping_list.php', function(){
		$('#mini-shopping-list').animate({'right':0},'fast', function(){
			if(appleDevice){
					$('.scroll-bar').mCustomScrollbar({ set_height: 180});
				}
				else{
					$('.scroll-bar').mCustomScrollbar({ set_height: 250});
				}
			$(this).removeClass('in');				
		});
		});
		
	}).fail(function(){
		/* Show errore message */	
	});
	
}

function ChangeBasketProductCount(product_sale_id) {
	var new_count = $('#product_count-' + product_sale_id).val();
	
	//alert('Adding poduct to basket');
	$.ajax({
		type: 'POST',
		url: 'includes/addToBasket.php',
		data: {
			product_sale_id: product_sale_id,
			product_count: new_count
		}
	}).done(function(data){
		/* Add product to minishopping list */
		//$('#mini-shopping-list table tbody').append(data);
		$('#mini-shopping-list').load('includes/mini_shopping_list.php', function(){
			$('#mini-shopping-list').animate({'right':0},'fast', function(){
				$('.scroll-bar').mCustomScrollbar({ set_height: 250});
				$(this).removeClass('in');	
			});
		});
	}).fail(function(){
		/* Show error message */
		alert(data)
	});
	return false;	
}

function ChangeBigBasketProductCount(product_sale_id) {
	var new_count = $('#product_count-' + product_sale_id).val();
	
	//alert('Adding poduct to basket');
	$.ajax({
		type: 'POST',
		url: 'includes/addToBasket.php',
		data: {
			product_sale_id: product_sale_id,
			product_count: new_count
		}
	}).done(function(data){
		/*remove item from mini-shopping-list*/
		$('#shopping-list-container').load('main_list_page.php #big_shopping_list');
	}).fail(function(){
		/* Show error message */
		alert(data)
	});
	return false;	
}

function removeFromBigBasket (product_sale_id){
	$.ajax({
		type: "POST",
		url: "includes/removeFromBasket.php",
		data:{
			product_sale_id : product_sale_id
		}
	}).done(function(){
		/*remove item from mini-shopping-list*/
		$('#shopping-list-container').load('main_list_page.php #big_shopping_list');
	}).fail(function(){
		/* Show errore message */	
	});
}
function saveShoppingList(){
	// Should we overwrite this one?
	var this_list_name = $('#shopping-list-name-input').val().toLowerCase();
	if ($.inArray(this_list_name, shopping_lists_to_confirm_overwrite) != -1) {
		var this_message = "There is currently already a list with the name '" + $('#shopping-list-name-input').val() + "' saved for this week. ";
		this_message += "Saving this list with the same name will overwrite it.";
		if (! confirm(this_message)) {
			return;
		}
	}
	
	$.ajax({
		type: "POST",
		url: "includes/save_shopping_list.php",
		dataType: "json",
		data: {
			name: $('#shopping-list-name-input').val(),
			user_id: $('#user-id-input').val()	
		}
	}).done(function(data){
		if(data.result){
			$('#save-message').html("<p>"+data.html+"</p>")
						.css({
							'background-color':'#9FB900',
							'border':'1px solid #E7E9EC'	
						})
						.slideDown('slow');	
		}
		else{
			$('#save-message').html("<p>"+data.html+"</p>")
						.css({
							'background-color':'#FF6A6A',
							'border':'1px solid #E7E9EC'	
						})
						.slideDown('slow');
		}
	});	
}
function deleteShoppingList(list_id){
	$.ajax({
		type: "POST",
		url: "includes/delete_shopping_list.php",
		dataType: "json",
		data: {
			list_id : list_id	
		}
	}).done(function(data){
		if(data.result){
			window.location = data.location;
		}
		else{
			return false;	
		}
	});
}

function findSalesAgain(list_id){
	$.ajax({
		type: "POST",
		url: "includes/find_sales_again.php",
		dataType: "json",
		data: {
			list_id : list_id	
		}
	}).done(function(data){
		if(data.result){
			if (data.products_copied == 0) {
				alert('Either none of these products are currently on sale, or you already have them all on your list.');
			} else {
				alert('There were ' + data.products_copied + ' products found on sale that you didn\'t already have on your list. Those products have been added.');
			}
		}
		else{
			return false;	
		}
	});
}

function goBack(){
	console.log(window.history.go(1));
	return false;
}