// JavaScript Document
$(document).ready(function(e) {
    
	// This is for the check boxes that are dotted around the website.
    if (!$.browser.opera) {

        $('select.select').each(function(){
            var title = $(this).attr('title');
            if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
            $(this)
                .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
                .after('<span class="select">' + title + '</span>')
                .change(function(){
                    val = $('option:selected',this).text();
                    $(this).next().text(val);
                    })
        });

    };
	
	$('#mini-shopping-list .tab').live('click', function(e) {
		if ($(this).parent('div#mini-shopping-list').hasClass('in')){
			$(this).parent('div#mini-shopping-list').animate(
			{
				right:0
			},'fast', function(){ $(this).removeClass('in').addClass('out'); });
			
		}
		else
		{
			$(this).parent('div#mini-shopping-list').animate(
			{
				right:-412
			},'fast', function(){ $(this).removeClass('out').addClass('in'); });	
			
		}
	});
	$('#mini-shopping-list').addClass('in')
	
	
	$('#mini-shopping-list .tab').live('mouseenter', function(e) {
        if($(this).parent('div#mini-shopping-list').hasClass('in'))
		{
			$(this).parent('div#mini-shopping-list').animate({right:-402},'fast');
		}
		
    });
	$('#mini-shopping-list').live('mouseleave', function(e) {
        if($(this).hasClass('in'))
		{
			$(this).animate({right:-412},'fast');
		}
    });
	$('#select-supermarket').live('change', function(){
		/*$.ajax({
			type: "POST",
			url: "main_list_page.php",
			data: {
				supermarket: $('#select-supermarket').val()
			}
		}).done(function(data){
			$('#shopping-list-container').html(data);
			return false;
		});*/
		$('#shopping-list-container').load("main_list_page.php?supermarket="+$('#select-supermarket').val()+" #big_shopping_list")
	});

//	$('#go-back').live('click', function(){
//		$('#product-list').fadeOut('slow', function(){
//			$('#browes-categories').fadeIn('fast');
//			$('#browseHeadline').html('Browes our list');
//		})
//	});
	
	//overlay-content in middle of screen
	var height = $('#overlay-content').outerWidth();
	var page_height = $(window).innerHeight();
	
	if(appleDevice){
		$('#overlay-content').css({'top':'35%'});
	} else {
		$('#overlay-content').css({'margin-left':'-250px', 'margin-top': "-"+(height/2)+"px", 'left':'50%', 'top':'50%'});
	}
	$('#user-panel').after($('#shoppinglist-list'));
	
	$('#overlay').click(function(){
		$('#overlay-content').fadeOut('fast');
		$(this).fadeOut('fast');
	})
});

// Set up the hash listener...
$(document).ready(function(e) {
	$(window).hashchange(function() {
		$('#filter_block_fields').hide();
		
		
		var hash = location.hash.replace(/^#/, '');
		if($('#browse').has('#product-list')){
			$('#product-list').remove();	
		}
		
		if($('#browse').has('#highlight_product_list')){
				$('#highlight_product_list').remove();
			}
		// Get the important part of the hash...
		hash = hash.split('/');
		var strOriginalHashLength = hash.length;
		
		hash = hash[0];
		
		var hash_parts = new Array();
		if (hash != "") {
			hash_parts = hash.split('-');
		}
		//alert(hash_parts.length);
		if ((hash_parts.length == 0) || (hash_parts.length == 1)) {
			current_aisle = hash_parts[0];
			
			$('#product-list').fadeOut('fast');
			$('#browes-categories').fadeIn('fast');
			$('#aisle').fadeIn('fast');
			if (hash_parts.length == 1) {
				getProducts('',true,'','',current_aisle);
				// do we need to select this aisle?
				if (! $('#aisle .' + current_aisle).parent('li').hasClass('current-category')) {
					//showProductType(current_aisle, true);
					
				} else {
					// make sure the categories are showing...
					$('#productType').fadeIn('fast');
					$('#productType').show();
				}
			
				$('#productType ul li').each(function(index, element) {
			        $(this).removeClass('current-category');
			    });	

//				if($('#browse').has('#productType')){
//					$('#productType').remove();	
//				}
				
				$('#browseHeadline').html('Shop By Aisle');
			} else {
				$('#browseHeadline').html('Select a Product Type');
			}

			if($('#browse').has('#product-list')){
				$('#product-list').remove();	
			}
		}

//		if (hash_parts.length == 2) {
//			current_aisle = hash_parts[0];
//			$('#product-list').fadeOut('fast');
//			$('#browes-categories').fadeIn('fast');
//			$('#aisle').fadeIn('fast');
//			
//			// do we need to select this aisle?
//			if (! $('#aisle .' + current_aisle).parent('li').hasClass('current-category')) {
//				showProductType(current_aisle, false);
//			} else {
//				// make sure the categories are showing...
//				$('#productType').fadeIn('fast');
//			}
//			
//			current_product_type = hash_parts[1];
//			$('#productType').fadeIn('fast');
//			
//			// do we need to select this category?
//			if (! $('#productType .' + current_product_type).parent('li').hasClass('current-category')) {
//				showProductType(current_product_type, true);
//			} else {
//				// make sure the product types are showing...
//				$('#productType').fadeIn('fast');
//			}
//			
//			if($('#browse').has('#product-list')){
//				$('#product-list').remove();	
//			}
//			$('#browseHeadline').html('Select a Product Type');
//		}

		if (hash_parts.length == 2) {
			//$('#browes-categories').fadeOut('fast');
			//$('#aisle').fadeOut('fast');
			//if(!gm_first_time && gm_registered_postcode && gm_logged_in){
				//$('#productType').fadeOut('fast');
			//}
			/*else
			{
				$('#overlay-content #overlay-header').html('<p>Want to register?</p>');
				$('#overlay-content #message').html('<p>Dont want to see this pop up gain? Just <a href="register.php">register</a> your details and be taken to your local pricing automatically.</p>')
				$('#overlay').fadeIn('fast', function(){
					$('#overlay-content').fadeIn('fast');	
				});
				return false;
			}*/
			//showProductType(current_aisle, true);
			//$('#productType').fadeOut('fast');
			getProducts(hash_parts[1], true,"","","");
		}
		
		if (hash_parts.length == 3) {
			
			$('#aisle').fadeIn('fast');
			//if(!gm_first_time && gm_registered_postcode && gm_logged_in){
				$('#productType').fadeOut('fast');
			//}
			/*else
			{
				$('#overlay-content #overlay-header').html('<p>Want to register?</p>');
				$('#overlay-content #message').html('<p>Dont want to see this pop up gain? Just <a href="register.php">register</a> your details and be taken to your local pricing automatically.</p>')
				$('#overlay').fadeIn('fast', function(){
					$('#overlay-content').fadeIn('fast');	
				});
				return false;
			}*/
			showProductType(hash_parts[0], true);
			//getProducts(hash_parts[1], true,"","","");
		}
		
		return;
	});
	  
	  // Since the event is only triggered when the hash changes, we need to trigger
	  // the event now, to handle the hash the page may have loaded with.
	  $(window).hashchange();
	  
});

$(document).ready(function(e) {
	//	alert("gm_first_time? " + gm_first_time);
	//	alert("gm_registered? " + gm_registered);
	//	alert("gm_registered_postcode? " + gm_registered_postcode);
	//	alert("gm_logged_in? " + gm_logged_in);
	$('#productType .category li a.has-sales, .search-results-list li a').live('click', function(e) {
        
		/*adrian start*/
		
		
		
		if ( !gm_registered_postcode ) {
			if (!gm_registered){				
				$('#overlay-content #message').html('We dont have your postcode yet, would you like to <a href="login.php">login</a>/<a href="register.php">register</a> or simply enter your <a onclick="javascript:userEnterPostCode(); return false;">postcode</a>?');
				openOverlay();
			} else {
				$('#overlay-content #message').html('You haven\'t set your postcode yet, <a href="edit-profile.php">please click here to edit your profile.</a>');
				openOverlay();
			}
			return false;
			
		} else if (gm_session_first_page && (! gm_registered)) {
			alert("Thanks for telling us your postcode before. Maybe you should register now...");
			
		} 
	});
//		$('#productType').fadeOut('fast');
//		$('#overlay-content #overlay-header').html('<p>Want to register?</p>');
//		$('#overlay-content #message').html('<p>Dont want to see this pop up gain? Just <a href="register.php">register</a> your details and be taken to your local pricing automatically.</p>')
//		$('#overlay').fadeIn('fast', function(){
//			$('#overlay-content').fadeIn('fast');	
//		});
//	}
});
function userEnterPostCode(){
	var the_postcode = null;
	while (! the_postcode) {
		var the_postcode = prompt("What postcode should we use for you?");
		if ((the_postcode == null) || (the_postcode == "")) {
			alert("Please tell us your postcode");
			continue;
		}

		var clean_postcode = parseInt(the_postcode, 10);
		if (isNaN(clean_postcode)) {
			alert(the_postcode + " is not a valid postcode");
			the_postcode = null;
			continue;
		}
		
		if ((clean_postcode < 1000) || (clean_postcode > 9999)) {
			alert(the_postcode + " is not a valid postcode");
			the_postcode = null;
			continue;
		}
		
		the_postcode = clean_postcode;
	}
	
	$.ajax({
		type: "POST",
		url: "set-postcode.php",
		dataType: "json",
		data: {
			postcode: the_postcode
		}
	}).success(function(data) {
		if (data.success) {
			location.reload();
		} else {
			alert(data.message);
		}
	});	
}
function openOverlay(){
	$('#overlay-content #message').css('display', 'block');
	$('#overlay-content').fadeIn('fast');	
	$('#overlay').fadeIn('fast');
}
function closeOverlay(){
	$('#overlay').fadeOut('fast');
	$('#overlay-content').fadeOut('fast');	
}

function fnFilter()
{
	
	var strFilterOrder = $('#filterorder').val();
	var strFilterWith = $('#filterwith').val();
	
	
	
	if(strFilterWith == "0")
	{
		alert("Please select a filteration criteria");
	}
	else
	{
		var hash = location.hash.replace(/^#/, '');
		hash = hash.split('/');
		hash = hash[0];
		
		var hash_parts = new Array();
		if (hash != "") {
			hash_parts = hash.split('-');
		}
		
		if (hash_parts.length == 1) {
			current_aisle = hash_parts[0];
			getProducts('',true,strFilterWith,strFilterOrder,current_aisle);
			
			
			//alert(hash_parts[1])
			
			
			//alert(current_aisle);
		}
		
		if (hash_parts.length == 2) {
			var product_type = hash_parts[1];
			getProducts(product_type, true,strFilterWith,strFilterOrder,'');
		}
	
		
	}
}