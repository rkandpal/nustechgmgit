<?php /*session_start();*/ //error_reporting(0); ?>
<?php require_once('functions.php') ?>
<?php include('includes/Mobile_Detect.php') ?>
<?php StartSession(); ?>
<?php 
	$gm_cookie = new GM_Cookie();
	/*
	error_log("1st time? " . (! $gm_cookie->has_visited_before));
	error_log("Registered? " . $gm_cookie->is_registered);
	error_log("Logged in? " . $gm_cookie->IsUserLoggedIn());
	error_log("Postcode? " . $gm_cookie->registered_postcode);
	*/
	$gm_first_time = (! $gm_cookie->has_visited_before);
	$gm_registered = $gm_cookie->is_registered;
	$gm_registered_postcode = $gm_cookie->registered_postcode;	
	$gm_logged_in = $gm_cookie->IsUserLoggedIn();
	$gm_session_first_page = (! isset($_COOKIE['gm_session_started']));
	setcookie('gm_session_started');
	
	// We have to do this after pulling all of the variables out, as we will change the 
	// variables on the object when we call the function...
	$gm_cookie->WriteBackCookie();
?>
<?php $detect = new Mobile_Detect(); ?>
<?php 
	if((!isset($_SESSION['list_id']))|| $_SESSION['list_id'] == false)
	{
		 $_SESSION['list_id'] = get_last_id();
	}	
?>
<?php //$current_list = (isset($_SESSION['list_id']))? $_SESSION['list_id'] : get_last_id(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<base href="<?php
		if ($config->site->url == 'http://www.grocerymate.com.au/') {
			print 'http://' . $_SERVER['HTTP_HOST'] . '/';
		} else {
			print $config->site->url;
		} 
		?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <title><?php print ( !empty($head_title) ? implode(' / ', array_reverse($head_title)).' - ' : '' ) . $config->site->name; ?></title>
		<!--<link href="tpl/css/reset.css" type="text/css" rel="stylesheet" media="screen" />-->
		<!--<link href="tpl/css/screen.css" type="text/css" rel="stylesheet" media="screen" />-->
        <script language="javascript" type="text/javascript" src="js/jquery.js"></script>
		<?php if ($detect->version('iPhone') > 0 || $detect->version('iPod') > 0): ?>
        		<script language="javascript" type="text/javascript">
					var appleDevice = true;
					$(document).ready(function(e) {
                        window.scrollTo(0,1);
                    });
				</script>
        		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0;" />
        <?php else: 
				if($detect->version('ipad')): ?>
                	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0;" />
                    <script language="javascript" type="text/javascript">
						$(document).ready(function(e) {
                            window.scrollTo(0,1);
                        });
					</script>
          <?php endif; ?>
        		<link rel="stylesheet" type="text/css" href="css/font-face.css" />
                <script language="javascript" type="text/javascript">
					var appleDevice = false;
				</script>
        		
        <?php endif; ?>
		<link href="tpl/js/jquery.wysiwyg/jquery.wysiwyg.css" type="text/css" rel="stylesheet" media="screen" />
        <link rel="shortcut icon" type="image/x-icon" href="tpl/img/icon.ico">
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <link type="text/css" rel="stylesheet" href="css/search.css" />
        <link type="text/css" rel="stylesheet" href="css/forms.css" />
        <link type="text/css" rel="stylesheet" href="css/shopoping-list.css" />
        <link type="text/css" rel="stylesheet" href="css/print.css" media="print" />
        <link type="text/css" rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
        <link type="text/css" rel="stylesheet" media="only screen and (max-width: 1024px)" href="css/ipad.css"  />
        <link type="text/css" rel="stylesheet" media="only screen and (max-width: 480px)" href="css/iphone.css" />
        <?php 
		$document_ready_js_stats = stat(dirname(__FILE__) . '/js/document.ready.js');
		$functions_js_stats = stat(dirname(__FILE__) . '/js/functions.js');
		?>
        
        <script language="javascript" type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery.mCustomScrollbar.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery.ba-hashchange.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/document.ready.js?<?php echo $document_ready_js_stats['mtime']; ?>"></script>
        <script language="javascript" type="text/javascript" src="js/custom.form.elements.js"></script>
        <script language="javascript" type="text/javascript" src="js/functions.js?<?php echo $functions_js_stats['mtime']; ?>"></script>
        <!--/*<script src="tpl/js/jquery.js" type="text/javascript" language="javascript"></script>*/-->
        <script src="tpl/js/jquery.wysiwyg/jquery.wysiwyg.js" type="text/javascript" language="javascript"></script>
        <script src="tpl/js/main.js" type="text/javascript" language="javascript"></script>
        <script language="javascript" type="text/javascript">
			//console.log("shopping list id: <?php //echo $_SESSION['list_id'] ?> \n user id: <?php //echo $_SESSION['user_id'] ?>")
		</script>
        <script type="text/javascript">
	var gm_first_time = <?php echo ($gm_first_time ? 'true' : 'false'); ?>;
	var gm_registered = <?php echo ($gm_registered ? 'true' : 'false'); ?>; 
	var gm_registered_postcode = '<?php echo $gm_registered_postcode; ?>'; 	
	var gm_logged_in = <?php echo ($gm_logged_in ? 'true' : 'false'); ?>; 
  	var gm_session_first_page = <?php echo ($gm_session_first_page ? 'true' : 'false'); ?>
  	
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35670934-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

	
</script>
    </head>
    
    <body>
    	<div id="overlay">
        	
        </div>
        <div id="overlay-content">
            <div id="overlay-header"><div id="close" onClick="closeOverlay();">x</div></div>
            <div id="message"></div>
            
        </div>
        <div id="corner-banner">
        	<p>On average our shoppers save per shop: <b>$<?php echo get_average_savings(); ?></b></p>
        </div>
    	<!--<div id="wrapper" class="clear-fix">-->
            
            
           <!-- <div id="body">-->

                <div id="toolbar" class="shadow dark-panel">
                    <ul class="toolbar-nav">
                        <li><a class="inline first main" href="">Home</a></li>
                        <!--<li><a class="inline" href="search.php">Search Products</a> </li>-->
           
                        <!--<li><a class="main inline" title="View other registered members" href="members.php">Members</a></li>-->
<?php
	if($user->isAuthorized()){
		$_SESSION['user_id'] = $user->getId();
?>
                        <li><a<?php print (($avatar = $user->getAvatar()) ? ' class="image main inline" ' : ' class="main inline"'); ?> href="members.php?profile=<?php print $user->getId(); ?>">My Account <span class="special">&#9662;</span></a>
                            <ul class="dark-panel shadow">
                                <li><a href="edit-profile.php">Edit Profile</a></li>
                                <?php print $user->getType() === MK_RecordUser::TYPE_CORE ? '<li><a href="change-password.php">Change Password</a></li>' : '' ?>
                                <li><a href="logout.php">Logout</a></li>
                                <?php print $user->getGroup()->isAdmin() ? '<li><a href="admin">Admin</a></li>' : '' ?>
                            </ul>
                        </li>
                        <li><a<?php print (($avatar = $user->getAvatar()) ? ' class="image main inline" ' : ' class="main inline"'); ?> href="members.php?profile=<?php print $user->getId(); ?>">Saved Shopping Lists</a>
<?php
	}else{
?>
                        <li><a class="main inline" href="login.php">Login</a></li>
                        <li><a class="main inline" href="register.php">Register</a></li>
<?php
	}
?>
						<li><a href="main_list_page.php" class="main inline last">View Basket</a></li>
                        
                    </ul>
                </div>
                <?php /* if($head_title[0] == 'Home'){ ?>
                	<div id="banner"><img src="images/banner.png" /></div>
                <?php }*/ ?>
				<h1 id="logo">
					<a title="Return to homepage" href="<?php print $config->site->url; ?>">
                    	<img src="images/logo.png" />
                    </a>
                </h1>
				<!--<div id="content" class="clear-fix">-->