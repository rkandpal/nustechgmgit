<?php 
error_reporting(0);
require_once('config.php');
require_once('_inc.php');
require_once('functions.php');

function QueryErrorLog($the_query) {
	$the_query = str_replace(array("\n", "\t"), ' ', $the_query);
	error_log($the_query);
}

$json_response = array('success' => false, 'error_text' => null, 'result' => null);
$target_filename = ('user-' . basename($_FILES['uploaded_file']['name']));

// What would be the target directory here?
$the_ean = $_POST['the_ean'];
$target_directory = 'EAN-' . strlen($the_ean) . '/';
$exploded_ean = str_split($the_ean, 4);
$target_directory .= implode('/', $exploded_ean);
$images_root_directory = realpath(dirname(__FILE__) . '/images/') . '/';
$full_directory = ($images_root_directory . $target_directory);

if (! file_exists($full_directory)) {
	mkdir($full_directory, 0777, true);
}

$full_copy_to_path = ($full_directory . '/' . $target_filename);

if (move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $full_copy_to_path)) {
	$json_response['success'] = true;
	$json_response['result'] = ($target_directory . '/' . $target_filename);
} else {
	$json_response['error_text'] = 'There was a problem copying the uploaded file';
}
die(json_encode($json_response));
?>