<?php 
error_reporting(0);
require_once('config.php');
require_once('_inc.php');
require_once('functions.php');

function QueryErrorLog($the_query) {
	$the_query = str_replace(array("\n", "\t"), ' ', $the_query);
	error_log($the_query);
}

$head_title = array();
$head_title[] = 'Product';
$search = trim($_POST['search']);
$ean_search = (isset($_POST['ean_search']) && $_POST['ean_search']);

$json_response = array('status' => null, 'status_text' => null, 'data' => array());
$json_response['data'] = array('brand' => array(), 'product_type' => array(), 'product' => array());

$db_link = new mysqli(DB_HOST, DB_USER, DB_PASS);
if (! $db_link) {
	$json_response['status'] = 'fail';
	$json_response['status_text'] = 'There was a problem. Please try again later...';
	die(json_encode($json_response));
}

if (! $db_link->select_db(DB_NAME)) {
	$json_response['status'] = 'fail';
	$json_response['status_text'] = 'There was a problem. Please try again later...';
	die(json_encode($json_response));
}

/*
 * Even though we reuse this string a lot it's OK to escape it now, as we're only going
 * to be breaking on spaces anyway...
 */ 
$search = $db_link->real_escape_string($search);

/*
 * EAN Search: return all products that contain the given string in either the name, the
 * technical name or the description. Then, if there is more than one word in the given
 * string, match all of the products that contain all of those words in either the name, 
 * the technical name or the description. Finally, add all of the products that have at
 * least one of those words in any of those fields. We do it in this order to try and 
 * achieve some sort of 'order by relevance'.
 * 
 * EAN Search only returns searches within products.
 */
function MaybeAddToEANSearchResults($match_type, $possible_result_to_add, &$product_ids_found, &$json_product_list) {
	$add_id = intval($possible_result_to_add['id']);
	$add_name = trim($possible_result_to_add['name']);
	if (! $add_name) {
		$add_name = trim($possible_result_to_add['technical_name']);
	}
	$add_ean = trim($possible_result_to_add['ean']);
	$add_match_type = $match_type;
	
	if (! in_array($add_id, $product_ids_found)) {
		$product_ids_found[] = $add_id;
		
		$json_product_list[] = array('id' => $add_id, 
									'name' => RewriteSmartQuotes($add_name), 
									'ean' => $add_ean,
									'match' => $add_match_type);
	}
}

if ($ean_search) {
	$product_ids_found = array();
	$json_results = array();
	
	// Straight up match...
	$query = "SELECT * FROM products 
				WHERE name LIKE '%$search%' 
				OR description LIKE '%$search%' 
				OR technical_name LIKE '%$search%'";
	$result = $db_link->query($query);
	
	if ($result) {
		while ($row = $result->fetch_assoc()) {
			MaybeAddToEANSearchResults('phrase', $row, $product_ids_found, $json_results);
		}
	} else {
		error_log($db_link->error);
	}

	$search_parts = explode(' ', $search);
	$search_words = array();
	foreach ($search_parts as $search_part) {
		if (trim($search_part) !== '') {
			$search_words[] = trim($search_part);
		}
	}
	
	if (count($search_words)) {
		// All words exist somewhere...
		$query = "SELECT * FROM products 
					WHERE (name LIKE '%" . implode("%' AND name LIKE '%", $search_words) . "%')
					OR (description LIKE '%" . implode("%' AND description LIKE '%", $search_words) . "%')
					OR (technical_name LIKE '%" . implode("%' AND technical_name LIKE '%", $search_words) . "%')";
		$result = $db_link->query($query);
	
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				MaybeAddToEANSearchResults('all', $row, $product_ids_found, $json_results);
			}
		} else {
			error_log($db_link->error);
		}
		
		// At least one word exists somewhere...
		$query = "SELECT * FROM products 
					WHERE (name LIKE '%" . implode("%' OR name LIKE '%", $search_words) . "%')
					OR (description LIKE '%" . implode("%' OR description LIKE '%", $search_words) . "%')
					OR (technical_name LIKE '%" . implode("%' OR technical_name LIKE '%", $search_words) . "%')";
		$result = $db_link->query($query);
	
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				MaybeAddToEANSearchResults('any', $row, $product_ids_found, $json_results);
			}
		} else {
			error_log($db_link->error);
		}
	}

	$json_response['data']['product'] = $json_results;
	$json_response['status'] = 'success';
	$json_response['status_text'] = '';
	die(json_encode($json_response));
}

/*
 * A non EAN search: this is a typical string search, looking for brands, product types
 * and / or products that match a given string. Having said that, it's a little deeper 
 * than that. If a product matches the string, then its product type and its brand should
 * automatically be added to the appropriate list as well.
 * 
 * We only return products etc. that are currently on sale somewhere.
 * 
 * We also want to be really liberal with our search: this will let the query caching
 * do its thing when we need it to.
 * 
 * Because of what we are after, the brands will have to be sorted by their name in their 
 * list, even if the products would be sorted differently (according to their names). 
 * Because of this requirement, it seems to be easier to just do a few searches, each time
 * sorting and grouping by different aspects.
 */
// I'm not sure how "end_date >= NOW()" would resolve, so for now, I'll work it out myself.
// We could eventually even work out the fact that we know which 'sale week' we're in, so 
// any query for any day during that week is *exactly* the same...
$now_date_string = date('Y-m-d');

$main_query = "SELECT brands.id AS brand_id, brands.name AS brand_name, 
					product_types.id AS product_type_id, product_types.name AS product_type_name,
					products.id AS product_id, products.ean AS product_ean,
					products.name AS product_name, products.technical_name AS product_technical_name
				FROM product_sales
				INNER JOIN products ON products.id=product_sales.product_id
				INNER JOIN product_types ON product_types.id=products.type
				INNER JOIN brands ON brands.id=products.brand
				WHERE
					product_sales.start_date <= '$now_date_string'
					AND product_sales.end_date >= '$now_date_string'
					AND (brands.name LIKE '%$search%'
						OR product_types.name LIKE '%$search%'
						OR (products.name != '' AND products.name LIKE '%$search%')
						OR (products.technical_name != '' AND products.technical_name LIKE '%$search%'))";

// Brands?
$query = $main_query . ' GROUP BY brands.id ORDER BY brands.name ASC';
$result = $db_link->query($query);
	
if ($result) {
	while ($row = $result->fetch_assoc()) {
		$brand_row = array('id' => intval($row['brand_id']), 'name' => RewriteSmartQuotes($row['brand_name']));
		$brand_row['hash_url'] = '#brand-' . intval($row['brand_id']);
		$brand_row['hash_url'] .= '/' . preg_replace('/[^a-z0-9]/i', '-', RewriteSmartQuotes($row['brand_name']));
		$json_response['data']['brand'][] = $brand_row;
	}
} else {
	error_log($db_link->error);
}

// Product types?
$query = $main_query . ' GROUP BY product_types.id ORDER BY product_types.name ASC';
$result = $db_link->query($query);

if ($result) {
	while ($row = $result->fetch_assoc()) {
		$json_response['data']['product_type'][] = array('id' => intval($row['product_type_id']), 'name' => RewriteSmartQuotes($row['product_type_name']));
	}
} else {
	error_log($db_link->error);
}

// Products?
$query = $main_query . ' GROUP BY products.id ORDER BY products.name ASC';
$result = $db_link->query($query);

if ($result) {
	while ($row = $result->fetch_assoc()) {
		$name_to_use = trim($row['product_name']);
		if (! $name_to_use) {
			$name_to_use = trim($row['product_technical_name']);
		}
		$json_response['data']['product'][] = array('id' => intval($row['product_id']), 
													'name' => RewriteSmartQuotes($name_to_use), 
													'ean' => $row['product_ean'],
													'match' => 'phrase');
	}
} else {
	error_log($db_link->error);
}

$json_response['status'] = 'success';
$json_response['status_text'] = '';
die(json_encode($json_response));
?>