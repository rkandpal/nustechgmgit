<?php
	session_start();
	require_once(dirname(__FILE__) . '/functions.php');
	require_once(dirname(__FILE__) . '/config.php');
	
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
		$query = "SELECT product_sales.id, brands.name, product_sales.price, product_sales.special_price, supermarkets.name
				FROM products, product_sales, brands, shopping_lists_products, supermarkets 
				WHERE  shopping_lists_products.shopping_list_id = 1 AND 
						product_sales.id = shopping_lists_products.shopping_list_product_sale_id AND 
						products.id = shopping_lists_products.shopping_list_product_id AND 
						product_sales.product_id = shopping_lists_products.shopping_list_product_id AND 
						products.brand = brands.id AND
						product_sales.supermarket_id = supermarkets.id";
		$query .= (isset($_GET['supermarket']) && $_GET['supermarket'] != '')? " AND supermarkets.name = '".$_GET['supermarket']."'" : "";
		$query .= " ORDER BY supermarkets.name ASC";
	$results = $db_link->prepare($query);
	
	$results->bind_result($id, $brand, $price, $special, $supermarket);
	$results->execute();
	$results->store_result();
	
	$supermarkets = $db_link->prepare("SELECT name FROM supermarkets");
	$supermarkets->bind_result($name);
	$supermarkets->execute();
	$supermarkets->store_result();
	
	$total_price = 0;
	$total_special = 0;
	$savings = array();
	
?>
	<?php require_once '_inc.php'; ?>
    <?php require_once '_header.php'; ?>
    <div id="shopping-list-container">
    <div id="big_shopping_list">
        <form action="form-actions/shoppinglist-form.php" method="post" name="shoppinglist" id="shoppinglist-list-form">
            <fieldset>
                <legend class="form-title top-rounded-corners cart">CHECK LIST</legend>
                <table width="99%" cellpadding="0" cellspacing="0" align="center">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>RRP</th>
                            <th>Special</th>
                            <th>Available At</th>
                            <th>List</th>
                        </tr>
                    </thead>
                   
                    <tbody>
                <?php
                    if($results->num_rows > 0):
                        while($results->fetch()):
						
							//Add values for each supermarket
							$savings = store_saving($price, $special, $supermarket, $savings);
                    ?>
                            <tr>
                                <td align="center"><?php echo $brand; ?></td>
                                <td align="center">$<?php echo $price; ?></td>
                                <td align="center">$<?php echo $special; ?></td>
                                <td align="center"><?php echo $supermarket ?></td>
                                <td align="center"><input type="checkbox" checked="checked" name="product[]" id="<?php echo $id; ?>" class="" onchange="javascript:removeFromBigBasket(<?php echo $id ?>);" /></td>
                            </tr>
                    <?php	
                            $total_price += $price;
                            $total_special += $special;
                        endwhile;
                    else:
                    ?>
                        <tr><td colspan="4" align="center">There are no items in your basket</td></tr>
                    <?php
                    endif;
                ?>
                    </tbody>
                    <tfoot>
                        <?php if ($results->num_rows > 0): ?>
                            <tr>
                                <td align="center">Total</td>
                                <td align="center">$<span id="total"><?php echo $total_price ?></span></td>
                                <td align="center">$<span id="special-total"><?php echo $total_special ?></span></td>
                                <td align="center">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                            </tr>
                        <?php endif; ?>
                        
                        <?php foreach ($savings as $supermarket_name => $value): ?>
                            <tr>
								<td colspan="5">Total savings for <b><?php echo $supermarket_name ?></b> are: $<?php echo $value ?></td>
                            </tr>    
						<?php endforeach; ?>
                        	<tr>
                            	
                                <td colspan="3" class="" align="left">
                                 &nbsp;
                                   </td>
                                   <td colspan="2" class="border-top buttons">                  
                                		&nbsp;
                           		</td>
                                </fieldset>
                           	</form>
                      	</tr>
       		</tfoot>
       </table>
    </div>
</div>
<?php require_once '_footer.php'; ?>
<?php 
	function store_saving ($price, $special, $supermarket, $savings)
	{
			$savings[$supermarket] += floatval($price) - floatval($special);		
		return $savings;
	}
?>