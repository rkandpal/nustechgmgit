<?php 
	require_once '_inc.php';
	require_once 'functions.php';
	$head_title = array();
	$head_title[] = 'Terms &amp; Conditions';
	require_once '_header.php';
	
?>
<div id="basic-content">
    <h2>WEBSITE TERMS AND CONDITIONS</h2>

<h4>WEBSITE TERMS AND CONDITIONS</h4>
<p>In these terms and conditions, “we” “us” and “our” refers to Iron Logic Pty Ltd.  Your access to and use of all information on this website including purchase of our service/s is provided subject to the following terms and conditions.  The information is intended for residents of Australia only.</p>
<p>We reserve the right to amend this Notice at any time and your use of the website following any amendments will represent your agreement to be bound by these terms and conditions as amended.  We therefore recommend that each time you access our website you read these terms and conditions.</p>
<p><strong>Members</strong><br />
1.	In order to have full access the services provided on this website, you must become a member. You must complete registration by providing certain information as set out on our membership/registration page.  Please refer to our Privacy Policy linked on our home page for information relating to our collection, storage and use of the details you provide on registration.<br />
2.	You agree to ensure that your registration details are true and accurate at all times and you undertake to update your registration details from time to time when they change.<br />
3.	On registration, we provide you with a password. On registration you agree to pay for our services as set out on our website.<br />
4.	We reserve the right to terminate your membership at any time if you breach these terms and conditions.<br />
5.	Our services are intended to be used by members within Australia only.<br /><br />
<strong>Our Website Services</strong><br />
6.	Our services are provided to adults over the age of eighteen (18) years.  By proceeding to purchase through our website, you acknowledge that you are over 18 years of age. <br />
7.	All prices are in Australian Dollars (AUD) and are inclusive of GST.  We endeavour to ensure that our price list is current.  Our price list can be accessed from our home page and we reserve the right to amend our prices at any time.  If you have placed an order, we reserve the right to cancel your order should our prices change. <br /><br />
<strong>Site Access</strong><br />
17.	When you visit our website, we give you a limited licence to access and use our information for personal use. <br />
18.	You are permitted to download a copy of the information on this website to your computer for your personal use only provided that you do not delete or change any copyright symbol, trade mark or other proprietary notice.  Your use of our content in any other way infringes our intellectual property rights.<br />
19.	Except as permitted under the Copyright Act 1968 (Cth), you are not permitted to copy, reproduce, republish, distribute or display any of the information on this website without our prior written permission.<br />
20.	The licence to access and use the information on our website does not include the right to use any data mining robots or other extraction tools.  The licence also does not permit you to metatag or mirror our website without our prior written permission. We reserve the right to serve you with notice if we become aware of your metatag or mirroring of our website.<br /><br />
<strong>Hyperlinks</strong><br />
21.	This website may from time to time contain hyperlinks to other websites.  Such links are provided for convenience only and we take no responsibility for the content and maintenance of or privacy compliance by any linked website.  Any hyperlink on our website to another website does not imply our endorsement, support, or sponsorship of the operator of that website nor of the information and/or products which they provide.<br />
22.	You may link our website only with our consent.  Any such linking will be entirely your responsibility and at your expense.  By linking, you must not alter any of our website's contents including any intellectual property notices and you must not frame or reformat any of our pages, files, images, text or other materials.<br /><br />
<strong>Intellectual Property Rights</strong><br />
24.	The copyright to all content on this website including applets, graphics, images, layouts and text belongs to us or we have a licence to use those materials.<br />
25.	All trade marks, brands and logos generally identified either with the symbols TM or ® which are used on this website are either owned by us or we have a licence to use them. Your access to our website does not license you to use those marks in any commercial way without our prior written permission.<br />
26.	Any comment, feedback, idea or suggestion (called “Comments”) which you provide to us through this website becomes our property.  If in future we use your Comments in promoting our website or in any other way, we will not be liable for any similarities which may appear from such use.  Furthermore, you agree that we are entitled to use your Comments for any commercial or non-commercial purpose without compensation to you or to any other person who has transmitted your Comments. <br />
27.	If you provide us with Comments, you acknowledge that you are responsible for the content of such material including its legality, originality and copyright. <br /><br />
<strong>Disclaimers</strong><br />
28.	Whilst we take all due care in providing our services, we do not provide any warranty either express or implied including without limitation warranties of merchantability or fitness for a particular purpose. <br />
29.	To the extent permitted by law, any condition or warranty which would otherwise be implied into these terms and conditions is excluded.  <br />
30.	We also take all due care in ensuring that our website is free of any virus, worm, Trojan horse and/or malware, however we are not responsible for any damage to your computer system which arises in connection with your use of our website or any linked website.<br />
31.	From time to time we may host third party content on our website such as advertisements and endorsements belonging to other traders.  Responsibility for the content of such material rests with the owners of that material and we are not responsible for any errors or omissions in such material. <br /><br />
<strong>Statutory Guarantees and Warranties to Consumers </strong><br />
32.	Schedule 2 of the Competition and Consumer Act 2010 (“C&C Act”) defines a consumer.  Under the C&C Act we are a supplier of either goods or services or both to you, and as a consumer the C&C Act gives you statutory guarantees.  Attached to the Standard Terms and Conditions are:-<br />
&nbsp;&nbsp;a.	Schedule 2 of the C&C Act; and<br />
&nbsp;&nbsp;b.	those statutory guarantees, all of which are given by us to you if you are a consumer.<br />
33.	If you are a consumer within the meaning of Schedule 2 of the C&C Act of our goods or services then we give you a warranty that at the time of supply of those goods or services to you, if they are defective then:-<br />
&nbsp;&nbsp;a.	We will repair or replace the goods or any part of them that is defective; or<br />
&nbsp;&nbsp;b.	Provide again or rectify any services or part of them that are defective; or<br />
&nbsp;&nbsp;c.	Wholly or partly recompense you if they are defective.<br />
34.	As a consumer under the C&C Act you may be entitled to receive from us notices under Schedule 2 section 103 of the C&C Act.  In that regard:-<br />
&nbsp;&nbsp;a.	If you are a consumer within the meaning of Schedule 2 of the C&C Act and the goods or services we are providing relate to the repair of consumer goods then we will give you any notice which we are obliged to give you under Schedule 2 section 103 of the C&C Act.<br />
&nbsp;&nbsp;b.	If we are a repairer of goods capable of retaining user-generated data then we hereby give you notice that the repair of those goods may result in the loss of the data.  <br />
&nbsp;&nbsp;c.	If we are a repairer and our practice is to supply refurbished goods as an alternative to repairing your defective goods or to use refurbished parts in the repair, then we give you notice that the goods presented by you to us for repair may be replaced by refurbished goods of the same type rather than being repaired.  We also give you notice that we may use in the repair of your goods, refurbished parts.<br /><br />
<strong>Limitation of Liability</strong><br />
35.	If you are not a consumer within the meaning of Schedule 2 of the C&C Act then this clause applies to you.  If you are a consumer within the meaning of the C&C Act then this clause has no effect whatsoever to in any way limit our liability or your rights.  If you are not a consumer:- <br />
&nbsp;&nbsp;a.	To the full extent permitted by law, our liability for breach of an implied warranty or condition is limited to the supply of the services again or payment of the costs of having those services supplied again. <br />
&nbsp;&nbsp;b.	We accept no liability for any loss whatsoever including consequential loss suffered by you arising from services we have supplied. <br />
&nbsp;&nbsp;c.	We do not accept liability for anything contained in the post of a user or in any form of communication which originates with a user and not with Us.<br />
&nbsp;&nbsp;d.	We do not participate in any way in the transactions between our users.<br /><br />
<strong>Indemnity</strong><br />
36.	By accessing our website, you agree to indemnify and hold us harmless from all claims, actions, damages, costs and expenses including legal fees arising from or in connection with your use of our website.  <br /><br />
<strong>Jurisdiction</strong><br />
37.	These terms and conditions are to be governed by and construed in accordance with the laws of Australia and any claim made by either party against the other which in any way arises out of these terms and conditions will be heard in Australia and you agree to submit to the jurisdiction of those Courts.<br />
38.	If any provision in these terms and conditions is invalid under any law the provision will be limited, narrowed, construed or altered as necessary to render it valid but only to the extent necessary to achieve such validity.  If necessary the invalid provision will be deleted from these terms and conditions and the remaining provisions will remain in full force and effect.<br /><br />
<strong>Privacy </strong><br />
39.	We undertake to take all due care with any information which you may provide to us when accessing our website. However we do not warrant and cannot ensure the security of any information which you may provide to us.  Information you transmit to us is entirely at your own risk although we undertake to take reasonable steps to preserve such information in a secure manner.<br />
40.	Our compliance with privacy legislation is set out in our separate Privacy Policy which may be accessed from our home page. <br />
</p>
<p>These Terms and Conditions have been specifically drafted for, and provided to Iron Logic Pty Ltd by LawLive Pty Ltd (www.lawlive.com.au).</p>

</div><!-- basic-content -->

<?php

	require_once '_footer.php';
	
?>