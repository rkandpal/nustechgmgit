<?php
	require_once '_inc.php';
	$head_title = array();
	$head_title[] = 'Forgotten Password';

	if($user->isAuthorized()){
		header('Location: '.MK_Utility::serverUrl('/'), true, 302);
		exit;
	}
	
	$output = '<div id="forgotten">';
	$output .= '<div id="forgotten-form">';
	$output .= '<legend class="form-title top-rounded-corners">Forgotten Password</legend>';
	$output .= '<p>Ener your email below, to reset your password.</p>';

	$settings = array(
		'attributes' => array(
			'class' => 'narrow clear-fix standard'
		)
	);

	$structure = array(
		'email' => array(
			'label' => 'Your Email',
			'validation' => array(
				'email' => array(),
				'instance' => array()
			)
		),
		'submit' => array(
			'type' => 'submit',
			'attributes' => array(
				'value' => 'Recover password'
			)
		)
	);

	$form = new MK_Form($structure, $settings);

	if($form->isSuccessful())
	{
		$user_type = MK_RecordModuleManager::getFromType('user');
		$user_search = array(
			array('field' => 'email', 'value' => $form->getField('email')->getValue()),
			array('field' => 'type', 'value' => MK_RecordUser::TYPE_CORE)
		);

		if( $reset_user = array_pop($user_type->searchRecords($user_search)) ){
			$new_password = MK_Utility::getRandomPassword(8);
			$reset_user
				->setTemporaryPassword($new_password)
				->save();

			$output = '<p class="alert success">We have sent you an email containing your new password.</p>';

			$message = '<p>Dear '.$reset_user->getDisplayName().',<br /><br />Your new password is: <strong>'.$new_password.'</strong>.</p>';
			$mailer = new MK_BrandedEmail();
			$mailer
				->setSubject('Password Recovery')
				->setMessage($message)
				->send($reset_user->getEmail(), $reset_user->getDisplayName());
		}else{
			$form->getField('email')->getValidator()->addError('This email does not match our records. Please try again.');
			$output .= $form->render();
		}
	}
	else
	{
		$output .= $form->render();
	}
	$output .= '</div>';
	$output .= '</div>';

	require_once '_header.php';
	print $output;
	require_once '_footer.php';
?>