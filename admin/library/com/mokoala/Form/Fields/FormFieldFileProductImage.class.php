<?php

require_once 'FormFieldFileImage.class.php';

class MK_Form_Field_File_Product_Image extends MK_Form_Field_File_Image
{
	protected $ean_field = null;
	public function __construct($name, $field_data = null)
	{
		$this->ean_field = (isset($field_data['ean_field']) ? $field_data['ean_field'] : null);
		$field_data['upload_path'] = '../images/';
		parent::__construct( $name, $field_data );
	}

	public function setValue($post_data = null)
	{

		if(is_string($post_data))
		{
			$this->value['existing'] = $post_data;
		}
		elseif(is_array($post_data))
		{
			$this->value = array_merge_replace( $this->value, $post_data );
		}

	}

	public function getValue()
	{
		if( !empty($this->value['new']) )
		{
			return $this->value['new'];
		}
		elseif( !empty($this->value['existing']) )
		{
			return $this->value['existing'];	
		}
		else
		{
			return null;	
		}
	}
	
	public function getFileRemoveLink()
	{

		if( !empty($this->file_remove_link) )
		{
			return $this->file_remove_link;
		}
		
	}
	
	public function process()
	{
		if (! $this->ean_field) {
			$this->getValidator()->addError('Sorry, an EAN needs to be set before uploading a file.');
			return;
		}
		
		if (! $this->ean_field['value']) {
			$this->getValidator()->addError('Sorry, an EAN needs to be set before uploading a file.');
			return;
		}
		
		$ean_length = strlen($this->ean_field['value']);
		if (($ean_length != 8) && ($ean_length != 13))  {
			$this->getValidator()->addError('Sorry, the given EAN is not valid.');
			return;
		}
		
		// Change the upload path based on the EAN
		$exploded_ean = str_split($this->ean_field['value'], 4);
		$ean_path = 'EAN-' . $ean_length . '/' . implode('/', $exploded_ean) . '/';
		if (! is_dir($this->upload_path . '/' . $ean_path)) {
			mkdir($this->upload_path . '/' . $ean_path, 0755, true);
		}
		
		if( !empty($this->value['name']) )
		{

			// Get file extension
			$extension = explode('.', $this->value['name']);
			$extension = array_pop( $extension );
			$extension = strtolower( $extension );

			// If the file's extension is invalid
			if( !empty($this->valid_extensions) && !in_array($extension, $this->valid_extensions) )
			{
				$this->getValidator()->addError("Sorry, <em>".$extension."</em> files are not valid.");
			}
			// If the file's mime type is invalid
			elseif( !empty($this->valid_mime_types) && !in_array($this->value['type'], $this->valid_mime_types) )
			{
				$this->getValidator()->addError("Sorry, <em>".$this->value['type']."</em> files are not valid.");
			}
			// If the file is too big
			elseif( $this->value['error'] === UPLOAD_ERR_INI_SIZE)
			{
				$this->getValidator()->addError("Sorry, the uploaded file exceeds the upload_max_filesize directive.");
			}
			elseif( $this->value['error'] === UPLOAD_ERR_FORM_SIZE)
			{
				$this->getValidator()->addError("Sorry, the uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.");
			}
			elseif( $this->value['error'] === UPLOAD_ERR_PARTIAL || $this->value['error'] === UPLOAD_ERR_EXTENSION )
			{
				$this->getValidator()->addError("Sorry, there was an error uploading this file.");
			}
			elseif( $this->value['error'] === UPLOAD_ERR_NO_TMP_DIR)
			{
				$this->getValidator()->addError("Sorry, missing a temporary folder.");
			}
			elseif( $this->value['error'] === UPLOAD_ERR_CANT_WRITE)
			{
				$this->getValidator()->addError("Sorry, failed to write file to disk.");
			}
			elseif( $this->value['error'] === UPLOAD_ERR_OK )
			{

				if(!empty($this->value['existing']))
				{
					try
					{
						$old_image = new MK_File('../images/' . $this->value['existing']);
						$old_image->delete();
					}
					catch(Exception $e){}
				}
	
				$path = $this->upload_path;
				$file_parts = explode('.', $this->value['name']);
				$ext = strtolower(array_pop($file_parts));
				$filename = slug(implode('.', $file_parts)).'.'.$ext;
				$counter=0;
				while(is_file($path.$filename))
				{
					$counter++;
					$filename = slug(implode('.', $file_parts)).'_'.$counter.'.'.$ext;
				}
		
				$filename = ($ean_path . $filename);
				$this->value['name'] = $filename;
				move_uploaded_file($this->value['tmp_name'], $path.$filename);
				$this->value['new'] = form_data(trim($filename, './'));
			
			}
			
		}
		elseif( array_key_exists('instance', $this->validation) && $this->value['error'] === UPLOAD_ERR_NO_FILE )
		{
			$this->getValidator()->addError("This field cannot be blank.");
		}
	}
	
	protected function renderField()
	{
	
		$html = '<label class="input-text" for="'.$this->getName().'">'.$this->getLabel().'</label>';
		$html .= '<div class="file-details">';
		$html .= '<input type="hidden" name="'.$this->getName().'[existing]" value="'.form_data($this->getValue()).'" />';
		if($this->getValue() && is_string($this->getValue()))
		{
			$html.='<img src="library/thumb.php?f=images/'.$this->getValue().'&h=240&w=250&m=contain" />';
			/* LJM
			if($this->getFileRemoveLink())
			{
				$html.='<p class="file-remove"><a href="'.$this->getFileRemoveLink().'">Remove file</a></p>';
			}
			*/
		}
	
		$html .= '<div class="input"><input'.($this->getAttributes()).' type="file" name="'.$this->getName().'" id="'.$this->getName().'" /></div>';
		$html .= '</div>';
	
		return $html;
	
	}
	
}

?>