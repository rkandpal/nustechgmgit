<?php

class MK_RecordUser extends MK_Record
{

	protected $group;

	const TYPE_CORE = 'core';
	const TYPE_FACEBOOK = 'facebook';
	const TYPE_TWITTER = 'twitter';
	const TYPE_YAHOO = 'yahoo';
	const TYPE_WINDOWS_LIVE = 'windows_live';
	const TYPE_GOOGLE = 'google';

	public function __construct( $module_id, $record_id = null ){
		
		parent::__construct( $module_id, $record_id );
		
		if(empty($record_id))
		{
			$group_type = MK_RecordModuleManager::getFromType('user_group');
			$search = array(
				array('field' => 'default_value', 'value' => '1')
			);
			$group = $group_type->searchRecords($search);
			$group = array_pop( $group );
			$this
				->setGroupId($group->getId())
				->setDateRegistered(date('Y-m-d H:i:s'))
				->setType( self::TYPE_CORE );
		}
		else
		{
			$user_meta_type = MK_RecordModuleManager::getFromType('user_meta');
			$search_criteria = array(
				array('field' => 'user', 'value' => $record_id)
			);
			$user_meta = $user_meta_type->searchRecords($search_criteria);

			foreach($user_meta as $meta)
			{
				$this->setMetaValue($meta->getKey(), $meta->getValue());
			}			
		}
		
	}

	public function setPassword($value)
	{
		$current_value = $this->getPassword();
		if( $current_value !== $value)
		{
			$value = MK_Utility::getHash($value);
			$this->setMetaValue('password', $value);
		}
		return $this;
	}

	public function setTemporaryPassword($value)
	{
		$current_value = $this->getPassword();
		if( $current_value !== $value)
		{
			$value = MK_Utility::getHash($value);
			$this->setMetaValue('temporary_password', $value);
		}
		return $this;
	}

	public function isAuthorized()
	{
		if( $this->getId() )
		{
			return true;
		}
		else
		{
			return false;	
		}
	}
	
	public function getGroup()
	{
		if(empty($this->group))
		{
			$group_id = $this->getGroupId();
			$group_type = MK_RecordModuleManager::getFromType('user_group');
			$this->group = MK_RecordManager::getFromId( $group_type->getId(), $group_id );
		}
		return $this->group;
	}
	
	public function sendVerificationEmail()
	{
		$config = MK_Config::getInstance();
		$email = new MK_BrandedEmail();
		
		if(!$verification_code = $this->getVerificationCode())
		{
			$verification_code = MK_Utility::getRandomString(20);
			$this->setVerificationCode($verification_code);
		}

		$verification_link = MK_Utility::serverUrl( "register.php?verification_code=".urlencode($verification_code) );
		$message = '<p>Hi, '.$this->getDisplayName().'!</p>
					<p>Thank you for joining <a href="http://www.grocerymate.com.au">grocerymate.com.au</a>. We\'re here to help you save time and money by searching the best in-store bargains for you.</p>
					<p>Your username is: '.$this->getEmail().'<br />please verifie your email by clicking on the following link <a href="'.$verification_link.'">'.$verification_link.'</a>.</p>
					<h2>Save Time &amp; Money with <a href="http://www.grocerymate.com.au">grocerymate.com.au</h2>
					<p>With your free <a href="http://www.grocerymate.com.au">grocerymate.com.au</a> account it\'s so easy to:<br />
					
					<ul><li></a><br />
						Save time - easy to shop by:<br />
						1. Selecting a Category or simply type in a product you need<br />
						2. Add the product to your shopping list<br />
						3. Review your savings and the cheapest supermarket by clicking on "View Basket".</li>
						<li>Save Money - we find the specials for you so you can save at least $30 per shop based on average Households weekly shop... that\'s over $1500 per year!</li></ul></p>
						<p>Thanks again for signing up and we can\'t wait for you to start saving on your groceries and liquor soon!<br />
						<strong>The grocerymate Team</strong></p>';
		$email
			->setSubject('Verify Email Address')
			->setMessage($message)
			->send($this->getEmail());
	}
	
	public function save( $update_meta = true )
	{

		$config = MK_Config::getInstance();

		if( !$this->getId() && !$this->isEmailVerified() && $config->extensions->core->email_verification && $this->getType() == self::TYPE_CORE )
		{
			$this->sendVerificationEmail();
		}
		elseif( $this->getType() != self::TYPE_CORE )
		{
			$this->isEmailVerified(1);
		}

		parent::save( $update_meta );

		if( $update_meta === true )
		{
			$user_meta_type = MK_RecordModuleManager::getFromType('user_meta');
	
			$search_criteria = array(
				array('field' => 'user', 'value' => $this->getId())
			);

			$user_meta = $user_meta_type->searchRecords($search_criteria);
			
			foreach($user_meta as $meta)
			{
				$meta->delete();
			}
	
			foreach($this->meta_extra as $key => $value)
			{
				if(!empty($value))
				{
					$new_meta = MK_RecordManager::getNewRecord($user_meta_type->getId());
					$new_meta
						->setKey($key)
						->setValue($value)
						->setUser($this->getId())
						->save( false );
				}
			}
		}

		return $this;	
	}

}

?>