<script type="text/javascript">
        $(document).ready(function () {
                if($('form').has('.field-unitprice'))
                {
                        $('.field-unitprice').css('visibility','hidden');
                        //var basePath = "http://"+window.location.hostname;
                        //alert(basePath);
                        
                        $('#price').change(function () {
                                var price = $('#price').val();
                                if(price == "" || price == "0")
                                {
                                        return false;
                                }
                                else
                                {
                                        var intProductSelected = $('#product_id').val();
                                        if(intProductSelected != "0")
                                        {
                                                //var basePath = "http://"+window.location.hostname
                                                $.ajax({
                                                        type: 'POST',
                                                        url: '../includes/getProductDetail.php',
                                                        dataType: "json",
                                                        data:{
                                                                productId : intProductSelected
                                                        },
                                                }).done(function(data){
                                                        //alert(data.result)
                                                        if(data.result == true)
                                                        {
                                                                var productMeasure = data.productmeasure;
                                                                //alert(price);                 
                                                                var UnitPrice = ((parseFloat(price))/(parseFloat(productMeasure))).toFixed(2);
                                                                //alert(UnitPrice);
                                                                //alert($('#unit_price'));
                                                                $('#unit_price').val(UnitPrice);
                                                        }
                                                        else
                                                        {
                                                                return false;
                                                        }
                                                })
                                        }
                                        else
                                        {
                                                return false;
                                        }
                                }
                        });
                }
                
                if($('form').has('.field-productsalecnt'))
                {
                        $('.field-productsalecnt').css('visibility','hidden');
                
                }
        });
</script>
<div class="block">
    <h2><?php print $this->module->getName(); ?> / <?php print $this->title; ?></h2>
    <?php
    
        print $this->form;
    
    ?>
</div>