<div class="block">
    <h2>Dashboard</h2>
    <p>Use the links above to navigate the CMS.</p>
<?php
if( count($this->message_list) > 0 )
{
	foreach( $this->message_list as $message )
	{
		print '<p class="simple-message simple-message-'.$message['type'].'">'.$message['message'].'</p>';
	}
}
?>
</div>
