ALTER TABLE `product_sales`
	ADD COLUMN `postcode` SMALLINT UNSIGNED NOT NULL AFTER `supermarket_id`;

ALTER TABLE `shopping_lists_products`
	ADD COLUMN `postcode` SMALLINT UNSIGNED NOT NULL AFTER `supermarket_id`;


