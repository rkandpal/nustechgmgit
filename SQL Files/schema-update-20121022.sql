ALTER TABLE `products`
	ALTER `uom` DROP DEFAULT;
ALTER TABLE `products`
	CHANGE COLUMN `uom` `uom` ENUM('kg','g','l','ml','per','pack') NOT NULL AFTER `measure`;


ALTER TABLE `product_sales`
	ADD COLUMN `promo_type` VARCHAR(100) NULL DEFAULT NULL AFTER `bogo_get_price`;


ALTER TABLE `shopping_lists`
	ADD COLUMN `user_id` INT(10) NULL DEFAULT NULL AFTER `total_special`,
	ADD COLUMN `visible` SMALLINT(6) NULL DEFAULT '1' AFTER `user_id`;


ALTER TABLE `shopping_lists_products`
	ADD COLUMN `supermarket_id` int(10) DEFAULT NULL AFTER `shopping_list_id`,
	ADD COLUMN `price` decimal(5,2) DEFAULT NULL NULL AFTER `supermarket_id`,
	ADD COLUMN `special_price` decimal(5,2) DEFAULT NULL NULL AFTER `price`,
	ADD COLUMN `type` int(10) DEFAULT NULL NULL AFTER `special_price`,
	ADD COLUMN `brand` int(10) DEFAULT NULL NULL AFTER `type`,
	ADD COLUMN `measure` decimal(9,4) DEFAULT NULL NULL AFTER `brand`,
	ADD COLUMN `uom` enum('kg','g','l','ml','per','pack') DEFAULT NULL NULL AFTER `measure`,
	ADD COLUMN `image` varchar(255) DEFAULT NULL NULL AFTER `uom`,
	ADD COLUMN `name` varchar(150) DEFAULT NULL NULL AFTER `image`,
	ADD COLUMN `description` varchar(300) DEFAULT NULL NULL AFTER `name`;
