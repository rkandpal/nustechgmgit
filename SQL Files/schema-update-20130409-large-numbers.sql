ALTER TABLE `product_sales`
	CHANGE COLUMN `price` `price` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `end_date`,
	CHANGE COLUMN `special_price` `special_price` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `price`,
	CHANGE COLUMN `bogo_buy_price` `bogo_buy_price` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `bogo_buy_count`,
	CHANGE COLUMN `bogo_get_price` `bogo_get_price` DECIMAL(7,2) UNSIGNED NULL DEFAULT NULL AFTER `bogo_get_count`;


ALTER TABLE `shopping_lists`
	ALTER `total_rrp` DROP DEFAULT,
	ALTER `total_special` DROP DEFAULT;
ALTER TABLE `shopping_lists`
	CHANGE COLUMN `total_rrp` `total_rrp` DECIMAL(7,2) NOT NULL AFTER `date`,
	CHANGE COLUMN `total_special` `total_special` DECIMAL(7,2) NOT NULL AFTER `total_rrp`;


ALTER TABLE `shopping_lists_products`
	CHANGE COLUMN `price` `price` DECIMAL(7,2) NULL DEFAULT NULL AFTER `postcode`,
	CHANGE COLUMN `special_price` `special_price` DECIMAL(7,2) NULL DEFAULT NULL AFTER `price`;


