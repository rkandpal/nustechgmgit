-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.63-0ubuntu0.11.10.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2012-07-17 23:53:27
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table noddingdog-grocery-mate.brands
DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table noddingdog-grocery-mate.brands: ~0 rows (approximately)
DELETE FROM `brands`;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;


-- Dumping structure for table noddingdog-grocery-mate.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `ean` decimal(13,0) unsigned NOT NULL,
  `brand` int(10) unsigned NOT NULL,
  `measure` decimal(9,4) unsigned NOT NULL,
  `uom` enum('kg','g','l','ml','per') NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_products_product_types` (`type`),
  KEY `FK_products_brands` (`brand`),
  CONSTRAINT `FK_products_brands` FOREIGN KEY (`brand`) REFERENCES `brands` (`id`),
  CONSTRAINT `FK_products_product_types` FOREIGN KEY (`type`) REFERENCES `product_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table noddingdog-grocery-mate.products: ~0 rows (approximately)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;


-- Dumping structure for table noddingdog-grocery-mate.products_similar
DROP TABLE IF EXISTS `products_similar`;
CREATE TABLE IF NOT EXISTS `products_similar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `similar_to_product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_products_similar_products` (`product_id`),
  KEY `FK_products_similar_products_2` (`similar_to_product_id`),
  CONSTRAINT `FK_products_similar_products_2` FOREIGN KEY (`similar_to_product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_products_similar_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table noddingdog-grocery-mate.products_similar: ~0 rows (approximately)
DELETE FROM `products_similar`;
/*!40000 ALTER TABLE `products_similar` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_similar` ENABLE KEYS */;


-- Dumping structure for table noddingdog-grocery-mate.product_sales
DROP TABLE IF EXISTS `product_sales`;
CREATE TABLE IF NOT EXISTS `product_sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `supermarket_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `price` decimal(5,2) unsigned DEFAULT NULL,
  `special_price` decimal(5,2) unsigned DEFAULT NULL,
  `promo_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_sales_products` (`product_id`),
  KEY `FK_product_sales_supermarkets` (`supermarket_id`),
  CONSTRAINT `FK_product_sales_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_product_sales_supermarkets` FOREIGN KEY (`supermarket_id`) REFERENCES `supermarkets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table noddingdog-grocery-mate.product_sales: ~0 rows (approximately)
DELETE FROM `product_sales`;
/*!40000 ALTER TABLE `product_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_sales` ENABLE KEYS */;


-- Dumping structure for table noddingdog-grocery-mate.product_sale_exceptions
DROP TABLE IF EXISTS `product_sale_exceptions`;
CREATE TABLE IF NOT EXISTS `product_sale_exceptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_sale_id` int(10) unsigned NOT NULL,
  `exception_store_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_sale_exceptions_products` (`product_sale_id`),
  KEY `FK_product_sale_exceptions_stores` (`exception_store_id`),
  CONSTRAINT `FK_product_sale_exceptions_products` FOREIGN KEY (`product_sale_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_product_sale_exceptions_stores` FOREIGN KEY (`exception_store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table noddingdog-grocery-mate.product_sale_exceptions: ~0 rows (approximately)
DELETE FROM `product_sale_exceptions`;
/*!40000 ALTER TABLE `product_sale_exceptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_sale_exceptions` ENABLE KEYS */;


-- Dumping structure for table noddingdog-grocery-mate.product_types
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE IF NOT EXISTS `product_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table noddingdog-grocery-mate.product_types: ~0 rows (approximately)
DELETE FROM `product_types`;
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;


-- Dumping structure for table noddingdog-grocery-mate.stores
DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supermarket_id` int(10) unsigned NOT NULL,
  `suburb` varchar(255) NOT NULL,
  `postcode` char(4) NOT NULL,
  `latitude` decimal(9,6) DEFAULT NULL,
  `longitude` decimal(9,6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_stores_supermarkets` (`supermarket_id`),
  CONSTRAINT `FK_stores_supermarkets` FOREIGN KEY (`supermarket_id`) REFERENCES `supermarkets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table noddingdog-grocery-mate.stores: ~0 rows (approximately)
DELETE FROM `stores`;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;


-- Dumping structure for table noddingdog-grocery-mate.supermarkets
DROP TABLE IF EXISTS `supermarkets`;
CREATE TABLE IF NOT EXISTS `supermarkets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table noddingdog-grocery-mate.supermarkets: ~0 rows (approximately)
DELETE FROM `supermarkets`;
/*!40000 ALTER TABLE `supermarkets` DISABLE KEYS */;
/*!40000 ALTER TABLE `supermarkets` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
