ALTER TABLE `shopping_lists_products`
	ADD COLUMN `shopping_list_product_count` TINYINT UNSIGNED NOT NULL DEFAULT '1' AFTER `id`;
