ALTER TABLE `product_types`
	DROP INDEX `name`,
	ADD UNIQUE INDEX `name` (`name`, `category_id`);
