<?php
	/* 
	/* This is the functions file, do as much of the computing in this file
	/* file as possible so the content will be neater and therefore easier 
	/* to change. 
	*/
	
	require_once('_inc.php');
	require_once('config.php');
?>

<?php 
	/* iniciate the page */
	
	function page_ini(){
		if(!isset($_GET['page'])):
			$_SESSION['home'] = true;
			//echo('Home is set to true');
		
		else:
			$_SESSION['page'] = $_GET['page'];
			$_SESSION['home'] = false;	
			//echo('Page has been set to '.$_GET['page']);
		endif;
	}
	/* include the search module when ever get_search() is called */
	function get_search(){
		include('includes/product-search.php');	
	}
	
	/* include the logo module when ever get_logo() is called */
	function get_logo(){
		include('includes/logo.php');	
	}
	/* include the banner when ever get_banner() is called */
	function get_banner(){
		include('includes/banner.php');
	}
	
	/* include the browse module when ever get_browse() is called */
	function get_browse(){
		include('includes/browse.php');	
	}
	
	/* include the header of the page*/
	function get_header(){
		include('user/_header.php');
	}
	function get_content(){
		include('includes/'.$_SESSION['page'].'.php');	
	}
	function get_page_title(){
		$title = ($_SESSION['home'] == true)? '': str_replace('-',  ' ', $_SESSION['page']);
		$title = ' | ' . ucwords($title);
		return $title;
	}
	function get_navigation()
	{
	?>
		<ul class="toolbar-nav">
			<?php if (isset($_SESSION['login']) && $_SESSION['login'] == true):?>
        		<li class="inline first">Hello <?php print_user_detail('name') ?></li>
           	<?php endif; ?>
            <?php if (isset($_SESSION['login']) && $_SESSION['login'] == true): ?>   
        		<li class="inline"><a href="<?php echo SITE_URL ?>?action=logout">Logout</a>
            <?php else: ?>
                <li class="inline first"><a href="?page=login">Login</a>
                <?php endif; ?>
            </li>
            <li class="inline">
            	<a href="?page=browse">Browse</a>
            </li>
            <li class="inline">
            	<a href="?page=search">Search</a>
            </li>
            <?php if (isset($_SESSION['login']) && $_SESSION['login'] == true): ?>
            	<li class="inline"><a href="?page=user-home">My Account</a>            
            <?php endif; ?>
        </ul><!-- .toolbar-nav -->	
	<?php
    }
    
    function LoadCurrentUserDetails() {
    	static $current_details = null;
    	
    	if ($current_details === null) {
    		$current_details = array('name' => '', 'email' => '', 'password' => '');
    		
    		if (isset($_SESSION['login']) && $_SESSION['login']) {
    			if (isset($_SESSION['user_id']) && $_SESSION['user_id']) {
    				$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
					$query = "SELECT * FROM users WHERE id=" . intval($_SESSION['user_id']);
					$result = $db_link->query($query);

					if ($result->num_rows) {
						$row = $result->fetch_assoc();
						$current_details['name'] = $row['display_name'];
						$current_details['email'] = $row['email'];
					}
    			}
    		}
    	}
    	
    	return $current_details;
    }
    
	/*
	/* get_user_detail($detail) returns the value of the field on the user and
	/* the detail ask for.
	*/
	function get_user_detail($detail){
		$user = LoadCurrentUserDetails();
		return $user[$detail];
	}
	function print_user_detail($detail){
		$user = LoadCurrentUserDetails();
		echo $user[$detail];
	}
	
	/* get_shoppinglist_lists gets all the shopping list the user has made */
	function get_shoppinglist_list(){
		include('includes/shoppinglist_list.php');
		//return "Getting Shopping list list";
	}
	function get_mini_shopping_list(){
		include('includes/mini_shopping_list.php');	
	}
	function do_action($action){
		switch($action){
			case 'logout':
				$_SESSION['login'] = false;
				break;	
		}
	}
	function get_item_details($id){
		//connect to database, get details and return them in a object array[]	
	}
	function get_last_id(){
		$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Unable to connect to database: '.$db_link->error);
				
		$make_new_list = $db_link->query("INSERT INTO shopping_lists (name, creation_time) VALUES ('temp', NOW())");
		return $db_link->insert_id;
		
	}
	function StartSession() {
 		if (! isset($_SESSION)) {
  			session_start();
 		} 
	}
	
	function get_average_savings(){
		 $db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
                
                //$query = "select avg(total_rrp - total_special) AS average from shopping_lists;";
                $strShoppingListQuery = "select COUNT(*) as total_rows from shopping_lists";
                $strShoppingListQueryExe = mysql_query($strShoppingListQuery);
                $arrResultSet = mysql_fetch_array($strShoppingListQueryExe);
                $intTotalLists = $arrResultSet['total_rows'];
                
                
                $query = "select SUM(total_rrp) as total_price, SUM(total_special) as total_special_price  from shopping_lists";
                $queryExe = mysql_query($query);
                $arrPriceesultSet = mysql_fetch_array($queryExe);
                /* echo "--".$arrPriceesultSet['total_price'];
                echo "--".$arrPriceesultSet['total_special_price']; */
                
                $floatSaving = (($arrPriceesultSet['total_price'])-($arrPriceesultSet['total_special_price']));
                
                $floatAvgSaving = number_format(($floatSaving / $intTotalLists),2);
                
                if($floatAvgSaving)
                {
                        return $floatAvgSaving;
                }
                else
                {
                        return "There was no average";  
                }
                
                
                
                /* $result = $db_link->prepare($query);
                $result->bind_result($total_price, $total_special_price);
                $result->execute();
                
                //echo "---".$total_price;
                //echo "---".$total_special_price;
                
                
                if ($result->fetch())
                {
                        //return number_format(floatval($average), 2);
                }
                else
                {
                        return "There was no average";  
                } */
	}
	
	 
	function store_saving ($price, $special, $supermarket, $savings, $multiplier)
	{
		if (! isset($savings[$supermarket])) {
			$savings[$supermarket] = 0;
		}
		
		$savings[$supermarket] += ($multiplier * (floatval($price) - floatval($special)));		
		return $savings;
	}

	function GetFormattedDBDate($the_date) {
		$formatted_date = '';
		if ($the_date && ($the_date != '0000-00-00')) {
			$date_parts = explode('-', $the_date);
			$formatted_date = ($date_parts[2] . '/' . $date_parts[1] . '/' . $date_parts[0]);
		}
		
		return $formatted_date;
	}
	
	function GetAislesWithNoProductTypes() {
		static $data = null;
		
		if ($data === null) {
			$data = array();
			
			$db_link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			$query = "SELECT aisle.id AS aisle_id 
						FROM aisle 
						LEFT JOIN product_types ON product_types.aisle_id=aisle.id
						WHERE product_types.id IS NULL";
			
			$result = $db_link->query($query);
			while ($row = $result->fetch_assoc()) {
				$data[intval($row['aisle_id'])] = true;
			}
		}
		
		return $data;
	}
	
	function GetDemographicsOfProductsForSale($filter_on_user_postcode = true) {
		static $data = null;
		global $user;
		
		if ($data === null) {
			$data = array();
			$data['products'] = array();
			$data['product_types'] = array();
			$data['aisles'] = array();
			
			$now_date_string = date('Y-m-d');
			$db_link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			$query = "SELECT products.id AS product_id, 
							product_types.id AS product_type_id,
							aisle.id AS aisle_id
						FROM product_sales 
						INNER JOIN products ON products.id=product_sales.product_id
						INNER JOIN product_types ON product_types.id=products.type
						INNER JOIN aisle ON aisle.id=product_types.aisle_id
						WHERE product_sales.start_date <= '$now_date_string'
						AND product_sales.end_date >= '$now_date_string'";
			
			if ($filter_on_user_postcode) {
				$gm_cookie = new GM_Cookie();
				$postcode = $gm_cookie->registered_postcode;
				 
				if ($postcode && is_numeric($postcode)) {
					$state_code = substr($postcode, 0, 1);
					$state_code_minimum = ($state_code * 1000);
					$state_code_maximum = (($state_code * 1000) + 999);
					$query .= " AND product_sales.postcode >= $state_code_minimum AND product_sales.postcode <= $state_code_maximum";
				}
			}
				
			$result = $db_link->query($query);
			
			while ($row = $result->fetch_assoc()) {
				$product_id = intval($row['product_id']);
				$product_type_id = intval($row['product_type_id']);
				$aisle_id = intval($row['aisle_id']);
				
				if (! isset($data['products'][$product_id])) {
					$data['products'][$product_id] = true;
				}
				if (! isset($data['product_types'][$product_type_id])) {
					$data['product_types'][$product_type_id] = true;
				}
				if (! isset($data['aisles'][$aisle_id])) {
					$data['aisles'][$aisle_id] = true;
				}
			}
		}
		
		return $data;
	}
	
	class GM_Cookie {
		const NAME = 'gm_registered_data';
		
		var $the_original_cookie_string;
		var $has_visited_before;
		var $is_registered;
		var $registered_postcode;
		
		function __construct() {
			$this->the_original_cookie_string = '';
			$this->has_visited_before = false;
			$this->is_registered = false;
			$this->registered_postcode = '';
			
			$this->LoadFromCurrentCookie();
		}
		
		function LoadFromCurrentCookie() {
			global $user;
			
			$this->the_original_cookie_string = '';
			if (isset($_COOKIE)) {
				if (isset($_COOKIE[self::NAME])) {
					if ($_COOKIE[self::NAME]) {
						$the_cookie_string = $_COOKIE[self::NAME];
						$this->the_original_cookie_string = $the_cookie_string;
						
						$the_cookie_string_parts = explode('|', $the_cookie_string);
						if (count($the_cookie_string_parts) == 3) {
							$this->has_visited_before = ($the_cookie_string_parts[0] == '1');
							$this->is_registered = ($the_cookie_string_parts[1] == '1');
							$this->registered_postcode = $the_cookie_string_parts[2];
						}
					}		
				}
			}
			
			// Now... there's something we might know other than the cookie.
			if ($user && $user->isAuthorized()) {
				$this->has_visited_before = true;
				$this->is_registered = true;
				$this->registered_postcode = ($user->getPostCode() ? $user->getPostCode() : '');
			}
		}
		
		function IsUserLoggedIn() {
			global $user;
			return ($user && $user->isAuthorized());
		}
		
		function WriteBackCookie() {
			// Maybe things have changed... we know the 'visited before' flag has, at least!
			$this->has_visited_before = true;
			
			$new_cookie_string = ($this->has_visited_before ? '1' : '0');
			$new_cookie_string .= '|' . ($this->is_registered ? '1' : '0');
			$new_cookie_string .= '|' . $this->registered_postcode;
			
			$_COOKIE[self::NAME] = $new_cookie_string;
			setcookie(self::NAME, $new_cookie_string, (time() + (60 * 60 * 24 * 365 * 10)));
		}
	}
	
	function RewriteSmartQuotes($the_string) {
		// Get the tricky encodings first...
		// from http://shiflett.org/blog/2005/oct/convert-smart-quotes-with-php
		// and http://digitalcolony.com/2007/07/replacing-the-extended-ascii-dash-in-c-and-sql/
		// http://www.fileformat.info/info/unicode/char/2795/index.htm: useful lookups
		$other_smart_quote_searches = array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6", "\xe2\x9e\x95");
		$other_smart_quote_replacements = array("'", "'", '"', '"', "-", "-", "...", "+");
		$the_string = str_replace($other_smart_quote_searches, $other_smart_quote_replacements, $the_string);
	
		// now convert a handful of other icky (read "smart-quote-type") characters...
		$smart_quote_character_code_replacements = array();
		$smart_quote_character_code_replacements[145] = "'";
		$smart_quote_character_code_replacements[213] = "'";
		$smart_quote_character_code_replacements[146] = "'";
		$smart_quote_character_code_replacements[147] = '"';
		$smart_quote_character_code_replacements[148] = '"';
		$smart_quote_character_code_replacements[150] = '-';
		$smart_quote_character_code_replacements[151] = '-';
	
		foreach ($smart_quote_character_code_replacements as $smart_quote_code => $smart_quote_replacement) {
			$the_string = str_replace(chr($smart_quote_code), $smart_quote_replacement, $the_string);
		}
	
		// Copied from http://forums.solmetra.com/viewtopic.php?f=1&t=1116
		// function fix_fancy_quotes($text) {
			$p = array("\xBB","\xAB","\xAA","\xD2","\x93","\x94","\x8D",
					"\xBA","\xD3","\x8E","\xD4","\x92","\x8F","\xD5",
					"\x90","\xD0","\xD1","\x97","\x84", "\x85");
			$r = array(  ")" , "("  , '"'  , '"'  , '"'  , '"'  , '"'  , '"'  , '"'  , '"'  ,
					"'"  , "'"  , "'"  , "'"  , "'"  , "-"  , "-"  , "-"  , "-", "...");
			$the_string = str_replace($p,$r,$the_string);
		// }
		
		return $the_string;
	}
?>