<?php
require_once '_inc.php';
require_once 'functions.php';
$head_title = array();
$head_title[] = 'Members';
require_once '_header.php';
//$shopping_lists = include('includes/shoppinglist_list.php');
// We get an instance of the users module
$user_module = MK_RecordModuleManager::getFromType('user');

if($user_id = MK_Request::getQuery('profile'))
{
	try
	{
		$user_profile = MK_RecordManager::getFromId($user_module->getId(), $user_id);
		
		$last_active_time = strtotime($user_profile->getLastlogin());
		$last_active = MK_Utility::timeSince( $last_active_time );

		$output = '';
		$output .= '<div id="user-home" class="rounded-corners">';
		
		$output .= '	<div id="user-panel">';
		$output .= '		<div id="user-details" class="rounded-corners">';		
		$output .= '			<h4>Welcome '.ucwords($user->getDisplayName()).'</h4>';

		

		$output.= '				<ul class="user-details-list">';
		$output.= '				<li><img class="user-image" src="library/thumb.php?f='.( $user_profile->getAvatar() ? $user_profile->getAvatar() : 'tpl/img/no-avatar.png' ).'&h=128&w=128&m=crop" /></li>';
		if( $name = $user_profile->getName() )
		{
			$output.= '			<li>Real Name: ';
			$output.= '				<span class="value">'.$name.'</span></li>';
		}
		$output.= '				<li>Display Name: ';
		$output.= '					<span class="value">'.$user_profile->getDisplayName().'</span></li>';
		$output.= '				<li>Member Since: ';
		$output.= '					<span class="value">'.$user_profile->renderDateRegistered().'</span></li>';
		$output.= '				<li>Last Seen: ';
		$output.= '					<span class="value">'.( $last_active_time < time() - 900 ? $last_active.' ago' : 'Currently online' ).'</span></li>';
		if( $website = $user_profile->getWebsite() )
		{
			$output.= '			<li>Website: ';
			$output.= '				<a href="'.$website.'">'.$website.'</a></li>';
		}
		if( $postcode = $user_profile->getPostCode() )
		{
			$output .= '		<li>Post Code: ';
			$output .= '			<span class="value">'.$postcode.'</span></li>';	
		}
		if( $date_of_birth = $user_profile->getDateOfBirth() )
		{
			$date_of_birth = date($config->site->date_format, strtotime($date_of_birth));
			$output.= '			<li>Date of Birth: ';
			$output.= '				<span class="value">'.$date_of_birth.'</span></li>';
		}
		$output .= '			</ul>';
		$output .= '		</div>';
		$output .= '	</div>';
		
			
		
	}
	catch(Exception $e)
	{
		header('Location:'.MK_Utility::serverUrl('index.php'), true, 302);
	}
}
else
{
	// We don't want ALL of the users so we create a MK_Paginator
	$paginator = new MK_Paginator();
	
	// If no page is defined we default to the first page and 10 users per page
	$page = MK_Request::getQuery('page', 1);
	
	$paginator
		->setPage($page)
		->setPerPage(12);
	
	// Get users
	$users = $user_module->getRecords($paginator);
	
	$output = '<h2>Members</h2>';
	$output.= '<ul class="member-list clear-fix">';
	foreach($users as $current_user)
	{
		$last_active_time = strtotime($current_user->getLastlogin());
		$last_active = MK_Utility::timeSince( $last_active_time );
		$output.= '<li>';
		$output.= '<img src="library/thumb.php?f='.($current_user->getAvatar() ? $current_user->getAvatar() : 'tpl/img/no-avatar.png').'&h=45&w=45&m=crop" />';
		$output.= '<h3><a href="members.php?profile='.$current_user->getId().'">'.MK_Utility::tidyTrim( $current_user->getDisplayName(), 15, true ).'</a></h3>';
		$output.= '<p>'.( $last_active_time < time() - 900 ? 'Last seen '.$last_active.' ago' : 'Currently online' ).'</p>';
		$output.= '</li>';
	}
	$output.= '</ul>';
	// Show pagination
	$output.= '<div class="clear-fix paginator">'.$paginator->render('members.php?page={page}').'</div>';
}


print $output;
//get_shoppinglist_list();
include('includes/shoppinglist_list.php');
print '</div>';
require_once '_footer.php';

?>