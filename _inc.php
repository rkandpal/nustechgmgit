<?php
//ini_set( "display_errors", 0);
// This file initializes Mokoala
require_once 'admin/library/com/mokoala/Mokoala.php';

// The next logical redirect page
$logical_redirect = (!empty($config->site->referer) && $config->site->referer === $config->site->page ? '/' : $config->site->referer);

// Connect to the database
MK_MySQL::connect();

// Get an instance of the session object
$session = MK_Session::getInstance();

// Get an instance of the config object
$config = MK_Config::getInstance();

// Get an instance of the cookie object
$cookie = MK_Cookie::getInstance();

// Is Mokoala installed?
if( !$config->site->installed )
{
	header("Location: ".MK_Utility::serverUrl("admin/"), true, 302);
	exit;
}

// If user is logging in decide which page they are redirected to next
if( !$config->extensions->core->login_url )
{
	$login_redirect = ltrim($logical_redirect, '/');
}
else
{
	$login_redirect = !empty($config->extensions->core->login_url) ? $config->extensions->core->login_url : '/';	
}

// Check if user is logging in with Windows Live
if( $config->site->windowslive->login && ( $wrap_verification_code = MK_Request::getParam('wrap_verification_code')) )
{
    $access_response = $config->windowslive->getAuthorizationToken($wrap_verification_code);
	if( !empty($access_response['uid']) && !empty($access_response['wrap_access_token']) )
	{
		$profile = $config->windowslive->getProfile($access_response['uid'], $access_response['wrap_access_token']);

		$user_details = array(
			'windows_live_id' => $profile['Cid'],
			'name' => trim($profile['FirstName']." ".$profile['LastName']),
			'email' => $profile['Emails'][0]['Address'],
			'image' => !empty($profile['ThumbnailImageLink']) ? $profile['ThumbnailImageLink'] : ''
		);
	}
	else
	{
		header('Location:'.MK_Utility::serverUrl('login.php'), true, 302);
		exit;
	}

	// Check if users has already logged in with this Yahoo account
	try
	{
		MK_Authorizer::authorizeByWindowsLiveId( $user_details['windows_live_id'] );
		$user = MK_Authorizer::authorize();
	}
	// If user hasn't logged in with this Yahoo account
	catch( Exception $e )
	{
		// Check if this user is already registered with their Yahoo email address 
		try
		{
			MK_Authorizer::authorizeByWindowsLiveEmail( $user_details['email'] );
			$user = MK_Authorizer::authorize();
			
			if( !$user->getName() )
			{
				$user->setName( $user_details['name'] );
			}
			
			if( !$user->getAvatar() && !empty($user_details['image']) )
			{
				$user_details['image'] = MK_FileManager::uploadFileFromUrl( $user_details['image'], $config->site->upload_path );
				$user->setAvatar( $user_details['image'] );
			}

			$user
				->setWindowsLiveId( $user_details['windows_live_id'] )
				->save();
		}
		catch( Exception $e )
		{
			$user_module = MK_RecordModuleManager::getFromType('user');
			$user = MK_RecordManager::getNewRecord($user_module->getId());
			
			if( !empty($user_details['image']) )
			{
				$user_details['image'] = MK_FileManager::uploadFileFromUrl( $user_details['image'], $config->site->upload_path );
			}

			$user
				->setEmail( $user_details['email'] )
				->setDisplayName( $user_details['name'] )
				->setName( $user_details['name'] )
				->setAvatar( $user_details['image'] )
				->setType( MK_RecordUser::TYPE_WINDOWS_LIVE )
				->setWindowsLiveId( $user_details['windows_live_id'] )
				->save();
		}
	}

	$cookie->set('login', $user->getId(), $config->site->user_timeout);
	$session->login = $user->getId();

	header('Location: '.MK_Utility::serverUrl($login_redirect), true, 302);
	exit;
}
// Check if user is logging in with Yahoo
elseif( $config->site->yahoo->login && YahooSession::hasSession($config->site->yahoo->consumer_key, $config->site->yahoo->consumer_secret, $config->site->yahoo->app_id) && ( $session = YahooSession::requireSession($config->site->yahoo->consumer_key, $config->site->yahoo->consumer_secret, $config->site->yahoo->app_id) ) )
{
	$user = $session->getSessionedUser();
	$profile = $user->getProfile();

	$image_url = ( !empty($profile->image) ? (string) $profile->image->imageUrl : '' );
	$email = '';
	
	if( !empty($profile->emails) )
	{
		foreach( $profile->emails as $single_email )
		{
			if( !empty($single_email->handle) && !empty($single_email->primary) )
			{
				$email = (string) $single_email->handle;
				if( (boolean) $single_email->primary )
				{
					break;
				}
			}
		}
	}

	$user_details = array(
		'yahoo_id' => (string) $profile->guid,
		'name' => trim(((string) $profile->givenName)." ".((string) $profile->familyName)),
		'email' => $email,
		'image' => $image_url
	);

	// Check if users has already logged in with this Yahoo account
	try
	{
		MK_Authorizer::authorizeByYahooId( $user_details['yahoo_id'] );
		$user = MK_Authorizer::authorize();
	}
	// If user hasn't logged in with this Yahoo account
	catch( Exception $e )
	{
		// Check if this user is already registered with their Yahoo email address 
		try
		{
			MK_Authorizer::authorizeByYahooEmail( $user_details['email'] );
			$user = MK_Authorizer::authorize();
			
			if( !$user->getName() )
			{
				$user->setName( $user_details['name'] );
			}
			
			if( !$user->getAvatar() && !empty($user_details['image']) )
			{
				$user_details['image'] = MK_FileManager::uploadFileFromUrl( $user_details['image'], $config->site->upload_path );
				$user->setAvatar( $user_details['image'] );
			}

			$user
				->setYahooId( $user_details['yahoo_id'] )
				->save();
		}
		catch( Exception $e )
		{
			$user_module = MK_RecordModuleManager::getFromType('user');
			$user = MK_RecordManager::getNewRecord($user_module->getId());
			
			if( !empty($user_details['image']) )
			{
				$user_details['image'] = MK_FileManager::uploadFileFromUrl( $user_details['image'], $config->site->upload_path );
			}

			$user
				->setEmail( $user_details['email'] )
				->setDisplayName( $user_details['name'] )
				->setName( $user_details['name'] )
				->setAvatar( $user_details['image'] )
				->setType( MK_RecordUser::TYPE_YAHOO )
				->setYahooId( $user_details['yahoo_id'] )
				->save();
		}
	}

	$cookie->set('login', $user->getId(), $config->site->user_timeout);
	$session->login = $user->getId();
	
	YahooSession::clearSession();

	header('Location: '.MK_Utility::serverUrl($login_redirect), true, 302);
	exit;
}
// Check if user is logging in with Facebook
elseif( $config->site->facebook->login && ( $facebook_session = $facebook->getSession() ))
{
	$user_details = $facebook->api('/me');

	// Check if users has already logged in with this facebook account
	try
	{
		MK_Authorizer::authorizeByFacebookId( $user_details['id'] );
		$user = MK_Authorizer::authorize();
	}
	// If user hasn't logged in with this Facebook account
	catch( Exception $e )
	{
		// Check if this user is already registered with their Facebook email address 
		try
		{
			MK_Authorizer::authorizeByFacebookEmail( $user_details['email'] );
			$user = MK_Authorizer::authorize();
			
			if( !$user->getName() )
			{
				$user->setName( $user_details['name'] );
			}
			
			if( !$user->getAvatar() )
			{
				$user_details['picture'] = MK_FileManager::uploadFileFromUrl( 'http://graph.facebook.com/'.$user_details['id'].'/picture?type=large', $config->site->upload_path );
				$user->setAvatar( $user_details['picture'] );
			}

			$user
				->setFacebookId( $user_details['id'] )
				->save();
		}
		catch( Exception $e )
		{
			$user_module = MK_RecordModuleManager::getFromType('user');
			$user = MK_RecordManager::getNewRecord($user_module->getId());
			
			$user_details['picture'] = MK_FileManager::uploadFileFromUrl( 'http://graph.facebook.com/'.$user_details['id'].'/picture?type=large', $config->site->upload_path );
	
			$user
				->setEmail( $user_details['email'] )
				->setDisplayName( $user_details['name'] )
				->setName( $user_details['name'] )
				->setAvatar( $user_details['picture'] )
				->setType( MK_RecordUser::TYPE_FACEBOOK )
				->setFacebookId( $user_details['id'] )
				->save();
		}
	}

	$cookie->set('login', $user->getId(), $config->site->user_timeout);
	$session->login = $user->getId();

	header('Location: '.MK_Utility::serverUrl($login_redirect), true, 302);
	exit;
}
// Check if user logged in with Twitter 
elseif( $config->site->twitter->login && $session->twitter_access_token )
{
	$user_details = $config->twitter->get('account/verify_credentials');
	unset($session->twitter_access_token);
	
	try
	{
		MK_Authorizer::authorizeByTwitterId( $user_details->id );
		$user = MK_Authorizer::authorize();

		$cookie->set('login', $user->getId(), $config->site->user_timeout);
		$session->login = $user->getId();
	}
	catch( Exception $e )
	{
		$session->registration_details = serialize(
			array(
				'picture' => str_replace('_normal', '', $user_details->profile_image_url),
				'display_name' => $user_details->name,
				'name' => $user_details->name,
				'twitter_id' => $user_details->id,
				'email' => '',
			)
		);
	}

	header('Location: '.MK_Utility::serverUrl('login.php'), true, 302);
}
// Check if user logged in with Google 
elseif( $config->site->google->login && $session->google_access_token )
{
	$user_details = $config->google->get('people/@me/@self');

	unset($session->google_access_token);
	$user_details = $user_details['entry'];
	
	try
	{
		MK_Authorizer::authorizeByGoogleId( $user_details['id'] );
		$user = MK_Authorizer::authorize();

		$cookie->set('login', $user->getId(), $config->site->user_timeout);
		$session->login = $user->getId();
	}
	catch( Exception $e )
	{

		$session->registration_details = serialize(
			array(
				'display_name' => $user_details['displayName'],
				'name' => $user_details['name']['formatted'],
				'google_id' => $user_details['id'],
				'email' => '',
			)
		);
	}

	header('Location: '.MK_Utility::serverUrl('login.php'), true, 302);
}
// Check if user hasn't finished logging in with oAuth
elseif( !empty($session->registration_details) && strpos($config->site->page_name, 'login.php') === false )
{
	header('Location: '.MK_Utility::serverUrl('login.php'), true, 302);
	exit;
}
// Check if user completed login with oAuth 
elseif( ($config->site->twitter->login || $config->site->google->login) && !empty($session->registration_details) )
{
	$user_details = unserialize( $session->registration_details );
	if( !empty($user_details['email']) )
	{
	
		$user_module = MK_RecordModuleManager::getFromType('user');
		$user = MK_RecordManager::getNewRecord($user_module->getId());
	
		$user
			->setEmail( $user_details['email'] )
			->setDisplayName( $user_details['name'] )
			->setName( $user_details['name'] );
			

		if( !empty($user_details['picture']) )
		{
			$user_details['picture'] = MK_FileManager::uploadFileFromUrl( $user_details['picture'], $config->site->upload_path );
			$user->setAvatar( $user_details['picture'] );
		}
		
		if( !empty($user_details['google_id']) )
		{
			$user
				->setType( MK_RecordUser::TYPE_GOOGLE )
				->setGoogleId( $user_details['google_id'] );
		}
		elseif( !empty($user_details['twitter_id']) )
		{
			$user
				->setType( MK_RecordUser::TYPE_TWITTER )
				->setTwitterId( $user_details['twitter_id'] );
		}

		$user->save();

		$cookie->set('login', $user->getId(), $config->site->user_timeout);
		$session->login = $user->getId();
		
		unset($session->registration_details);

		header('Location: '.MK_Utility::serverUrl($login_redirect), true, 302);
		exit;
	}
}

// If user session has expired but cookie is still active
if( $cookie->login && empty($session->login) )
{
	$session->login = $cookie->login;
}

// Get current user
if( !empty($session->login) )
{
    MK_Authorizer::authorizeById( $session->login );
}

$user = MK_Authorizer::authorize();

?>