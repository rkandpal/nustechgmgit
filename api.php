<?php
require_once '_inc.php';

$api_server = new MK_API_REST_Server();

if( $parameters = MK_Request::getPost() )
{
	$method = 'post';
}
else
{
	$method = 'get';
	$parameters = MK_Request::getQuery();
}

$output = $api_server->processRequest($method, $parameters);

print json_encode( $output );

?>